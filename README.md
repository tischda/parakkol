Parakkol - Parallel AKKA Color Solver
=====================================

Polytech Tours, 5ème année, 2013-1024

Description
-----------

Parakkol is a java application that solves a color problem using the AKKA
framework to run several solvers in parallel.

To run the application:

~~~
gradle -q run
~~~

Links:

* Source: [https://bitbucket.org/tischda/parakkol](https://bitbucket.org/tischda/parakkol)
* Bugs: [https://lobot.danieltischer.com:8080/jira/browse/VAE](http://lobot.danieltischer.com:8080/jira/browse/VAE)
* Sonar: [https://lobot.danieltischer.com:9000/sonar/dashboard/index/391](http://lobot.danieltischer.com:9000/sonar/dashboard/index/391)
* Build: [https://luke.danieltischer.com:8810/dashboard](http://luke.danieltischer.com:8810/dashboard)


Requirements
============

* gnuplot 5
* graphwiz 2.38
* pandoc 1.19
* LaTeX tools

SonarQube requires NodeJS to be installed, othwerise:

~~~
Failed to get Node.js version. No CSS files will be analyzed.
java.io.IOException: Cannot run program "node": CreateProcess error=2, The system cannot find the file specified
~~~

Gradle
======

Maven -> Gradle: The command is not `gradle setupBuild` but `gradle init`. This
also installs the Gradle Wrapper. When you start `gradlew`, this downloads
Gradle (~ 40 MB) and runs it. This is useful on Mac as the version on Macports
is 1.8 and not 1.10.

Gradle 4.0 seems to be the last version to work with this project. Newer versions (even 4.x) break
the API used by the JavaFX plugin. Newer version of JavaFX plugin require newer versions of java...


Gradle notes
------------

* compilation output folder is `build` not `target` (but some files still get
  written to `target` by our java classes!

* you should ignore `.gradle` folder in git

* To see the dependency tree: `gradle dependencies`

* http://www.gradle.org/docs/current/dsl/org.gradle.api.tasks.JavaExec.html
  The property allJvmArgs throws java.lang.UnsupportedOperationException
  You can use jvmArgs instead, not sure which params it actually activates

* Gradle logging is defined here:
  http://www.gradle.org/docs/current/userguide/logging.html


Gradle packaging
----------------
To assemble the application:

~~~
gradle installApp
~~~

This will output everything here:

~~~
u:\src\polytech\parakkol\build\install\parakkol
~~~

Startup script is in the `bin` folder.

To package a distribution zip, use `gradle distZip`



JAVA FX 2
=========

Eclipse Plugins
---------------

http://www.eclipse.org/efxclipse/install.html

* xtext - http://download.eclipse.org/modeling/tmf/xtext/updates/composite/releases/

    There are conflicts with Geppetto (puppet), you need to install all these:

    [x] EMFT MWE2 Language - 2.5.1
    [x] EMFT MWE2 Runtime - 2.5.1
    [x] Xtext-2.5.2

* efxeclipse - http://download.eclipse.org/efxclipse/updates-released/0.9.0/site/

    STS 3.4 -> Eclipse 4.3 -> Kepler

    [x] e(fx)clipse - IDE - Kepler


Documentation
=============

`biblio-parakkol.bib` needs to be in the build directory `u:\src\parakkol\build\docs\`, or `latexmk` will
not find it. There are also a few issues that need to be fixed to work with latest pandoc+latex (01/2017).
