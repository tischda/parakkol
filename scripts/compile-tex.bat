::-----------------------------------------------------------------------------
:: This scripts compiles LaTeX files to PDF with latexmk (pdflatex)
::-----------------------------------------------------------------------------
@echo off
setlocal

if "%~1"=="" goto usage

set INPUT=%*

:: paths
set BASE_DIR=%cd%
set SRC_DIR=%BASE_DIR%\src\tex
set BUILD_DIR=%BASE_DIR%\build\docs
set CLS_PATH=%BASE_DIR%/modele

:: latex
set DOCUMENTCLASS=EPURapport
set TEXINPUTS=%SRC_DIR%;%CLS_PATH%;%BUILD_DIR%
set BIBINPUTS=%SRC_DIR%
set max_print-line=1048576

::-----------------------------------------------------------------------------
REM Process each .tex input file individually

pushd "%BUILD_DIR%"
for %%X in (%INPUT%) do (
    REM add the -enable-write18 option for pstricks
    call latexmk -pdf -bibtex-cond -interaction=nonstopmode %SRC_DIR%/%%X
)
popd

goto end
:: ----------------------------------------------------------------------------
:usage REM Usage
echo Usage: %0 ^<file.tex [file2.tex ...]^>

:end
endlocal
