#!/bin/bash
# ----------------------------------------------------------------------------
# This script compiles markdown files to latex, and then to pdf using pandoc
# ----------------------------------------------------------------------------
if [ $# -eq 0 ] ; then
    echo Usage: $0 \<file1.md [file2.md ...]\>
	exit 1
fi

# source files and final PDF output file name (without .pdf file extension)
INPUT=$*
OUTPUT=`basename ${1%.*}`
BASE_DIR=$(pwd)

# Windows tools can't handle '/cygdrive' prefixes
if [ "$(expr substr $(uname -s) 1 6)" == "CYGWIN" ];then
    OUTPUT=$(cygpath -w ${OUTPUT})
    BASE_DIR=$(cygpath -w ${BASE_DIR})
fi

SRC_DIR="${BASE_DIR}/src/md"
BUILD_DIR="${BASE_DIR}/build/docs"
CLS_PATH="${BASE_DIR}/modele"

# LaTeX paths (mind the trailing double // in TEXINFPUTS !)
DOCUMENTCLASS=EPURapport
export TEXINPUTS="${SRC_DIR}:${CLS_PATH}//:"
export BIBINPUTS="${SRC_DIR}"

# No unix path separator ':' and trailing double // in TEXINFPUTS on Windows
if [ "$(expr substr $(uname -s) 1 6)" == "CYGWIN" ];then
    export TEXINPUTS="${SRC_DIR};${CLS_PATH}"
fi

# Pandoc
PANDOCOPTIONS="--template=${BASE_DIR}/pandoc/pandoc.latex \
    --highlight-style=tango \
    --number-sections \
    --top-level-division=chapter \
    --listings \
    -V documentclass=${DOCUMENTCLASS} \
    --filter pandoc-citeproc \
    --standalone ${PANDOC_OPTS}"

# ----------------------------------------------------------------------------

# Convert Markdown to LaTeX
pushd ${SRC_DIR}
pandoc -o ${BUILD_DIR}/${OUTPUT}.tex ${INPUT} ${PANDOCOPTIONS}
popd

# Process LaTeX
pushd ${BUILD_DIR}
latexmk -pdf -bibtex-cond -interaction=nonstopmode ${OUTPUT}.tex
popd
