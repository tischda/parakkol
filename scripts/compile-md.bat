::-----------------------------------------------------------------------------
:: This script compiles markdown files to LaTeX, and then to PDF using Pandoc
::-----------------------------------------------------------------------------
@echo off
setlocal

if "%~1"=="" goto usage

:: source files and PDF output file name (without .pdf file extension)
set INPUT=%*
set OUTPUT=%~n1

:: paths
set BASE_DIR=%cd%
set SRC_DIR=%BASE_DIR%\src\md
set BUILD_DIR=%BASE_DIR%\build\docs
set CLS_PATH=%BASE_DIR%/modele

:: latex
set DOCUMENTCLASS=EPURapport
set TEXINPUTS=%SRC_DIR%;%CLS_PATH%
set BIBINPUTS=%SRC_DIR%
set max_print-line=1048576

:: pandoc
set PANDOCOPTIONS=--template=%BASE_DIR%/pandoc/pandoc.latex ^
    --highlight-style=tango ^
    --number-sections ^
    --top-level-division=chapter ^
    --listings ^
    -V documentclass=%DOCUMENTCLASS% ^
    --filter pandoc-citeproc ^
    --standalone %PANDOC_OPTS%

::-----------------------------------------------------------------------------
REM Convert Markdown to LaTeX
pushd "%SRC_DIR%"
call pandoc -o %BUILD_DIR%/%OUTPUT%.tex %INPUT% %PANDOCOPTIONS%
popd

REM Process LaTeX
pushd "%BUILD_DIR%"
call latexmk -pdf -bibtex-cond -interaction=nonstopmode %OUTPUT%.tex
popd

goto end
::----------------------------------------------------------------------------
:usage REM Usage
echo Usage: %0 ^<file1.md [file2.md ...]^>

:end
endlocal
