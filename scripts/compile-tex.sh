#!/bin/bash
# ----------------------------------------------------------------------------
# This scripts compiles the .tex files with pdflatex
# ----------------------------------------------------------------------------
if [ $# -eq 0 ] ; then
    echo Usage: $0 \<file1.tex [file2.tex ...]\>
    exit 1
fi

INPUT=$*
BASE_DIR=$(pwd)

# Windows tools can't handle '/cygdrive' prefixes
if [ "$(expr substr $(uname -s) 1 6)" == "CYGWIN" ];then
    BASE_DIR=$(cygpath -w ${BASE_DIR})
fi

SRC_DIR="${BASE_DIR}/src/tex"
BUILD_DIR="${BASE_DIR}/build/docs"
CLS_PATH="${BASE_DIR}/modele"

# LaTeX paths (mind the trailing double // in TEXINFPUTS !)
export TEXINPUTS="${SRC_DIR}:${BUILD_DIR}:${CLS_PATH}//:"
export BIBINPUTS="${SRC_DIR}"

# No unix path separator ':' and trailing double // in TEXINFPUTS on Windows
if [ "$(expr substr $(uname -s) 1 6)" == "CYGWIN" ];then
    export TEXINPUTS="${SRC_DIR};${BUILD_DIR};${CLS_PATH}"
fi

# ----------------------------------------------------------------------------
# Process each .tex input file individually

pushd ${BUILD_DIR}
for X in $INPUT
do
    # add -enable-write18 for pstricks
    latexmk -pdf -bibtex-cond -interaction=nonstopmode ${SRC_DIR}/$X
done
popd
