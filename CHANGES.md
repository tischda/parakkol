Project Notes / History
=======================

20/03/2014
----------

Finished report.

17/03/2014
----------

Fixed performance issue due to accessing Akka Config inside of ParametersGA.
Fixed memory leak. EvaluationListener must be removed when problem resolved.


10/03/2014
----------

Exception in masterActor will restart actor and reinitialize fx fields to null.
The UI becomes unresponsive. The problem is that you only see that in the logs.

Hard time to propertly stop solvers on ClearStatistics.

Semantic confusion between JavaFX "Master" View and AKKA "MasterActor" which is
controlled by JavaFX DetailView.

ObservableList is not threadsafe.

09/03/2014
----------

Fixed parameter handling.

New in akka 2.3.0:
http://akka.io/news/2014/02/03/akka-2.3.0-RC2-released.html
Routees can be dynamically added and removed by sending special management
messages to the router.

--> Aha, just spent 4 hours trying to find how to do that...


07/03/2014
----------

Just noticed that the injection mechanism (by type) only allows to inject a
single actor (ActorRef).


06/03/2014
----------

Solver status now updates list values correctly :

When a solver finishes, the record stays in the list with status 'Idle'.
A new entry is created for the same actor for a different problem. Because
status and metric updates are done async, the display may appear to switch
between the problems (new solver aldready started, but metrics messages of
old problem still arriving).


05/03/2014
----------

Implemented list of actors. The problem: actors can be remote and the the
UI is local. How do we update multiple local UIs?

Same issue for parameters. Ok we save(), but this is only on the local machine.
How do actors get aware of configuration changes, aka. RUN_COUNT has changed?


22/02/2014
----------

The original code records the _best_ result, so max() and mean() return values
that are different from what we expect in our unit tests.

L.420
~~~java
  private void recordSolution(final Solution solution) {
    if (best.get(runCounter) == null || best.get(runCounter).getFitness() > solution.getFitness()) {
      best.set(runCounter, solution.copy());
    }

    final long elapsed = getCpuTime() - runStartTime;
    evaluations.get(runCounter).add(best.get(runCounter).getFitness());      <--- best
    elapsedTimes.get(runCounter).add(elapsed);
  }
~~~

L.337
~~~java
      private double maxByEvaluation(final int eval) {
        double max = evaluations.get(0).get(eval);
        for (int i = 0; i <= runCounter; ++i) {
          max = Math.max(max, evaluations.get(i).get(eval));       <--- will return value of eval 0 instead of 1
        }
        return max;
      }
~~~


19/02/2014
----------

Integrated afterburner.fx framework. Was a lot of effort to test the injection
part. The problem was to run multiple tests where the state of static classes
remains the same (concurrent executions of test methods). Also calling
`System.setProperties(null)` to initialize tests was a very bad idea.

Now injecting AKKA master Actor directly into controller objects.

Implemented internationalization with resource bundles.


16/02/2014
----------

Implemented GUI with Java FX. One important thing is to add the dependency for
JDK7 (not necessary in JDK8). The gradle plugin takes care of this.

The application plugin works together with the javafx plugin, but I had to add
the configuration file in the CLASSPATH and fix the start scripts to run from
the APP_HOME.

First experience with JavaFX, seems ok. Not very clear what is the best way to
inject the master ActorRef into MainApp, because I don't want to instantiate the
Akka system in MainApp (think about future headless execution of the slaves).

Also, be careful when copy-pasting GUI items, it copies stuff meta data it
should definitely not copy, for example IDs. It is better to create new objects
from scratch. You should always check the FXML output.

Clean stop of Akka framework : Tried with poison pill to Master, but need to
check what happens exactly, appart from master dying. Is this not what system
shutdown is doing anyway? When we click 'stop' do we want to kill or restart a
new run?

Note: running many solvers in parallel could impact timings and performance
metrics of each individual run. Have we thought about that in the initial
comparison?


10/02/2014
----------

After reading http://stackoverflow.com/questions/17343287/whats-the-point-of-the-akka-microkernel-using-java-and-maven
found a better solution to package and run application with dependencies:

~~~
gradle installApp
cd u:\src\polytech\parakkol\build\install\parakkol\bin
parakkol
~~~

or simply
~~~
gradle -q run
~~~


09/02/2014
----------
Can't generate class diagram:

    * Filed Graphviz issue: http://www.graphviz.org/mantisbt/view.php?id=2421
    * Sent Pull Request: https://github.com/dspinellis/UMLGraph/pull/18

Integrated Pandoc doc generation for final report.


08/02/2014
----------

Compiled umlgraph-5.6.7-SNAPSHOT.jar from Github
Note that tools.jar need to be in the same dir.

~~~
sudo port install graphviz
sudo port install plotutils
~~~

* http://jira.codehaus.org/browse/MJAVADOC-136
* https://github.com/kercheval/GradleCMPlugin/wiki/Create-Javadoc-with-Class-Diagrams-using-UMLGraph-with-Gradle
* http://forums.gradle.org/gradle/topics/add_custom_javadoc_option_that_does_not_take_an_argument
* http://stackoverflow.com/questions/7165108/in-osx-lion-lang-is-not-set-to-utf8-how-fix


06_02_2014
----------

Fixed bug in pair swap: copied colors, but forgot to swap fitness value.

Note: You cannot update fitness after adding to TreeSet, this will miserably fail:
http://stackoverflow.com/questions/2579679/maintaining-treeset-sort-as-object-changes-value


05/02/2014
----------

Major cleanup


04/02/2014
----------

The change to work only with pairs did not work out, because a color in a pair
can also be in another pair, so you cannot just update a color in one pair.
Went back to original solution, but caching pair fitness during evaluation.


03/02/2014
----------

Refactored class names.


02/02_2014
----------

Converted Maven -> Gradle: Configuration line count goes from 266 (pom.xml)
to 31 (build.gradle).

