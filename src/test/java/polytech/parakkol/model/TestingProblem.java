package polytech.parakkol.model;

public final class TestingProblem {

    public static Problem createTestProblem() {
        int[][] colors = new int[3][3];

        // problem colors
        colors[0] = new int[] { 51, 51, 51 };
        colors[1] = new int[] { 34, 34, 34 };
        colors[2] = new int[] { 0, 116, 189 };

        // problem pairs
        int[][] pairs = new int[2][2];
        pairs[0][0] = 0;
        pairs[0][1] = 1;
        pairs[1][0] = 2;
        pairs[1][1] = 1;

        Problem problem = new Problem("problem", colors, pairs);

        return problem;
    }

    private TestingProblem() { }
}
