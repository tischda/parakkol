package polytech.parakkol.model;

public final class TestingPalette {

    public static Palette[] createTestPalettes() {

        int[][][] colors = new int[5][3][3];

        Problem problem = TestingProblem.createTestProblem();

        // init test data
        colors[0][0] = new int[] { 246, 77, 228 };
        colors[0][1] = new int[] { 151, 139, 175 };
        colors[0][2] = new int[] { 86, 250, 77 };

        colors[1][0] = new int[] { 59, 87, 86 };
        colors[1][1] = new int[] { 228, 68, 239 };
        colors[1][2] = new int[] { 194, 104, 87 };

        colors[2][0] = new int[] { 228, 81, 232 };
        colors[2][1] = new int[] { 102, 130, 124 };
        colors[2][2] = new int[] { 149, 182, 70 };

        colors[3][0] = new int[] { 174, 248, 244 };
        colors[3][1] = new int[] { 143, 163, 46 };
        colors[3][2] = new int[] { 247, 104, 23 };

        colors[4][0] = new int[] { 108, 203, 144 };
        colors[4][1] = new int[] { 73, 246, 157 };
        colors[4][2] = new int[] { 118, 252, 156 };

        Palette[] palettes = new Palette[5];
        for (int j=0; j<5; j++) {
            palettes[j] = new Palette(colors[j], new FitnessCache(problem.pairs.length, problem.reverseIndex, problem.evaluator));
            // palettes[j].cache.setEvaluator(problem.evaluator);
        }
        return palettes;
    }

    private TestingPalette() { }
}
