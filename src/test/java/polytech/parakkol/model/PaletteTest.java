package polytech.parakkol.model;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public final class PaletteTest {

    // http://stackoverflow.com/questions/1088216/whats-wrong-with-using-to-compare-floats-in-java
    private static final double EPSILON = 1.0e-15;

    Problem problem;
    Palette[] palettes = new Palette[5];

    private double flooredFitness(Palette palette) {
        return Math.floor(palette.getFitness() / EPSILON) * EPSILON;
    }

    @Before
    public void setUp() {
        palettes = TestingPalette.createTestPalettes();
        problem = TestingProblem.createTestProblem();
    }

    @Test
    public void testFitness() {
        Palette refsPalette = problem.palette;

        assertThat(Math.abs(refsPalette.getFitness() - 0.5818479597077194), lessThan(EPSILON));
        assertThat(Math.abs(palettes[0].getFitness() - 0.7005562831226713), lessThan(EPSILON));
        assertThat(Math.abs(palettes[1].getFitness() - 0.6377491993284202), lessThan(EPSILON));
        assertThat(Math.abs(palettes[2].getFitness() - 0.6769274198716912), lessThan(EPSILON));
        assertThat(Math.abs(palettes[3].getFitness() - 0.6437212357961534), lessThan(EPSILON));
        assertThat(Math.abs(palettes[4].getFitness() - 0.8259021891346149), lessThan(EPSILON));
    }

    @Test
    public void testColorChangeAffectsPair() {
        Palette palette = palettes[0];

        // calculating fitness set evaluated flags to true
        double fitness1 = flooredFitness(palette) ;

        palette.setColor(0, 5);

        assertTrue("Pair 0 is dirty", palette.cache.fitnessOfPair[0].isNaN());
        assertFalse("Pair 1 is ok", palette.cache.fitnessOfPair[1].isNaN());
        assertFalse("Palette needs to be re-evaluated", palette.isEvaluated());

        double fitness2 = flooredFitness(palette) ;
        assertThat(fitness2, not(fitness1));

        palette.setColor(1, 5);

        assertTrue("Pair 0 is dirty", palette.cache.fitnessOfPair[0].isNaN());
        assertTrue("Pair 1 is dirty", palette.cache.fitnessOfPair[1].isNaN());
        assertFalse("Palette needs to be re-evaluated", palette.isEvaluated());

        double fitness3 = flooredFitness(palette) ;
        assertThat(fitness3, not(fitness2));
    }

    @Test
    public void testPairSwapInvalidatesAffectedPairsBUTSwappedPair() {
        double fitness0 = flooredFitness(palettes[0]);
        double fitness1 = flooredFitness(palettes[1]);

        Palette.swapPair(problem.pairs, 0, palettes[0], palettes[1]);

        assertFalse("P0, Pair 0 is ok", palettes[0].cache.fitnessOfPair[0].isNaN());
        assertTrue( "P0, Pair 1 dirty", palettes[0].cache.fitnessOfPair[1].isNaN());
        assertFalse("P1, Pair 0 is ok", palettes[1].cache.fitnessOfPair[0].isNaN());
        assertTrue( "P1, Pair 1 dirty", palettes[1].cache.fitnessOfPair[1].isNaN());

        assertFalse("Palette 0 needs to be re-evaluated", palettes[0].isEvaluated());
        assertFalse("Palette 1 needs to be re-evaluated", palettes[1].isEvaluated());

        double newFitness0 = flooredFitness(palettes[0]);
        double newFitness1 = flooredFitness(palettes[1]);

        assertThat(newFitness0, not(fitness0));
        assertThat(newFitness1, not(fitness1));
    }

    @Test
    public void testPairSwap() {
        palettes[0].getFitness();
        palettes[1].getFitness();
        double fitness = palettes[0].cache.fitnessOfPair[0];

        Palette.swapPair(problem.pairs, 0, palettes[0], palettes[1]);

        // check color swap
        assertThat(palettes[0].toString(), equalTo("? [ #3b5756 #e444ef #56fa4d ]"));
        assertThat(palettes[1].toString(), equalTo("? [ #f64de4 #978baf #c26857 ]"));

        // check fitness swap
        assertThat(palettes[1].cache.fitnessOfPair[0], equalTo(fitness));
    }

    @Test
    public void testCompare() {
        assertThat(palettes[3].compareTo(palettes[0]), lessThan(0));
        assertThat(palettes[3].compareTo(palettes[3]), equalTo(0));
        assertThat(palettes[4].compareTo(palettes[3]), greaterThan(0));
    }
}
