package polytech.parakkol.model;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import polytech.parakkol.Loader;

import com.typesafe.config.ConfigFactory;

public final class LoaderTest {

//    Instance 3-2
//
//    initial colors
//    --------------
//    51 51 51
//    34 34 34
//    0 116 189
//
//    pairs
//    -----
//    0 1
//    2 1

    @Test
    public void testLoadInstance() throws IOException {
        String dataPath = ConfigFactory.load().getConfig("parakkol").getString("data_path");
        Loader loader = new Loader(dataPath);

        Problem problem = loader.readFile("3-2.dat");

        assertThat(problem.palette.getRGBColor(0)[0], equalTo(51));
        assertThat(problem.palette.getRGBColor(2)[2], equalTo(189));

        assertThat(problem.pairs[0][0], equalTo(0));
        assertThat(problem.pairs[1][1], equalTo(1));

        assertTrue("Color 0 is in pair 0", problem.reverseIndex.getPairsWithColor(0).contains(0));
        assertTrue("Color 2 is in pair 1", problem.reverseIndex.getPairsWithColor(2).contains(1));

        System.out.println(problem.toString());
    }
}

