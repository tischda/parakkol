package polytech.parakkol;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import polytech.parakkol.model.Problem;
import polytech.parakkol.model.TestingProblem;

import com.typesafe.config.ConfigFactory;

public final class LoaderTest {

    private final Logger log = LoggerFactory.getLogger(getClass());
    private final Problem theProblem = TestingProblem.createTestProblem();

    private final String dataPath = ConfigFactory.load().getConfig("parakkol").getString("data_path");

    private class TestLoader extends Loader {
        public TestLoader() {
            super(dataPath);
        }

        @Override
        public Problem readFile(String fileName) throws IOException {
            log.debug("Reading {}", fileName);
            return theProblem;
        }
    }

    @Test
    public void testPickAFile() throws IOException {
        TestLoader loader = new TestLoader();
        assertThat(loader.toProcessCount(), equalTo(164));
        assertSame(loader.pickAFile(), theProblem);
    }

    @Test(expected = IllegalStateException.class)
    public void testInititializeFileNameFromInvalidPath() {
        new Loader("z:\\osdyfglsdhg");
    }

    @Test(expected = IllegalStateException.class)
    public void testInititializeFileNameNonePresent() {
        new Loader(".");
    }
}
