package polytech.parakkol.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

public final class ColorEncoderTest {

    int[][] rgb = {
            { 255, 255, 255 },
            {   0, 127, 254 },
            { 255,  12, 128 },
            {   0,   0,   0 } };

    @Test
    public void testRGBToIntConversion() {

        for (int i = 0; i < rgb.length; i++) {
            int c = ColorEncoder.rgbToInt(rgb[i]);
            int[] rgbdec = ColorEncoder.intToRGB(c);
            assertTrue(Arrays.equals(rgb[i], rgbdec));
        }
    }

    @Test
    public void testGrayCodeConversion() {
        for (int i = 0; i < 256; i++) {
            int g = ColorEncoder.bin2gray(i);
            int b = ColorEncoder.gray2bin(g);
            assertEquals(i, b);
        }
    }

    @Test
    public void testByteConversion() {
        for (int i = 0; i < 256; i++) {
            byte b = (byte) i;
            int ii = (b & 0xFF);
            assertEquals(i, ii);
        }
    }
}
