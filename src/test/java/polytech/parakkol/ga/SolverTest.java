package polytech.parakkol.ga;

import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.Random;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import polytech.parakkol.ConfigManager;
import polytech.parakkol.Loader;
import polytech.parakkol.ParametersGA;
import polytech.parakkol.model.Problem;
import polytech.parakkol.stats.Metrics;
import polytech.parakkol.stats.Statistics;

import com.typesafe.config.ConfigFactory;

public final class SolverTest {

    private static final double ONE_MILLION = 1e6;
    private static final double ONE_BILLION = 1e9;

    private static final double EPSILON = 1.0e-15;
    private final Logger log = LoggerFactory.getLogger(getClass());

    Loader loader;
    final Solver solver;
    final ParametersGA params;

    /**
     * Constructor. Creates the testing objects needed for the test.
     *
     * @throws IOException
     */
    public SolverTest() throws IOException {
        ConfigManager configManager = new ConfigManager();
        params = new ParametersGA(configManager.load());

        String dataPath = ConfigFactory.load().getConfig("parakkol").getString("data_path");
        loader = new Loader(dataPath);
        solver = new Solver(params);
    }

    @Before
    public void setUp() {
        Random random = new Random();
        random.setSeed(1L);
        solver.setRandom(random);

    }

    @Test
    public void testSolverFitnessCalculation() throws IOException {
        Problem problem = loader.readFile("3-2.dat");
        Statistics stats = new Statistics(params);

        log.info(params.toString());

        long startTime = System.nanoTime();
        double meanBestFitness = 0.0;

        for (int i = 0; i < params.getRunCount(); i++) {

            Metrics metrics = new Metrics(params.getMaxEvaluations());
            solver.solve(problem, metrics);
            stats.record(metrics);

            double bestFitness = metrics.getBestSolution().getFitness();
            meanBestFitness += bestFitness;
            log.info("Best fitness: {}", bestFitness);
        }
        meanBestFitness /= params.getRunCount();

        log.info("---------------------");

        log.info("Best: {}", stats.getOverallBestFitness());
        log.info("Mean: {}, Total time: {} seconds", meanBestFitness, (System.nanoTime() - startTime) / ONE_BILLION);

        assertThat(stats.getMeanOfBestFitness() - meanBestFitness, lessThan(EPSILON));
        assertThat("fitness is same or has improved",
                stats.getMeanOfBestFitness(), lessThanOrEqualTo(0.03326999059424124));
    }

    @Test
    @Ignore
    public void testSolverResolutionTime() throws IOException {
        log.info(params.toString());

        Problem problem = loader.readFile("10-1.dat");
        long startTime = System.nanoTime();

        for (int i = 0; i < 1000; i++) {
            Metrics metrics = new Metrics(params.getMaxEvaluations());
            solver.solve(problem, metrics);
            log.info("Run: {}, Time: {} ms", i, metrics.getElapsedTime() / ONE_MILLION);
        }
        log.info("---------------------");
        log.info("Total time: {} seconds", (System.nanoTime() - startTime) / ONE_BILLION);
    }
}
