package polytech.parakkol.ga;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertThat;

import java.util.NavigableSet;

import org.junit.Before;
import org.junit.Test;

import polytech.parakkol.model.Palette;
import polytech.parakkol.model.TestingPalette;

public final class PopulationTest {

    // http://stackoverflow.com/questions/1088216/whats-wrong-with-using-to-compare-floats-in-java
    private static final double EPSILON = 1.0e-15;

    Palette[] palettes = new Palette[5];
    Population population = new Population();

    Double[] orderedFitness = new Double[] {
            0.6377491993284202,
            0.6437212357961534,
            0.6769274198716912,
            0.7005562831226713,
            0.8259021891346149
    };

    @Before
    public void setUp() {
        palettes = TestingPalette.createTestPalettes();
        for (Palette p : palettes) {
            population.add(p);
        }
    }

    @Test
    public void testOrdered() {
        int i = 0;
        for (Palette individual : population) {
            assertThat(Math.abs(individual.getFitness()
                    - orderedFitness[i++]), lessThan(EPSILON));
        }
    }

    @Test
    public void testGetBest() {
        NavigableSet<Palette> best = population.getBest(2);
        int i = 0;
        for (Palette individual : best) {
            assertThat(Math.abs(individual.getFitness()
                    - orderedFitness[i++]), lessThan(EPSILON));
        }
        assertThat(i, equalTo(2));
    }

    @Test
    public void testToString() {
        assertThat(population.toString(), containsString("Population of size 5"));
        assertThat(population.toString(), containsString("0.7005562831226713 [ #f64de4 #978baf #56fa4d ]"));
    }
}
