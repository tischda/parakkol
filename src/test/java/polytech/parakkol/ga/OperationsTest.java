package polytech.parakkol.ga;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import polytech.parakkol.ConfigManager;
import polytech.parakkol.ParametersGA;
import polytech.parakkol.model.Palette;
import polytech.parakkol.model.Problem;
import polytech.parakkol.model.TestingPalette;
import polytech.parakkol.model.TestingProblem;

public final class OperationsTest {

    Problem problem;
    Palette[] palettes = new Palette[5];
    Operations ops;

    @Before
    public void setUp() {
        ConfigManager configManager = new ConfigManager();
        ParametersGA params = new ParametersGA(configManager.load());

        palettes = TestingPalette.createTestPalettes();
        problem = TestingProblem.createTestProblem();
        ops = new Operations(params);
    }

    @Test
    public void testInvertBit() {
        int n = 0b01101;
        int k = 2;

        n = ops.flipBit(n, k);
        assertThat(n, equalTo(0b01001));
    }

    @Test
    public void testRandomCrossoverPairs() {
        Palette palette0 = palettes[0].copy();
        Palette palette1 = palettes[1].copy();
        ops.randomCrossoverPairs(problem, palettes[0], palettes[1]);
        assertThat(palettes[0], not(palette0));
        assertThat(palettes[1], not(palette1));
    }

    @Test
    public void testRandomCrossoverColors() {
        Palette palette0 = palettes[0].copy();
        Palette palette1 = palettes[1].copy();
        ops.randomCrossoverColors(problem, palettes[0], palettes[1]);
        assertThat(palettes[0], not(palette0));
        assertThat(palettes[1], not(palette1));
    }
}
