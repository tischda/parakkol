package polytech.parakkol.stats;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class TimerTest {

    // The hotspot VM uses 1ms resolution for Thread.sleep
    // https://blogs.oracle.com/dholmes/entry/inside_the_hotspot_vm_clocks
    private static final long MS_20 = 19L * 100000;

    @Test(expected = IllegalStateException.class)
    public void testTimerException() {
        Timer timer = new Timer();
        timer.getElapsedTime();
    }

    @Test
    public void testTotalTime() throws InterruptedException {
        Timer timer = new Timer();

        long startTime = System.nanoTime();
        timer.start();
        Thread.sleep(20);
        timer.stop();
        long endTime = System.nanoTime();

        assertThat(timer.getElapsedTime(), greaterThanOrEqualTo(MS_20));
        assertThat(timer.getElapsedTime(), lessThanOrEqualTo(endTime - startTime));
    }
}
