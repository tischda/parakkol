package polytech.parakkol.stats;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.concurrent.atomic.AtomicLong;

import org.junit.Before;
import org.junit.Test;

public final class MetricsTest {

    TestingMetrics metrics;

    final AtomicLong testingNanos = new AtomicLong(0);

    double[] fitness = new double[] { 0.71, 0.51, 0.41, 0.31, 0.21, 0.11 };

    public MetricsTest() {
        metrics = new TestingMetrics(fitness.length);
    }

    @Before
    public void recordTestMetrics() {
        metrics.reset();
        metrics.recordTestMetrics(fitness);
    }

    @Test
    public void testFitnessRecorded() {
        assertThat(metrics.fitness[1], equalTo(0.51));
        assertThat(metrics.fitness[3], equalTo(0.31));
    }

    @Test
    public void testElapsedRecorded() {
        assertThat(metrics.elapsed[1], equalTo(2L));
        assertThat(metrics.elapsed[3], equalTo(4L));
    }
}
