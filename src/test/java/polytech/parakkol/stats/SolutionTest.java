package polytech.parakkol.stats;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import polytech.parakkol.util.ColorEncoder;

public final class SolutionTest {

    Solution solution;

    @Before
    public void setUp() {
        int v1 = ColorEncoder.bin2gray(1);
        int v2 = ColorEncoder.bin2gray(2);
        int v3 = ColorEncoder.bin2gray(3);

        solution = new Solution(new int[] {v1, v2, v3}, 1.5);
    }

    @Test
    public void testGetFitness() {
        assertThat(solution.getFitness(), equalTo(1.5));
    }

    @Test
    public void testSolutionToString() {
        assertThat(solution.toString(), containsString("1.5 [ #000001 #000002 #000003 ]"));
    }
}


