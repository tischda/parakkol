package polytech.parakkol.stats;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

/**
 * Tests the Statistics object.
 *
 * @author 'Daniel TISCHER'
 *
 */
public final class StatisticsTest extends AbstractStatisticsTest {

    @Test
    public void testFitnessRecorded() {
        assertThat(stats.fitness[1][1], equalTo(0.52));
        assertThat(stats.fitness[3][1], equalTo(0.54));
    }

    @Test
    public void testElapsedRecorded() {
        assertThat(stats.elapsed[1][1], equalTo(8L));
        assertThat(stats.elapsed[3][1], equalTo(20L));
    }

    @Test
    public void testBest() {
        assertThat(stats.getOverallBestFitness(), equalTo(0.11));
    }

    @Test
    public void testMeanOfBest() {
        assertThat(stats.getMeanOfBestFitness(), equalTo(0.125));
    }

    @Test
    public void testMinByEvaluation() {
        assertThat(stats.fitnessMin[2], equalTo(0.41));
    }

    @Test
    public void testMaxByEvaluation() {
        assertThat(stats.fitnessMax[2], equalTo(0.44));
    }
}
