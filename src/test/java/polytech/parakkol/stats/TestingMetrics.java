package polytech.parakkol.stats;

import java.util.concurrent.atomic.AtomicLong;

public final class TestingMetrics extends Metrics {

    public TestingMetrics(int size) {
        super(size);
    }

    private static final AtomicLong testingNanos = new AtomicLong(0);

    @Override
    public long getElapsedTime() {
        return testingNanos.incrementAndGet();
    };

    @Override
    public void reset() {
        testingNanos.set(0L);
    }

    public void recordTestMetrics(double[] fitness) {
        for (int i = 0; i < fitness.length; i++) {
            record(new Solution(new int[] { 1, 2 }, fitness[i]));
        }
    }

    public static void resetTimer() {
        testingNanos.set(0L);
    }
}
