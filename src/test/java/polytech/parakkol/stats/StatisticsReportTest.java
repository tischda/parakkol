package polytech.parakkol.stats;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;

public final class StatisticsReportTest extends AbstractStatisticsTest {

    private static final double EPSILON = 1.0e-15;

    @Test
    public void testMeanByEvaluation() {
        assertThat(stats.meanByEvaluation(2), equalTo(0.425));
    }

    @Test
    public void testStddevByEvaluation() {
        assertThat(stats.stddevByEvaluation(0) - 0.15, lessThan(EPSILON));
    }

    // Times for each run:
    //
    //  0:   6
    //  1:  12
    //  2:  18
    //  3:  24
    //
    // Mean time is 15, the closest run is #1

    @Test
    public void testSelectRunClosestToMeanTime() {
        assertThat(stats.selectRunClosestToMeanTime(), equalTo(1));
    }

    @Test
    public void testPrintRecord() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        stats.printRecord(new PrintStream(baos), 1, "firstCol");
        assertThat(baos.toString(), containsString("firstCol 0.5175000000000001 0.017078251276599347 0.5004217487234007 0.5345782512765994 0.5 0.54"));
    }

    @Test
    public void testSaveFileHeader() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        stats.saveFileHeader(new PrintStream(baos), "abcdefghi", "jklmn");
        assertThat(baos.toString(), containsString("#           POPULATION_SIZE"));
        assertThat(baos.toString(), containsString("# abcdefghi"));
        assertThat(baos.toString(), containsString("#jklmn mean stddev mean-stddev mean+stddev min max"));
    }
}
