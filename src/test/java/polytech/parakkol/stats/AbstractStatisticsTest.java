package polytech.parakkol.stats;

import org.junit.Before;

import polytech.parakkol.ConfigManager.Keys;
import polytech.parakkol.ParametersGA;
import polytech.parakkol.Parameters;

import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValueFactory;

public abstract class AbstractStatisticsTest {
    final StatisticsReports stats;
    final Parameters params;

    // 4 runs of 6 evals
    double[][] fitness = new double[][] {
            { 0.8, 0.51, 0.43, 0.31, 0.21, 0.11 },
            { 0.8, 0.52, 0.44, 0.32, 0.22, 0.12 },
            { 0.5, 0.53, 0.41, 0.33, 0.23, 0.13 },
            { 0.8, 0.54, 0.42, 0.34, 0.24, 0.14 } };

    public AbstractStatisticsTest() {
        params = new ParametersGA(
            ConfigFactory.load().getConfig("parameters")
                .withValue(Keys.MAX_EVALUATIONS.toString(), ConfigValueFactory.fromAnyRef(fitness[0].length))
                .withValue(Keys.RUN_COUNT.toString(), ConfigValueFactory.fromAnyRef(fitness.length))
        );
        stats = new StatisticsReports(params);
    }

    @Before
    public void recordTestMetrics() {
        stats.reset();
        TestingMetrics.resetTimer();

        for (int i = 0; i < params.getRunCount(); i++) {
            TestingMetrics metrics = new TestingMetrics(fitness[i].length);
            metrics.recordTestMetrics(fitness[i]);
            stats.record(metrics);
        }
    }
}
