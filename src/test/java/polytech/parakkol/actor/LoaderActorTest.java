package polytech.parakkol.actor;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import polytech.parakkol.Loader;
import polytech.parakkol.actor.Message.RequestAFile;
import akka.actor.Props;
import akka.testkit.TestActorRef;

public final class LoaderActorTest extends AbstractActorTest {

    @Test
    public void testRequestAFile() {
        final Props props = Props.create(LoaderActor.class);
        final TestActorRef<LoaderActor> ref = TestActorRef.create(system, props);

        Loader loader = ref.underlyingActor().getLoader();
        assertThat(loader.toProcessCount(), equalTo(164));

        ref.tell(new RequestAFile(), ref);

        assertThat(loader.toProcessCount(), equalTo(163));
    }
}
