package polytech.parakkol.actor;

import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import polytech.parakkol.ConfigManager;
import polytech.parakkol.Loader;
import polytech.parakkol.model.Problem;
import akka.actor.Props;
import akka.testkit.TestActorRef;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public final class SolverActorTest extends AbstractActorTest {

    private static final String PROBLEM_INSTANCE = "3-2.dat";

    Problem problem;
    Config config;

    TestActorRef<LoaderActor> loaderRef;
    TestActorRef<StatisticsActor> statisticsRef;

    @Before
    public void setUp() throws IOException {
        ConfigManager configManager = new ConfigManager();
        config = configManager.load();

        String dataPath = ConfigFactory.load().getConfig("parakkol").getString("data_path");
        Loader loader = new Loader(dataPath);
        problem = loader.readFile(PROBLEM_INSTANCE);

        // create loader
        Props loaderProps = Props.create(LoaderActor.class);
        loaderRef = TestActorRef.create(system, loaderProps);

        // create statistics
        Props statisticsProps = Props.create(StatisticsActor.class, config);
        statisticsRef = TestActorRef.create(system, statisticsProps);
    }

    @Test
    public void testOnReceive() {
        // create solver
        Props solverProps = Props.create(SolverActor.class, "John", loaderRef, statisticsRef, config);
        TestActorRef<SolverActor> solverRef = TestActorRef.create(system, solverProps);
        SolverActor solverActor = solverRef.underlyingActor();

        // fix random
        Random random = new Random();
        random.setSeed(1L);
        solverActor.solver.setRandom(random);

        // run actor
        solverActor.solveProblem(problem);

        StatisticsActor statisticsActor = statisticsRef.underlyingActor();
        double fitness = statisticsActor.statistics.get(PROBLEM_INSTANCE).getMeanOfBestFitness();
        assertThat(fitness, lessThanOrEqualTo(0.03326999059424124));
    }
}
