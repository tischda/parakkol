package polytech.parakkol.actor;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import polytech.parakkol.ConfigManager;
import polytech.parakkol.ConfigManager.Keys;
import polytech.parakkol.actor.Message.SolverMetrics;
import polytech.parakkol.stats.Metrics;
import polytech.parakkol.stats.Solution;
import akka.actor.Props;
import akka.testkit.TestActorRef;

import com.typesafe.config.Config;

public final class StatisticsActorTest extends AbstractActorTest {

    private static final String TEST_PROB_0 = "testProblem0";
    private static final String TEST_PROB_1 = "testProblem1";
    private static final String TEST_PROB_2 = "testProblem2";
    private static final double EXPECTED_FITNESS = 0.123456789;
    private static final double SOL_FITNESS_1 = 0.25;
    private static final double SOL_FITNESS_2 = 0.75;
    private static final double SOL_FITNESS_3 = 0.10;
    private static final double SOL_FITNESS_4 = 0.30;

    Config config;
    int maxEvaluations;
    StatisticsActor statisticsActor;

    @Before
    public void setUp() {
        ConfigManager configManager = new ConfigManager();
        config = configManager.load();
        maxEvaluations = configManager.getIntConfigValue(Keys.MAX_EVALUATIONS);

        Props props = Props.create(StatisticsActor.class, config);
        TestActorRef<StatisticsActor> ref = TestActorRef.create(system, props);
        statisticsActor = ref.underlyingActor();
    }

    @Test
    public void testOnReceive() {
        Solution solution = new Solution(new int[] {1, 2, 3}, EXPECTED_FITNESS);

        Metrics metrics = new Metrics(maxEvaluations);
        metrics.start();
        metrics.record(solution);

        statisticsActor.onReceive(new SolverMetrics(TEST_PROB_0, 0, metrics));

        assertThat(statisticsActor.statistics.get(TEST_PROB_0).getOverallBestFitness(), equalTo(EXPECTED_FITNESS));
    }

    @Test
    public void testRecordSolverMetrics() {
        Solution[] solutions = new Solution[] {
                new Solution(new int[] {1, 2, 3}, SOL_FITNESS_1),
                new Solution(new int[] {4, 5, 6}, SOL_FITNESS_2),
                new Solution(new int[] {7, 8, 9}, SOL_FITNESS_3),
                new Solution(new int[] {3, 2, 0}, SOL_FITNESS_4) };

        Metrics[] metrics = new Metrics[solutions.length];
        for (int i = 0; i < 4; i++) {
            Metrics m = new Metrics(maxEvaluations );
            m.start();
            m.record(solutions[i]);
            metrics[i] = m;
        }

        statisticsActor.recordSolverMetrics(new SolverMetrics(TEST_PROB_1, 0, metrics[0]));
        statisticsActor.recordSolverMetrics(new SolverMetrics(TEST_PROB_1, 1, metrics[1]));

        statisticsActor.recordSolverMetrics(new SolverMetrics(TEST_PROB_2, 0, metrics[2]));
        statisticsActor.recordSolverMetrics(new SolverMetrics(TEST_PROB_2, 1, metrics[3]));

        assertThat(statisticsActor.statistics.get(TEST_PROB_1).getOverallBestFitness(), equalTo(SOL_FITNESS_1));
        assertThat(statisticsActor.statistics.get(TEST_PROB_2).getOverallBestFitness(), equalTo(SOL_FITNESS_3));
        assertThat(statisticsActor.statistics.get(TEST_PROB_1).getMeanOfBestFitness(), equalTo((SOL_FITNESS_1 + SOL_FITNESS_2)/2));

        statisticsActor.clearStatistics();
        assertNull(statisticsActor.statistics.get(TEST_PROB_1));
    }
}
