package polytech.parakkol.actor;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import scala.collection.Iterator;
import scala.concurrent.duration.Duration;
import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.Inbox;
import akka.actor.Kill;
import akka.actor.Props;
import akka.actor.Terminated;
import akka.actor.UntypedActor;
import akka.routing.ActorRefRoutee;
import akka.routing.AddRoutee;
import akka.routing.Broadcast;
import akka.routing.BroadcastPool;
import akka.routing.BroadcastRoutingLogic;
import akka.routing.Routee;
import akka.routing.Router;
import akka.testkit.TestActorRef;
import akka.testkit.TestProbe;

public class LearningTest extends AbstractActorTest {

    private final static Logger log = LoggerFactory.getLogger(LearningTest.class);
    private static final int ACTOR_COUNT = 3;
    private static final String BROADCAST_MSG = "this is a broadcast message";
    private static final String TEST_MSG = "anybody?";

    /**
     * Test actor that registers and logs messages received.
     */
    static class PrintActor extends UntypedActor {
        Vector<String> messageList;

        public PrintActor(Vector<String> list) {
            messageList = list;
        }

        @Override
        public void onReceive(Object message) {
            log.info(String.format("PrintActor %s received message ’%s’",
                    getSelf().path().name(), message));
            if (message instanceof String) {
                messageList.add((String) message);
            }
        }
    }

    @Before
    public void setUp() {
        log.info("----------------------------------------");
    }

    /**
     * Group - The routee actors are created externally to the router and the
     * router sends messages to the specified path using actor selection,
     * without watching for termination.
     */
    @Test
    public void testRouterBroadcastGroup() throws InterruptedException {
        Inbox routerInbox = Inbox.create(system);
        Vector<String> messages = new Vector<String>();
        Props props = Props.create(PrintActor.class, messages);

        // create routees
        List<Routee> routees = new ArrayList<Routee>();
        for (int i = 0; i < ACTOR_COUNT; i++) {
          ActorRef r = system.actorOf(props, "routee" + i);
          routerInbox.watch(r);
          routees.add(new ActorRefRoutee(r));
        }

        // create router
        Router router = new Router(new BroadcastRoutingLogic(), routees);
        assertThat(router.routees().size(), equalTo(ACTOR_COUNT));

        // add routee, see "Management Messages", p.114 akka doc (since 2.3.0)
        // do not forget to reassign router from return value!!!
        ActorRef other = TestActorRef.create(system, props, "r4");
        router = router.addRoutee(other);
        assertThat(router.routees().size(), equalTo(ACTOR_COUNT + 1));

        // watch routee r4 lifecycle
        TestProbe probe = new TestProbe(system);
        probe.watch(other);

        // test broadcast, note that the method is route(), not tell()
        router.route(BROADCAST_MSG, ActorRef.noSender());

        // wait for messages to arrive
        Thread.sleep(10);

        assertThat(messages.size(), equalTo(ACTOR_COUNT + 1));
        assertThat(messages, hasItem(equalTo(BROADCAST_MSG)));

        // send Kill to all routees (wrapped in a Broadcast !)
        router.route(new Broadcast(Kill.getInstance()), ActorRef.noSender());

        assertTrue(routerInbox.receive(Duration.create(1, TimeUnit.SECONDS)) instanceof Terminated);
        probe.expectTerminated(other, Duration.create(1, TimeUnit.SECONDS));

        // remove corpses, and do not forget to reasign router !!!
        Iterator<Routee> it = router.routees().toIterator();
        while (it.hasNext()) {
            router = router.removeRoutee(it.next());
        }

        // reuse router (this works only for group type)
        other = TestActorRef.create(system, props, "r5");
        router = router.addRoutee(other);
        router.route(TEST_MSG, ActorRef.noSender());
        assertThat(messages, hasItem(equalTo(TEST_MSG)));
    }

    /**
     * Pool - The router creates routees as child actors and removes them from
     * the router if they terminate.
     *
     * In the event that all children of a pool router have terminated the router
     * will terminate itself unless it is a dynamic router.
     *
     * @throws Exception
     */
    @Test
    public void testRouterBroadcastPool() throws InterruptedException {
        Vector<String> messages = new Vector<String>();
        Props props = Props.create(PrintActor.class, messages);

        // create router with pool
        ActorRef router = system.actorOf(
                new BroadcastPool(ACTOR_COUNT).props(props), "router");

        // add routee, see "Management Messages", p.125 akka doc (since 2.3.0)
        ActorRef other = TestActorRef.create(system, props, "poolr4");
        router.tell(new AddRoutee(new ActorRefRoutee(other)), ActorRef.noSender());

        // watch actor lifecycle
        TestProbe routerProbe = new TestProbe(system);
        routerProbe.watch(router);

        TestProbe otherProbe = new TestProbe(system);
        otherProbe.watch(other);

        // Wait for "addRoutee" message to be received
        // otherwise tell() will not reach poolr4 (cf. p.125)
        Thread.sleep(10);

        // test broadcast message via router
        router.tell(BROADCAST_MSG, ActorRef.noSender());

        // wait for messages to arrive
        Thread.sleep(10);

        assertThat(messages.size(), equalTo(ACTOR_COUNT + 1));
        assertThat(messages, hasItem(equalTo(BROADCAST_MSG)));

        // send Kill to all routees (wrapped in a Broadcast !)
        router.tell(new Broadcast(Kill.getInstance()), ActorRef.noSender());

        // routee should be terminated
        otherProbe.expectTerminated(other, Duration.create(1, TimeUnit.SECONDS));

        // In the event that all children of a pool router have terminated
        // the router will terminate itself unless it is a dynamic router (p.116)
        routerProbe.expectTerminated(router, Duration.create(1, TimeUnit.SECONDS));
    }

    /**
     * Selection by name, the problem here is naming consistency.
     */
    @Test
    public void testActorSelection() {
        Vector<String> messages = new Vector<String>();
        Props props = Props.create(PrintActor.class, messages);

        // create actors
        ActorRef actor1 = TestActorRef.create(system, props, "println1");
        ActorRef actor2 = TestActorRef.create(system, props, "println2");

        // watch actor lifecycle
        TestProbe probe = new TestProbe(system);
        probe.watch(actor1);
        probe.watch(actor2);

        // test broadcast to selection
        ActorSelection selection = system.actorSelection("/user/print*");
        selection.tell(BROADCAST_MSG, ActorRef.noSender());
        assertThat(messages.size(), equalTo(2));
        assertThat(messages, hasItem(equalTo(BROADCAST_MSG)));

        // kill
        selection.tell(Kill.getInstance(), ActorRef.noSender());

        // actors should be terminated
        probe.expectTerminated(actor1, Duration.create(1, TimeUnit.SECONDS));
        probe.expectTerminated(actor2, Duration.create(1, TimeUnit.SECONDS));
    }

    // TODO check if you understand something from page 75 of akka doc

    // TODO gracefullstop
}
