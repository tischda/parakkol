package polytech.parakkol.actor;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Before;
import org.junit.Test;

import polytech.parakkol.ConfigManager;
import polytech.parakkol.actor.Message.SolverStatus;
import polytech.parakkol.actor.fx.StatusListUpdateable;
import akka.actor.Props;
import akka.testkit.TestActorRef;

public final class MasterActorTest extends AbstractActorTest {

    MasterActor masterActor;

    @Before
    public void setUp() {
        ConfigManager configManager = new ConfigManager();
        Props props = Props.create(MasterActor.class, configManager.load());
        TestActorRef<MasterActor> ref = TestActorRef.create(system, props);
        masterActor = ref.underlyingActor();
    }

    @Test
    public void testReceiveUpdateable() {
        final AtomicInteger counter = new AtomicInteger(0);
        StatusListUpdateable updateable = new StatusListUpdateable() {
            @Override
            public void update(Object solverStatus) {
                counter.incrementAndGet();
            }
        };
        masterActor.onReceive(updateable);

        masterActor.startTimer();
        masterActor.onReceive(new SolverStatus("", "", ""));

        assertThat(counter.get(), equalTo(1));
    }
}
