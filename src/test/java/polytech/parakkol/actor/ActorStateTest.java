package polytech.parakkol.actor;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class ActorStateTest {

    @Test
    public void testStringValue() {
        assertThat(ActorState.STARTING.toString(), equalTo("Starting"));
    }
}
