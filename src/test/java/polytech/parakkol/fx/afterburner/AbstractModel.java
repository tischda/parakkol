package polytech.parakkol.fx.afterburner;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

public abstract class AbstractModel {

    boolean initialized = false;
    boolean destroyed = false;

    @Inject String injectedProperty;
    @Inject String doesNotExists;

    @Inject Service injectedObject;

    @PostConstruct
    public void init() {
        this.initialized = true;
    }

    @PreDestroy
    public void destroy() {
        this.destroyed = true;
    }
}
