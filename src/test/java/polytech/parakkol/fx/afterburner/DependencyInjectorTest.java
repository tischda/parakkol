package polytech.parakkol.fx.afterburner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * Tests the afterburner injection framework. Beware of injecting objects in the
 * controller because its instantiation happens from the view in another thread
 * and DependencyInjector may be shutting down from a previous run. So you may
 * not get the Controller you want. This is only an issue for testing because
 * applications will not shutdown the DependencyInjector until they are closed.
 *
 * Therefore, we need to make sure each test has dedicated models objects. Be
 * also carefull setting system properties. Example: System.setProperties(null)
 * does remove important properties such as "user.dir" which will make I/O based
 * tests fail.
 *
 * @author 'Daniel TISCHER'
 *
 */
@RunWith(Parameterized.class)
public final class DependencyInjectorTest {

    @Parameterized.Parameters
    public static List<Object[]> data() {
        // Run this test 10 times
        return Arrays.asList(new Object[10][0]);
    }

    @Before
    public void setUp() {
        System.clearProperty("injectedProperty");
        InstantiationModel.INSTANCE_COUNT.set(0);
    }

    @After
    public void tearDown() {
        DependencyInjector.shutdownAll();
        System.clearProperty("injectedProperty");
    }

    @Test
    public void testInjectedCached() {
        SampleView view = (SampleView) DependencyInjector.getInjected(SampleView.class);
        assertNotNull(view.concreteModel);

        AnotherView anotherView = (AnotherView) DependencyInjector.getInjected(AnotherView.class);

        assertSame("cache returns same object", anotherView.concreteModel, view.concreteModel);
        assertThat("only one instance of model", InstantiationModel.getNumberOfInstances(), equalTo(1));
    }

    @Test
    public void testPropertiesResolved() {
        System.clearProperty("injectedProperty");
        Properties externalProperties = new Properties();
        externalProperties.put("injectedProperty", "theValue");
        externalProperties.put("injectedObject", "wants to ba a Service");
        DependencyInjector.initializePropertiesWith(externalProperties);

        PropertyModel model = (PropertyModel) DependencyInjector.getInjected(PropertyModel.class);
        assertThat(model.injectedProperty, equalTo("theValue"));
        assertNull("unresolved properties remain null", model.doesNotExists);

        assertTrue("inject by type, not by actorName", model.injectedObject instanceof Service);

        externalProperties.put("injectedProperty", "anotherValue");
        assertThat("injected properties are safe", model.injectedProperty, equalTo("theValue"));

        System.setProperty("injectedProperty", "anotherValue");
        model = (PropertyModel) DependencyInjector.getInjected(PropertyModel.class);
        assertThat("injected cannot be reinjected", model.injectedProperty, equalTo("theValue"));
    }

    @Test
    public void testSystemPropertiesOverride() {
        Properties externalProperties = new Properties();
        externalProperties.put("injectedProperty", "theValue");
        DependencyInjector.initializePropertiesWith(externalProperties);

        System.setProperty("injectedProperty", "anotherValue");
        PropertyModel model = (PropertyModel) DependencyInjector.getInjected(PropertyModel.class);
        assertThat("system properties override properties", model.injectedProperty, equalTo("anotherValue"));
    }

    @Test
    public void testExistingObjectIsInjected() {
        Service service = new Service();
        service.injectedProperty = "I am unique";
        DependencyInjector.initializeCacheWith(Service.class, service);

        PropertyModel model = (PropertyModel) DependencyInjector.getInjected(PropertyModel.class);
        assertThat(model.injectedObject.injectedProperty, equalTo("I am unique"));
        assertSame(model.injectedObject, service);
    }

    @Test
    public void testPostConstruct() {
        PostConstructModel model = (PostConstructModel) DependencyInjector.getInjected(PostConstructModel.class);
        DependencyInjector.getInjected(PostConstructModel.class);
        assertThat(model.getInitializationCount(), equalTo(1));
    }

    @Test
    public void testPreDestroy() {
        PreDestroyModel model = (PreDestroyModel) DependencyInjector.getInjected(PreDestroyModel.class);
        DependencyInjector.shutdownAll();
        DependencyInjector.shutdownAll();
        assertThat("destroy called exactly once", model.getDestructionCount(), equalTo(1));
    }
}
