package polytech.parakkol.fx.afterburner;

import java.util.concurrent.atomic.AtomicInteger;

public class InstantiationModel extends AbstractModel {

    static volatile AtomicInteger INSTANCE_COUNT = new AtomicInteger(0);

    public InstantiationModel() {
        INSTANCE_COUNT.incrementAndGet();
    }

    public static int getNumberOfInstances() {
        return INSTANCE_COUNT.get();
    }
}
