package polytech.parakkol.fx.afterburner;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Properties;

import javafx.scene.Parent;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public final class SampleViewTest {

    private SampleView view;

    @Before
    public void setUp() {
        view = new SampleView();

        Properties externalProperties = new Properties();
        externalProperties.put("injectedProperty", "theValue");
        DependencyInjector.initializePropertiesWith(externalProperties);
    }

    @After
    public void tearDown() {
        DependencyInjector.shutdownAll();
    }

    @Test
    public void testGetView() {
        Parent parent = view.getView();
        assertNotNull(parent);
        assertThat(parent.getStylesheets().get(0), containsString("Sample.css"));
    }

    @Test
    public void testGetController() {
        Object object = view.getController();
        assertNotNull(object);
        assertTrue("object is a controller", object instanceof SampleController);
    }
}
