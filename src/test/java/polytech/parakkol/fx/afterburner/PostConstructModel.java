package polytech.parakkol.fx.afterburner;

import java.util.concurrent.atomic.AtomicInteger;

public class PostConstructModel extends AbstractModel {

    private final AtomicInteger initializationCounter = new AtomicInteger(0);

    @Override
    public void init() {
        initializationCounter.incrementAndGet();
    }

    public int getInitializationCount() {
        return initializationCounter.get();
    }
}
