package polytech.parakkol.fx.afterburner;

import java.util.concurrent.atomic.AtomicInteger;

public class PreDestroyModel extends AbstractModel {

    private final AtomicInteger destructionCounter = new AtomicInteger(0);

    @Override
    public void destroy() {
        destructionCounter.incrementAndGet();
    }

    public int getDestructionCount() {
        return destructionCounter.get();
    }
}
