package polytech.parakkol.fx.master;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import javafx.application.Application;
import javafx.stage.Stage;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import polytech.parakkol.ConfigManager;
import polytech.parakkol.ParametersGA;
import polytech.parakkol.fx.afterburner.DependencyInjector;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.testkit.JavaTestKit;
import akka.testkit.TestProbe;

public final class MasterControllerTest extends Application {

    static ActorSystem system;

    @BeforeClass
    public static void setup() {
        // http://stackoverflow.com/questions/11385604/how-do-you-unit-test-a-javafx-controller-with-junit
        Thread t = new Thread("JavaFX Init Thread") {
            @Override
            public void run() {
//                Application.launch(MasterControllerTest.class, new String[0]);
            }
        };
        t.setDaemon(true);
        t.start();

        system = ActorSystem.create();

        // Master instantiates Details, which injects an ActorRef
        // so here we provide one...
        final TestProbe probe = TestProbe.apply(system);
        DependencyInjector.initializeCacheWith(ActorRef.class, probe.ref());
    }

    @AfterClass
    public static void teardown() {
        JavaTestKit.shutdownActorSystem(system);
        system = null;

        // exit JavaFX
//        Platform.exit();
    }

    @Test
    @Ignore("Launchin a JavaFX application fails on CentOS: Gtk-WARNING **: cannot open display:")
    public void testParametersLoaded() {
        ConfigManager configManager = new ConfigManager();
        ParametersGA params = new ParametersGA(configManager.load());

        MasterController masterController = (MasterController) new MasterView().getController();

        assertThat(masterController.getMaxEvaluations(), equalTo(params.getMaxEvaluations()));
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        // TODO Auto-generated method stub
    }
}
