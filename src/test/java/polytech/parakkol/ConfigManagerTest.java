package polytech.parakkol;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import polytech.parakkol.ConfigManager.Keys;

public class ConfigManagerTest {

    @Test
    public void testParametersToJSON() {
        ConfigManager configManager = new ConfigManager();
        configManager.load();

        System.out.println(configManager.toJSON());
        assertThat(configManager.toJSON(), containsString(Keys.POPULATION_SIZE.toString() + "\":900"));
    }

    @Test
    public void testParametersNotForProduction() {
        ConfigManager configManager = new ConfigManager();
        configManager.load();
        assertThat(configManager.getIntConfigValue(Keys.RUN_COUNT), equalTo(5));
    }
}
