package polytech.parakkol;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

import org.junit.Test;

/**
 * Tests that the parameters are properly initialized.
 *
 * @author 'Daniel TISCHER'
 *
 */
public final class ParametersTest {

    @Test
    public void testParametersToString() {
        ConfigManager configManager = new ConfigManager();
        Parameters params = new ParametersGA(configManager.load());
        assertThat(params.toString(), containsString("  RUN_COUNT=5"));
    }
}
