Difficultés rencontrées
-----------------------

L'intégration de nouvelles technologies comporte toujours une part de risques,
par exemple lorsque la communauté open-source s'en désintéresse : QT Jambi, mais
aussi Maven, n'ont pas connu de mise à jour significative depuis plus de deux
ans.

Parmi les technologies adoptées, voici ce qui a posé problème.

### Gradle, système de build et documentation

Avec Gradle, j'ai eu des difficultés pour convertir le fichier Maven : la
commande était '`gradle init`' et non '`gradle setupBuild`' comme spécifié dans
la documentation. Ensuite on se demande d'abord à quoi sert le wrapper `gradlew`
[^8], puis on l'apprécie sur MacOS où macports ne propose qu'une version
antique.

De Maven à Gradle, certaines conventions changent (`target` &rarr; `build`),
mais on trouve les solutions rapidement [^9]. Notons enfin que Gradle se base sur
[Groovy](http://groovy.codehaus.org/), un langage de la JVM compilé à la volée.

[^8]: Why gradle wrapper should be commited to vcs? [[http://bit.ly/1idmari](http://stackoverflow.com/questions/20348451/why-gradle-wrapper-should-be-commited-to-vcs)]
[^9]: Resources from src/main/resources not included for test [[http://bit.ly/NoLi2T](http://forums.gradle.org/gradle/topics/resources_from_src_main_resources_not_included_for_test)]

Le système de build s'est ensuite compliqué avec l'intégration des outils pour
générer la documentation (UmlGraph, Graphviz, PlotUtils, GhostScript, Pandoc,
\LaTeX) et quelques problèmes à corriger (crash de GraphViz[^10] et patch soumis
à UmlGraph).

[^10]: 0002421: dot crashes when label empty [[http://bit.ly/1idqozk](http://www.graphviz.org/mantisbt/view.php?id=2421)]

### Akka

Au début, j'ai eu des problèmes pour lancer le kernel Akka (avec '`akka.bat`' et
non pas '`start.bat`'). Mais finalement, cette approche s'est avérée sans
intérêt.[^11]

[^11]: What's the point of the Akka Microkernel using Java and Maven? [[http://bit.ly/1idpenl](http://stackoverflow.com/questions/17343287/whats-the-point-of-the-akka-microkernel-using-java-and-maven)]

Avec Akka, on entre dans un autre univers, plus accessible à ceux qui maitrisent
la programmation fonctionnelle en Scala, car la plupart des concepts sont
illustrés dans ce langage.

Pour l'apprentissage j'étais dépassé par l'ampleur du framework : acteurs
distribués, tolérance de pannes, clustering, routage, réseau NIO, automates
finis, mémoire transactionnelle (Software Transactional Memory, STM). Cette
richesse fonctionnelle demande du temps, car il faut assimiler les concepts
sous-jacents avant de pouvoir utiliser l'API comme il faut.

### JavaFX 2

Pour JavaFX, la prise en main a été rapide. Il a cependant fallu réfléchir à la
façon de structurer l'interface, et à une convention de nommage pour les
composants de l'UI.

Une fois le développement entamé, il était difficile de déplacer les éléments
d'un groupe MVC vers un autre. En particulier, il faut se méfier des copier-coller
de composants dans l'éditeur graphique, car on duplique ainsi leurs
identifiants et cela empêche l'interface de s'initialiser.

Les parties gauche et droite du splitPane, composées chacune de leurs propre vue
et contrôleur, étaient assemblées programmatiquement. Cela permettait de bien
modulariser le code, mais avec la contrainte qu'un contrôleur ne peut interagir
qu'avec sa propre vue.

Dans la composition programmatique, les relations de taille "parent-enfant" ne
sont plus gérées par l'éditeur. Le problème est qu'elles sont supprimées
lorsqu'on y recharge la vue.

![Composition et ajustement de l'interface graphique](images/fx-resize-commented.png)

JavaFX est single-threaded (sauf pour la partie multimédia) et ne gère pas les
accès concurrents à son graphe de scène, même par ses propres objets[^12]. Il
faut forcer l'exécution sur le thread d'application :

~~~java
Platform.runLater(new Runnable() {
  @Override public void run() {
    data.add(item);
  }
});
~~~

[^12]: NullPointerException in JavaFX initialize method [[http://bit.ly/1idrHy5](http://stackoverflow.com/questions/15304203/nullpointerexception-in-javafx-initialize-method)]

Dans l'API de Java 8 (non finalisée), la fonction dont j'avais besoin pour
synchroniser les accès était commentée :

![Code de synchronisation sur le thread d'application](images/runandwait.png)

Dans l'application, la liste est actualisée suite à deux événements : Le
démarrage d'un nouveau solveur et la réception de statistiques. Pour mettre à
jour ces métriques, l'entrée correspondante au solveur doit exister. Akka
garantit l'ordre d'envoi et de réception des messages, j'attendais la même chose
de JavaFX. Mais avec `runLater()` cette garantie s'effaçait. J'ai finalement
réglé le problème en important `com.sun.javafx.application.PlatformImpl`
qui permettait d'appeler `runAndWait()`.

Cette contrainte de l'UI remettait en cause une partie de l'architecture :
envoyer des données en temps réel que l'interface ne savait pas traiter ? Par
conséquent, lorsque les solvers avaient terminé le travail, l'affichage n'était
encore qu'à 80% de sa mise à jour.

### Injection de dépendances

J'étais fier d'avoir su intégrer afterburner.fx, mais ce n'est qu'en avançant
dans le projet que j'ai découvert ses limitations. Jusqu'alors, je n'injectais
qu'un seul acteur (`MasterActor`). Quand j'ai voulu en ajouter d'autres, je me
suis rendu compte que l'injection se faisant par type, et Akka ne publiant que
des `ActorRef`, j'étais limité à l'injection d'un seul et unique acteur.

### Paramètres de configuration

La gestion des paramètres dans le code original se basait sur
[JCommander](http://jcommander.org/). Je ne voulais pas imposer de tels détails
d'implémentation à toutes mes classes paramétrées.

J'ai pensé à réutiliser la configuration d'Akka, même si pour la mise à jour,
l'objet immutable `Config` avait ses limitations. Je l'ai donc encapsulé dans un
objet `Parameters` mais j'avais oublié qu'obtenir un paramètre d'une table de
hachage ferait une comparaison de chaîne, opération coûteuse ! (en Scala, c'est
la double peine). Le code suivant (ligne 4) ralentissait l'application de façon
significative :

~~~java
EvaluationListener listener = new EvaluationListener() {
    @Override
    public void fitnessEvaluated(Palette p) {
        if (metrics.getCurrentEvaluationCount() < params.getMaxEvaluations()) {
            metrics.record(new Solution(p.getColors(), p.getFitness()));
        }
    }
};
~~~

![Profiling d'un problème de performances](images/typesafe-config-slow.png)

La solution a été de copier les paramètres dans des champs natifs dans le
constructeur. D'ailleurs une classe sans getters et des champs `public final` ne
serait pas une si mauvaise idée (cela va à l'encontre des règles de CheckStyle).

### Modélisation des statistiques

Les métriques partielles de chaque solver devaient être regroupées pour calculer
les moyennes et l'écart type. Le dilemme était de tout stocker en mémoire, ou de
calculer les moyennes sur une fenêtre de temps, comme cela est implémenté dans
[Metrics](http://metrics.codahale.com/). J'ai choisi la première approche pour
rester fidèle aux spécifications d'origine.

J'ai recopié les métriques de chaque run dans un tableau statistique. Faire une
copie profonde des tableaux ne me plaisait pas (pour des raisons de mémoire et
de performances), j'ai donc copié la référence de l'objet `runMetrics`, bien
qu'il ne fût pas immutable. A ce propos, même le wrapper
`Collections.unmodifiableList()` ne fait que décorer l'objet liste original
[^13] \cite{naftalin_java_2007} :

~~~java
public boolean add(Object object) {
    throw new UnsupportedOperationException();
}
~~~

[^13]: Java Immutable Collections [[http://bit.ly/1iiE248](http://stackoverflow.com/questions/7713274/java-immutable-collections)]

Pour Akka, dans un système réparti, l'immutabilité a son importance. Les
messages étant sérialisés, une modification sur l'objet source ne serait pas
répercutée sur la copie distante, et l'état du système deviendrait incohérent.
Même si l'on n'a pas prévu de modifier l'objet source, l'immutabilité est un
garde-fou qui, si j'avais verrouillé `Metrics.record()`, m'aurait permis de
détecter le problème suivant.

### Problèmes détectés à l'exécution

Suivant les préceptes des experts dans ce domaine \cite{goetz_java_2006,
hunt_java_2012}, j'ai fait des tests de charge et me suis rendu compte que
l'application ralentissait progressivement au bout d'une centaine de runs.

![Memory Leak](images/monitoring-leak.png)

On voit sur ce graphique que des objets étaient alloués mais pas libérés. Une
instruction `System.gc()` dans le boucle n'ayant pas eu d'effet, il s'agissait
donc bien d'un "memory leak", un problème qui aurait causé tôt ou tard une
exception de mémoire. Je ne voyais cependant pas le rapport immédiat avec le
ralentissement jusqu'à ce que j'étudie ce qui modifiait l'état du Heap entre
deux exécutions.

J'ai trouvé la solution dans le listener d'évaluation (encore lui !) : Pour
chaque résolution, j'attachais un nouveau listener à mon `Evaluator`, cependant
j'oubliais de l'enlever à la fin du traitement. Chaque nouveau problème ajoutait
donc un listener à la liste. Après 1000 itérations, notre `Evaluator` avait fort
à faire, et surtout, il gardait une référence sur tous les objets, empêchant
ainsi leur collecte.


Perspectives d'évolution
------------------------

### Fonctionnalités

Sur ce projet, nous avons réalisé une preuve de concept pour la parallélisation.
Cependant, pour que le programme soit utile, il faut présenter les résultats de
manière à faciliter la prise de décision.

On pourrait par exemple comparer les résultats entre deux exécutions avec des
paramètres différents. L'acteur statistique persisterait les données dans des
fichiers, ce qui permettrait de décharger la mémoire. Un autre type d'acteur
pourrait ensuite s'occuper de corréler les données et de tracer les courbes.

A ce propos, JavaFX contient des composants pour la visualisation graphique de
données. Un double-clic sur une instance du problème afficherait la courbe
correspondante en substitution de la liste.

Une approche encore plus poussée serait l'intégration d'outils de monitoring
tels que [Graphite](http://graphite.wikidot.com/screen-shots) ou
[Kibana](http://www.elasticsearch.org/overview/kibana/). Ces solutions
permettent des statistiques avancées en temps réel, par exemple en combinaison
avec [Metrics](http://metrics.codahale.com/)[^14], on n'a donc plus de besoin de
stocker les données.

[^14]: Metrics, Metrics, Everywhere - Coda Hale [[http://bit.ly/OzKvgL](http://www.youtube.com/watch?v=czes-oa0yik)]

Pour satisfaire les objectifs initiaux des travaux pratiques, l'application
devrait gérer plusieurs algorithmes. Les fonctions de comparaison seraient donc
doublement utiles. Une difficulté serait l'édition de paramètres différents en
type et en nombre. Dans l'UI, il faudrait remplacer la partie gauche de l'écran
par une boîte de dialogue ou une liste de propriétés de taille variable.

### Améliorations

L'orientation future du projet déterminera si la partie statistique sort du code
ou s'il faut l'améliorer. En attendant, on peut rendre l'objet `runMetrics`
immutable et restreindre le nombre de mises à jour de l'UI par une stratégie
"pull" plutôt que "push". Ainsi, à l'aide d'un scheduler, l'UI demanderait
toutes les 0.2 secondes (max. temps d'exécution d'un run) un "status update" à
`StatisticsActor`.

Cela simplifierait l'initialisation du Master qui ne ferait plus la mise à jour
des composants, ce qui enfreignait la loi d'immutabilité d'Akka et n'aurait pas
fonctionné en version distribuée.

Une autre amélioration concerne l'arrêt des solveurs. Actuellement, on fait un
broadcast sur le nom de l'acteur (`/user/masterActor/solverActor*`), ce qui
nécessite une convention de nommage. Une alternative serait de commander au
`LoaderActor` qu'il vide sa liste de fichiers à traiter. Les prochaines requêtes
des `SolverActor` renverraient alors une pilule de poison, une façon élégante
d'ordonner un suicide collectif.

Enfin, il faudrait réexaminer le cycle de vie des acteurs, en particulier la
phase preStart(). Pendant les tests, il m'est arrivé qu'une exception soit
provoquée dans un solveur. Dans ce cas, le superviseur exécute l'action par
défaut qui est de redémarrer l'acteur. Mais la première chose que faisait
`SolverActor` en s'initialisant était de demander un autre problème au Loader,
générant ainsi une cascade d'exceptions.

Dans la philosophie "let it crash", si l'on prend le problème à l'envers, le
redémarrage devient une opération standard, et une perspective à envisager pour
implémenter la fonction "Clear Statistics".

### Akka

Il reste des points à étudier dans le framework Akka. Tout d'abord, il faut
s'assurer que l'intégrité des statistiques est garantie en cas de crash (tous
les problèmes sont traités, sans doublons). On s'intéressera donc aux stratégies
de supervision pour maintenir le système dans un état cohérent.

Par conséquent, Akka serait bien adapté pour le Cloud où les noeuds sont
instables. Avec la distribution des acteurs sur plusieurs serveurs, nous
arriverions à des temps de réponse intéressants pour enrichir l'application d'un
"self-tuning" permettant de déterminer les meilleurs paramètres d'un algorithme.

Pour la partie répartie, Akka propose encore d'autres concepts comme les Agents
(inspirés de [Clojure](http://clojure.org/agents)) et les Clusters. On pourrait
identifier les solvers avec un `UniqueIdActor` et implémenter les paramètres
comme une extension Akka.

Si l'on partait sur une version répartie, il faudrait repenser l'interface
utilisateur pour qu'elle intègre ces concepts, par exemple un mode console pour
les solveurs.

### Problèmes connus

Quelques tickets sont encore référencés dans JIRA, parmi lesquels des problèmes
de rafraîchissement de la liste, des boutons à redimensionner sous MacOS, et des
questions triviales de configuration.


Conclusion
----------

### Partie projet

Le projet s'est bien déroulé, et je suis satisfait du bon fonctionnement des
outils, même si l'infrastructure était surdimensionnée pour un tel
développement. Il aura fallu se familiariser avec Gradle, définir un workflow
Kanban et réaliser une chaîne de production pour la génération de PDF à partir
de Markdown en passant par \LaTeX. J'ai aussi approfondi mes connaissances de
GIT et de JIRA, des outils très performants pour la collaboration en équipe,
aspect non mis en pratique pour ce projet individuel.

J'insiste sur l'importance des outils, car ils permettent de s'affranchir des
rapports d'activité coûteux imposés dans les projets classiques. Ici tout est
visible en temps réel, les décisions se prennent rapidement, au bon moment,
quand l'information est disponible, et non pas à l'avance sur la base
d'estimations.

Il faut cependant accepter de travailler dans une dynamique de changement, ce
qui n'est pas toujours possible ou accepté par le client.

Enfin, les nombreuses possibilités d'évolution du projet montrent l'importance
de choisir ses priorités, car en si peu de temps on ne pouvait pas tout faire.
Pour une preuve de concept, et l'acquisition de nouvelles compétences, les
objectifs ont toutefois été pleinement atteints.

### Librairies et QT

Contrairement à C++ qui fait appel à la STL (Standard Template Library), le
langage Java propose depuis longtemps des conteneurs dynamiques[^15], par
exemple `TreeSet` qui matérialise une population dans l'algorithme génétique et
`ConcurrentHashMap` pour le cache d'injection de dépendances. Akka et surtout
JavaFX y ajoutent leurs propres structures.

[^15]:Java 8 propose même des [itérateurs parallèles](http://docs.oracle.com/javase/tutorial/collections/streams/parallelism.html) pour les Streams.

La communication asynchrone entre objets et le parallélisme ont été largement
couverts par Akka, mais on peut aussi mentionner la gestion des événements dans
le modèle MVC de JavaFX[^16] qui s'apparente aux "Signals et slots" de QT.

[^16]: Using JavaFX Properties and Binding [[http://bit.ly/1gCSaRi](http://docs.oracle.com/javafx/2/binding/jfxpub-binding.htm)],
JavaFX 2.0 Binding [[http://ubm.io/1gCSOym](http://www.drdobbs.com/jvm/javafx-20-binding/231903245)]

La nature mono-thread de JavaFX entre toutefois en conflit avec le modèle de
concurrence d'Akka, car la mise à jour des composants est liée au thread
d'application. Malgré cela, mon impression est que l'architecture de JavaFX,
beaucoup plus simple que celle de QT, a été bien pensée.

En conclusion de la partie UI, je relève l'importance du maquettage et de la
gestion hiérarchique des contrôles. J'ai aussi appris des leçons sur la
composition d'un UI desktop, un aspect encore peu développé dans ma carrière.

### Parallélisation

Pour la parallélisation, Akka est un framework très complet. De par ses origines
fonctionnelles (Scala), il est plus difficile à appréhender qu'une librairie
traditionnelle. De plus, il faut s'imprégner de sa philosophie et assimiler des
concepts assez avancés.

Dans le code, en revanche, on est vite séduit par la légèreté et la robustesse
des acteurs. On aimerait d'ailleurs les écrire en Scala pour sa syntaxe plus
compacte (ce qui est possible), mais ce langage reste difficile à apprendre car
très différent de Java.

La mise en oeuvre dans le projet est rudimentaire. En effet, j'ai fait l'impasse
sur la partie réseau, routage et tolérance de pannes. Cependant la librairie est
légère (moins de 10 Mo, dont 7 Mo pour Scala) et elle peut donc aussi servir
pour gérer des tâches simples.

Le véritable défi était d'intégrer deux technologies aussi diamétralement
opposées qu'un UI desktop mono-thread et un système de parallélisation pour
serveurs dans le Cloud. Je crois que ce défi a été relevé, mais l'intégration
par le code doit se limiter au strict nécessaire. Le couplage entre le modèle
du MVC (les bindings de JavaFX) et l'acteur Master a montré ses limites.

En somme, Parakkol a été un projet technologiquement difficile, mais très
enrichissant. J'y ai découvert des librairies et des techniques d'avenir qui me
seront très utiles professionnellement.


Annexes
=======

\input{tex/TPs.tex}
