@echo off
call pic2plot -Tps --page-size a4,ysize=10cm sequence-diag.txt > out.ps 2>nul
call ps2pdf -sPAPERSIZE#a4 -dEPSCrop out.ps sequence-diag.pdf
xcopy /Y/Q sequence-diag.pdf ..\images
del /Q out.ps
