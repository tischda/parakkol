Sequence diagrams
=================

Documentation
-------------
http://www.umlgraph.org/doc/seq-intro.html


Dependencies
------------
Download the following:

    * From UmlGraph 5.6, you only need one file: `sequence.pic`
    * PlotUtils: http://gnuwin32.sourceforge.net/packages/plotutils.htm
    * GhostScript: http://www.ghostscript.com/download/gsdnld.html

Note that the version of GhostScript that comes with Miktek 2.9.5105 does not
work. You will get this error:

~~~
u:\src\polytech\parakkol>ps2pdf out.ps
Error: /rangecheck in --get--
Operand stack:
   --nostringval--   --nostringval--   --nostringval--   descender   0   --nostringval--   1
Execution stack:
   %interp_exit   .runexec2   --nostringval--   --nostringval--   --nostringval--   2   %stopped_push   --nostringval--   --nostringval--   --nostringval--   false   1   %stopped_push   1926   1   3   %oparray_pop   1925   1   3   %oparray_pop   --nostringval--   1909   1   3   %oparray_pop   1803   1   3   %oparray_pop   --nostringval--   %errorexec_pop   .runexec2   --nostringval--   --nostringval--   --nostringval--   2   %stopped_push   --nostringval--   --nostringval--
Dictionary stack:
   --dict:1169/1684(ro)(G)--   --dict:0/20(G)--   --dict:82/200(L)--   --dict:39/50(L)--   --dict:2/17(L)--   --dict:5/17(L)--   --dict:1/3(L)--   --dict:17/19(ro)(L)--
Current allocation mode is local
Last OS error: No such file or directory
Current file position is 11013
MiKTeX GPL Ghostscript 9.05: Unrecoverable error, exit code 1
~~~


Editing
-------
1. Follow instructions in the UmlGraph documentation.

    WARNING: the line endings must be UNIX !!!

2. Convert PIC to PS (ignore the 'output stream jammed' error message)

~~~
pic2plot -Tps sequence-diag.txt > out.ps
libplot error: output stream jammed
~~~

3. Convert PS to PDF

~~~
u:\bin\gs\lib\ps2pdf out.ps
~~~
