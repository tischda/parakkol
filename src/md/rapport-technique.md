Partie technique
================

Structure de l'application
--------------------------

### Reprise de l'existant

La base de code qui servait pour ce projet n'avait pas été conçue pour la
parallélisation. Le codage du problème, de la solution, de la fonction fitness
et des statistiques se trouvaient ensemble dans la même classe `Problem.java`.
Pour tenir compte de la topologie distribuée des acteurs Akka (messages
immutables, fonctions asynchrones), il a fallu faire un découpage et clarifier
certaines responsabilités.

### Packaging

J'ai d'abord séparé les types de composantes : acteurs (actor), interface
utilisateur (fx), algorithme génétique (ga), modélisation du problème (model) et
enfin les statistiques et les classes utilitaires.

![Structure des packages](images/packages-stan.png)

Le diagramme de droite montre les dépendances entre les packages. On pourrait
sans doute faire mieux, mais ce qu'il faut éviter à tout prix, ce sont les
cycles ("package tangle").

### Système de build

Connaissant les limites de Maven, j'ai décidé de migrer vers
[Gradle](http://www.gradle.org/)
\cite{muschko_gradle_2013,berglund_building_2011}. Le premier bénéfice a été une
réduction du fichier de configuration de 266 à 31 lignes. Ensuite cela a
facilité l'intégration des composantes du projet. Ces quelques lignes couvrent
la compilation de Java et Scala (pour Akka), la génération d'un livrable, la
couverture des tests, les métriques de qualité et l'intégration de JavaFX :

~~~
apply plugin: 'java'
apply plugin: 'scala'
apply plugin: 'application'
apply plugin: 'jacoco'
apply plugin: 'sonar-runner'
apply from: 'javafx.plugin'
~~~

Enfin, il était facile de créer de nouvelles tâches, par exemple pour compiler
la documentation :

~~~java
task createDocs(type: Exec) {
    description 'Compiles Markdown documentation into PDF.'
    switch(System.getProperty('os.name').toLowerCase().split()[0]) {
        case 'windows':
            commandLine 'make.bat'
            break
        default:
            commandLine './make.sh'
    }
    outputs.dir "${buildDir}/docs"
}
~~~

Cette stratégie a été payante, même s'il a fallu investir un peu de temps pour
adopter l'outil.

### Système de fichiers

Voici un aperçu de la structure de fichiers. Elle s'apparente beaucoup à celle
de Maven:

~~~
répertoires :    build                 sortie de build pour Gradle
                 gradle                installation locale de Gradle (gradle-wrapper)
                 libs                  dépendances externes (normalement on utilise un dépôt)
                 logs                  sortie de logs pour l'exécution de parakkol
                 modele                classe de document LaTeX (documentation)
                 scripts               scripts de compilation de la documentation
                 src                   sources java, tests et documentation
                 templates             modèle Pandoc pour LaTeX (documentation)
fichiers :       build.fxbuild         fichier de configuration Eclipse pour JavaFX
                 build.gradle          fichier de build de Gradle
                 CHANGES.md            historique des changements (notes de projet)
                 gradlew               script de lancement de Gradle (gradle-wrapper UNIX)
                 gradlew.bat           script de lancement de Gradle (gradle-wrapper Windows)
                 javadoc.options       options de génération de diagrammes de classes (UmlGraph)
                 javafx.plugin         configuration du plugin JavaFX de Gradle
                 make.bat              script de compilation de la documentation (Windows)
                 make.sh               script de compilation de la documentation (UNIX)
                 README.md             fichier lisez-moi (présentation et démarrage rapide)
                 settings.gradle       fichier de propriétés de Gradle
~~~

Framework Akka
--------------

[Akka](http://akka.io/) est un framework récent (2009) écrit en
[Scala](http://www.scala-lang.org/), un langage fonctionnel qui s'exécute sur la
JVM[^3]. Son modèle d'acteurs concurrents est inspiré d'Erlang et se caractérise
par une interaction asynchrone par messages immutables et une structure de
supervision hiérarchique.

Un aspect intéressant pour les calculs scientifiques répartis est la tolérance
de pannes (redémarrage, reprise, arrêt, escalation) qui se fait selon la
philosophie "let it crash" : A une programmation défensive pour maintenir un
objet dans un état stable, on préfère le laisser "s'écraser" pour le recréer
ensuite.

[^3]: Java Virtual Machine

Les acteurs sont des processus très légers (non liés à des threads). Par
exemple, dans un 1GB de RAM, on peut avoir 4096 threads (pile de 256K). Pour les
acteurs ce chiffre s'élève à 2.7 millions \cite[p. 21]{roestenburg_akka_2013}.
En même temps, la montée en charge vers un modèle distribué peut se faire par
simple configuration.

Enfin, le framework fournit une API Java modulaire disponible sous forme de
fichier JAR.

### Prise en main

Pour démarrer rapidement, j'ai commandé tous les livres récents parlant d'Akka
\cite{subramaniam_programming_2011, roestenburg_akka_2013, allen_effective_2013,
wyatt_akka_2013}. Le sujet est particulièrement dense, et malheureusement seul
"Programming concurrency on the JVM" \cite{subramaniam_programming_2011}
présente du code en Java. Si les livres sont une bonne introduction aux
concepts, en pratique la meilleure référence reste la documentation en ligne.

#### Création d'un acteur

La première chose à faire est de créer un système d'acteurs (ligne 1). C'est un
objet global pour l'application qui est au sommet de la hiérarchie (donc
responsable de l'allocation des threads). Ensuite, on crée l'acteur principal en
lui passant ses paramètres de constructeur (ligne 4). Enfin, cet acteur est
transmis au système qui renvoie une référence de type `ActorRef` (ligne 5).

~~~java
ActorSystem system = ActorSystem.create("parakkol");

ConfigManager configManager = new ConfigManager();
Props props = Props.create(MasterActor.class, configManager.load());
ActorRef masterActor = system.actorOf(props, MasterActor.NAME);

system.shutdown();
~~~

Le fait que l'on reçoive une référence et non l'objet réel est la base de la
distributivité des acteurs, car selon la configuration, ces références peuvent
être locales ou distantes. Il faut aussi gérer cet aspect si l'on utilise un
framework d'injection de dépendances basé sur le modèle du Singleton (les
acteurs ne sont pas des Singletons).

Pour intégrer mon algorithme génétique avec Akka, j'ai choisi le patron de
composition de POJOs ("Plain Old Java Objects", objets java standards). Je
pouvais alors tester les fonctions de ces objets en isolation et de façon
synchrone sans dépendre d'Akka (par exemple l'objet `Loader`, ligne 2 ci-dessous).

Chaque acteur encapsulait donc ses fonctions dans un objet. Il restait à
orchestrer leur collaboration. Pour cela, il fallait surcharger la méthode
`onReceive()` de la classe de base `UntypedActor` et brancher sur le type de
message reçu. J'ai créé une classe pour chaque message afin de bénéficier de la
complétion de l'éditeur et de la validation du compilateur (à éviter :
`message.toString().equals("monMessssage")`).

~~~java
public final class LoaderActor extends UntypedActor {
    private Loader loader;

    @Override
    public void onReceive(final Object message) {
        if (message instanceof RequestAFile) {
            sendFileOrPoisonIfFinished(message);
        } else {
            unhandled(message);
        }
    }
}
~~~

Le branchement en cascade avec des `if ... else` n'étant pas très élégant, j'ai
déporté la logique de chaque cas d'utilisation dans des fonctions séparées
(cela facilitait aussi les tests unitaires).

Dans l'exemple ci-dessous, l'acteur `LoaderActor` appelle une fonction de
l'objet `loader` qui charge une instance du problème et la retourne à l'envoyeur
(ligne 4). Lorsque tous les fichiers sont traités, cet acteur envoie une "pilule
de poison" à l'envoyeur, ce qui causera sa mort certaine (ligne 10).

~~~java
private void sendFileOrPoisonIfFinished(final Object message) {
    if (loader.toProcessCount() > 0) {
        try {
            getSender().tell(loader.pickAFile(), getSelf());
        } catch (IOException e) {
            log.error(e, "Cannot pick a file");
            unhandled(message);
        }
    } else {
        getSender().tell(PoisonPill.getInstance(), getSelf());
    }
}
~~~

Notons que l'envoyeur n'est pas en attente bloquante d'une réponse, il possède
lui-même une cascade `onReceive()` qui agit sur réception d'un message de type
`Problem` :

~~~java
if (message instanceof Problem) {
    solveProblem((Problem) message);
    requestAFile();
}
~~~

Enfin, lorsque le travail du solveur est terminé, le `SolverActor` crée une
nouvelle demande de fichier à traiter qu'il envoie au `LoaderActor`. La boucle
est ainsi bouclée.

#### Particularités

Les origines fonctionnelles d'Akka peuvent surprendre les développeurs Java
habitués à voir une méthode `add()` ou `remove()` modifier l'objet original
(ligne 6). Avec Akka, il faut penser à assigner la valeur de retour, car les
objets sont en général immutables, c'est-à-dire non modifiables (ligne 16) :

~~~java
// create routees
List<Routee> routees = new ArrayList<Routee>();
for (int i = 0; i < ACTOR_COUNT; i++) {
  ActorRef r = system.actorOf(props, "routee" + i);
  routerInbox.watch(r);
  routees.add(new ActorRefRoutee(r));
}

// create router
Router router = new Router(new BroadcastRoutingLogic(), routees);
assertThat(router.routees().size(), equalTo(ACTOR_COUNT));

// add routee, see "Management Messages", p.114 akka doc (since 2.3.0)
// do not forget to reassign router from return value!!!
ActorRef other = TestActorRef.create(system, props, "r4");
router = router.addRoutee(other);
assertThat(router.routees().size(), equalTo(ACTOR_COUNT + 1));
~~~

Ce genre d'erreur n'est pas détecté à la compilation, il est donc impératif
d'écrire des tests d'apprentissage. Ces tests ne servent pas à tester le
framework, mais à vérifier que l'on a bien compris comment l'utiliser.

Et ce n'est pas toujours évident. Par exemple, lorsque tous les acteurs associés
à un routeur se terminent, ce dernier considère qu'il est inutile et se termine
aussi ("let it crash!"). Bien que ce comportement soit documenté, on aura
peut-être du mal à s'y habituer.


JavaFX 2
--------

La version 2 de JavaFX est une alternative intéressante face à QT Jambi, AWT et
Swing[^4], car cette solution, en plus d'être portable, fait partie intégrante
de Java 8 (sorti le 18 mars 2014), réduisant ainsi les dépendances et donc les
incompatibilités.

[^4]: Java GUI frameworks. What to choose? [[http://bit.ly/1lERfF8](http://stackoverflow.com/questions/7358775/java-gui-frameworks-what-to-choose-swing-swt-awt-swingx-jgoodies-javafx)]

Ce qui m'a plu ensuite, c'était la séparation entre modèle, vue et contrôleur
(MVC). La présentation est entièrement codée en (F)XML, le lien avec le code
se fait au travers des attributs :

~~~xml
<BorderPane ... fx:controller="polytech.parakkol.fx.root.RootLayoutController">
~~~

Les composants de l'interface utilisateur (UI) sont alors automatiquement
injectés dans le code de la classe contrôleur marqué par des annotations :

~~~java
@FXML private ProgressBar progressBar;
@FXML private TextField solverCount;
@FXML private Button clearButton;
@FXML private Button startButton;
~~~

Le code Java de l'application JavaFX s'en trouve ainsi considérablement réduit :

~~~java
stage.setTitle("Parakkol");
Parent rootLayout = new RootLayoutView().getView();
Scene scene = new Scene(rootLayout);
stage.setScene(scene);
stage.show();
~~~


Dans l'architecture modulaire de JavaFX[^5], on retiendra la bibliothèque
multimédia native et la possibilité de styliser l'apparence avec CSS (Cascading
Style Sheets). J'ai testé également la traduction de l'interface avec des
fichiers de ressources, pour cela il suffit de préfixer les labels avec un signe
'%'.

[^5]: JavaFX Architecture [[http://bit.ly/OzaS6k](http://docs.oracle.com/javafx/2/architecture/jfxpub-architecture.htm)]

### Prise en main

Pour une nouvelle technologie, JavaFX est très bien documenté. Le plus simple
est de suivre un tutoriel en ligne [^6] ou de reprendre un exemple existant.
En quelques heures, j'avais réalisé une première version. On pourrait même
envisager d'utiliser l'outil ScreenBuilder pour faire des prototypes d'analyse.

[^6]: Getting Started with JavaFX Scene Builder [[http://bit.ly/OrSVqm](http://www.youtube.com/watch?v=rHcnsEoSK_c)]

Ce qui était plus ennuyeux, en revanche, c'était la compilation. JavaFX n'était
pas encore intégré avec le JDK 7 et il a fallu faire appel à des extensions pour
Eclipse et Gradle. Sous Linux, j'ai dû désinstaller OpenJDK et le remplacer par
le JDK d'Oracle (JavaFX est distribué sous licence commerciale). Espérons que
ces détails seront réglés avec Java 8.


Architecture
------------

### Injection de dépendances

Dans une présentation de la conférence JAX (une des plus grandes conférences
Java en Europe), l'expert Adam Bien proposait une approche de "convention over
configuration" sur un mini-projet nommé "afterburner.fx".[^7] Il s'agit d'une
convention de nommage et d'instanciation des composants JavaFX avec injection de
dépendances (DI).

[^7]: Adam Bien: Enterprise JavaFX 8 [[http://bit.ly/1lERlg5](http://www.youtube.com/watch?v=Yh03cziYdp8)]

DI est un patron intéressant pour intégrer deux technologies disparates : d'un
côté le système des acteurs, et de l'autre l'application JavaFX. Je ne voulais
pas créer le système d'acteurs dans l'interface utilisateur, pour permettre
l'écriture de solveurs "headless" (sans UI) sur des machines distantes. Je ne
pouvais pas non plus implémenter la `MainApp` de l'UI avec le système d'acteurs.
La DI me permettait de réaliser l'instanciation de ces objets de façon externe,
puis de les injecter pour les mettre en relation.

J'ai donc repris le code d'afterburner.fx en supprimant les parties inutiles.
Malheureusement j'y ai passé plus de temps que prévu, car je ne comprenais pas
certains choix de l'auteur, et je ne voulais m'encombrer ni de Spring, ni de
Guice que je ne connaissais pas.

Après suppression du code relatif au chargement asynchrone des composants, j'ai
enfin pu injecter mes acteurs Akka dans mes contrôleurs JavaFX. En clair, les
contrôleurs (qui recevraient les clics de l'utilisateur) pouvaient maintenant
interagir avec la couche de services et exécuter ses actions.


### Diagramme des flux

Le diagramme ci-dessous décrit les classes sous forme de rectangles, les
acteurs sous forme de cercles (plusieurs instances pour le solveur), et les
ressources entre deux lignes horizontales pointillées.

![Diagramme des flux](images/architecture.pdf)

L'organisation des acteurs suit l'architecture des exemples de la littérature,
avec la difficulté supplémentaire de l'intégration des fonctionnalités de l'UI.
La règle était que l'UI pouvait parler aux acteurs, mais pas l'inverse. La
création d'un acteur maître comme seul interlocuteur de l'UI a permis de régler
ce contentieux.

La deuxième règle était de ne pas bloquer une communication asynchrone dans
l'attente d'une réponse. J'ai mis en place une communication unidirectionnelle, à
l'initiative du solveur. Dans la mini-chaîne de production `Loader` &larr;
`Solver` &rarr; `Statistics` on pouvait donc ajouter et retirer des solveurs à
tout instant.

Notons que cette architecture, présentée au début du projet, n'a quasiment pas
évolué. J'avais d'abord prévu un acteur pour gérer la liste des fichiers (selon
le modèle "publish-subscribe"), mais le `Loader` a très bien fait l'affaire.

### Diagramme de séquence

D'un point de vue dynamique, le `Master` crée les acteurs `Loader` et
`Statistics` dans sa phase `preStart()`. Il demande ensuite au `Loader` le
nombre de fichiers à traiter, valeur qu'il initialisera à réception du message.

Ensuite, l'application JavaFX transmet au `Master` les modèles (M de MVC) de
données qui doivent être mis à jour dans l'UI. Par exemple, le `Master`
connaissant le nombre total de fichiers à traiter, celui-ci pourra calculer le
pourcentage d'avancement pour la barre de progression affichée à l'utilisateur
par l'UI.

![Diagramme de séquence](images/sequence-diag.pdf)

Lorsque l'utilisateur clique sur le bouton "Start Solver", l'UI envoie un
message de commande au `Master` qui crée un `Solver` et lui transmet le message.
Le `Solver` demande ensuite un fichier au `Loader` qui répond avec une instance
du problème à résoudre. Lorsque le `Solver` reçoit ce message, il devient actif
et communique son état au `Master`.

A la finalisation de chaque "run" (exécution complète de l'algorithme), le
`Solver` envoie ses métriques à l'acteur `Statistics` qui les retransmet au
`Master` pour mise à jour du modèle MVC (modèle sous-jacent à la liste des
problèmes affichés dans l'interface utilisateur).

### Diagramme de classes

Les diagrammes de classe sont générés par `UmlGraph` (dans le javadoc) et
permettent de naviguer dans le modèle en cliquant sur les classes. Laissons
à chaque outil sa spécialité et ne dupliquons pas inutilement ces informations
dont je résume ici les points essentiels.

La fonction fitness a été dissociée du problème dans la classe `Evaluator` qui
est responsable de mettre à jour la valeur de fitness dans les palettes de
couleur. De même, les opérations de l'algorithme génétique sont externalisées
([jMetal](http://jmetal.sourceforge.net/) utilise une approche semblable).

![Diagramme de classes (Problem)](images/problem-class.png)

Avant de refactoriser, j'avais spécifié le code avec des tests de
caractérisation. Cela était nécessaire pour vérifier que mon `Evaluator`
renvoyait bien les mêmes valeurs de fitness que l'original.

Enfin, notons la relation entre l'acteur `SolverActor` et les autres classes du
domaine. En l'occurrence, la classe `Problem` est utilisée comme message entre
`LoaderActor` et le `SolverActor`, les statistiques ne sont générées qu'ensuite
dans leur classe `Metrics` et transmises au `StatisticsActor` par le message
`SolverMetrics`.

### Interface utilisateur

L'application présente un écran unique. Sur la gauche, on retrouve les
paramètres d'exécution que l'utilisateur peut modifier et enregistrer pour une
utilisation future. A droite, on a la liste des solveurs et des instances en
cours de traitement. Un problème résolu reste affiché avec sa moyenne de fitness.

![Interface utilisateur (UI)](images/fx-general-commented.png)

En bas de l'écran, on trouve les boutons qui permettent de sauvegarder les
paramètres, de réinitialiser les statistiques et de démarrer un solveur. La
barre de progression indique la progression globale sur toutes les instances, et
à côté, le nombre de solveurs actifs.

Il est possible d'interrompre l'exécution à tout moment en cliquant sur "Clear
Statistics". Cette fonction stoppe les solveurs, met à zéro les statistiques et
initialise les paramètres.


Résultats obtenus
-----------------

Le temps écoulé, affiché dans la partie gauche de l'écran, montre les
résultats de la parallélisation.

Configuration                 Temps
-------------                 ----------
TP classique séquentiel       514.343 secondes
Parakkol 1 solveur            530.339083478 secondes
Parakkol 8 solveurs           100.550958626 secondes

Les mesures ont été réalisées sur une machine Windows 7 64-bit avec un
processeur Intel(R) Core(TM) i7-3770 @ 3.40 GHz avec 4 coeurs physiques (8 HT),
équipée de 16 GB de RAM.

En exécutant d'abord un seul solveur, on remarque que seuls deux des huit coeurs
virtuels sont actifs, et seulement à la moitié de leur capacité.

![Utilisation CPU avec un seul solveur](images/perf-cpu-1-solver.png)

Avec huit solveurs, en revanche, on voit que tous les coeurs sont exploités
au maximum. L'utilisation des threads par Akka est donc optimale.

![Utilisation CPU avec 8 solveurs](images/perf-cpu-8-solver.png)

Ceci est confirmé par un temps de résolution cinq fois inférieur à l'exécution
précédente. Ce résultat est excellent puisque le processeur de la machine de test
n'a en réalité que 4 coeurs physiques.

Notons enfin que l'intégration d'Akka ne pénalise que très peu l'exécution
monotâche en comparaison avec le code original non parallélisé.
