Gestion de projet
=================

Méthodologie
------------

Pour réaliser ce projet de courte durée l'approche a été "agile". Il n'y a donc
pas eu de "big upfront analysis", l'effort méthodologique est en rapport avec le
temps et les ressources disponibles. De plus :

- les objectifs sont clairs
- les contacts avec l'utilisateur sont fréquents

Néanmoins, cette démarche sera contrôlée par les outils de gestion de projet et
d'intégration continue.

Infrastructure
--------------

Sur un serveur hébergé dédié, j'ai mis en place les outils suivants:

- Atlassian [JIRA](https://www.atlassian.com/software/jira) : gestion de projet et suivi des bogues
- Sonatype [Nexus](http://www.sonatype.org/nexus/) : dépôt d'artefacts
- PMEase [QuickBuild](http://www.pmease.com/) : build et intégration continue
- SonarSource [SonarQube](http://www.sonarqube.org/) : métriques de qualité

Le dépôt source [Bitbucket](https://bitbucket.org/) (Atlassian) s'intègre
parfaitement avec ces outils. La gestion de version GIT
\cite{loeliger_version_2012} permet de travailler en local sans dépendre d'une
connexion internet.

Ces outils sont d'une importance capitale, car ils accompagnent le développement
et favorisent ainsi la bonne gestion du projet (traçabilité, feedback
instantané, visualisation de l'état d'avancement).


Planification
-------------

Pour planifier le projet, il faut décomposer les objectifs initiaux en
sous-tâches plus petites:

- mise en place de l'infrastructure de développement
- modularisation du code existant en vue de sa parallélisation
- prise en main des frameworks Akka et QT
- tests et développements
- rédaction du rapport

Au début d'un projet, on a rarement tous les éléments pour décider, surtout
lorsqu'on incorpore de nouvelles technologies. Même avec une marge de "risque
technologique", un planning détaillé reste illusoire puisque de nombreux choix
dépendront des possibilités encore inconnues des frameworks.

J'ai donc choisi la méthode du "Personal Kanban" \cite{benson_personal_2011},
recommandée par "A Factory of One" \cite{markovitz_factory_2012}, qui s'est
avérée efficace pour visualiser l'avancement du projet. En effet, au cours du
projet, de nouvelles idées enrichissaient le backlog. Ce système permettait,
de façon tangible, de discuter et fixer les priorités avec le client.

L'auteur du livre préconise les trois colonnes obligatoires : "To Do" (le
backlog), "In Progress" (en cours) et "Done" (Terminé), avec en plus une colonne
"On Hold" pour les tâches en attente.

![JIRA "Agile Board", version Kanban](images/kanban-commented.png)

Horizontalement, le tableau se divise en trois zones. On y trouve un flux
général pour les objectifs à long terme, un flux "urgent" pour les problèmes
bloquants, et finalement le flux standard pour le projet. Une fois configuré
correctement, l'outil évite le "scope creep" (glissement de la portée) typique
d'une gestion trop laxiste, tout en visualisant le potentiel d'évolution pour
les versions futures.


Suivi et traçabilité
--------------------

JIRA est un maillon très puissant dans la chaîne d'intégration continue. Il
permet de tracer, pour chaque tâche, les parties du code modifiées, et cela en
parallèle avec la gestion des versions officielles. J'ai donc décidé de "livrer"
une nouvelle version avant chaque visite de mon client.

En plus de permettre un suivi des métriques de projet (temps estimé, temps
effectif), JIRA est un bon outil de collaboration et une base de connaissances
très utile quand il faut retracer le processus de résolution d'une tâche. La
zone "Activity" présente les commentaires et l'historique dans son contexte
mieux qu'un rapport d'activité classique.

![Tâche JIRA avec son historique complet](images/jira-commented.png)


Mise en oeuvre
--------------

Une bonne gestion commence par la mise en place des outils, la compilation et la
génération des artefacts. Soulignons l'importance de traiter le packaging au
plus tôt, car cela évite de se retrouver en fin de projet dans l'impossibilité
de livrer.

J'ai pris le parti d'écarter d'autres risques en amont, par exemple la
génération du rapport en \LaTeX \cite{mittelbach_latex_2004}. Pour me
familiariser avec le langage, j'ai repris le modèle standard de Polytech et j'y
ai intégré la possibilité de compiler du code
[Markdown](http://daringfireball.net/projects/markdown/), un langage de balisage
dont le but est d'offrir une syntaxe facile à lire et à écrire. En séparant
ainsi le contenu de la forme, j'ai pu me concentrer sur la rédaction sans être
distrait par des problèmes de formatage.[^1]

[^1]: Le présent rapport est écrit entièrement en Markdown.

Toujours dans la même idée, j'ai réalisé la génération des diagrammes de classe
et de séquence de façon programmatique, ce qui m'aura évité plus tard de perdre
du temps à les éditer manuellement.

Quant à la configuration des outils de gestion de projet, l'extension "Agile" de
JIRA nécessitait quelques ajustements. Plus connu sous le nom de "GreenHopper",
ce module était en pleine refonte. On y trouvait un mode "Classic" (voué à
disparaitre) et une implémentation de Kanban encore incomplète.

J'ai mis plusieurs heures à ajouter un simple statut "On Hold", mais j'ai
compris ainsi comment définir et administrer un workflow sous JIRA.

![Workflow "Personal Kanban"](images/workflow.png)

Le statut "On Hold" ("The Pen", dans "Personal Kanban") est un état temporaire
d'attente, pour des tâches qu'on ne contrôle pas, exécutées par des acteurs
externes (par exemple, la validation de la documentation). Cela permet de libérer son
WIP (Work in Progress) pour s'attaquer à d'autres tâches, sans pour autant
bloquer le flux.

### Gestion du changement

L'argument des méthodes agiles est que le changement est inévitable. En effet,
même sur ce projet de courte durée, j'ai dû faire face à un changement majeur.

Après deux jours de tentatives infructueuses pour installer [QT Jambi](http://qt-jambi.org/),
il a fallu se rendre à l'évidence que le projet, abandonné depuis quelques
années, n'avait pas le potentiel imaginé. Je n'étais même pas arrivé à déplorer
le manque de documentation : ce projet ne compilait tout simplement pas.[^2]

[^2]: QTJambi - build notes [[http://bit.ly/OrSNHa](https://www.mail-archive.com/qt-jambi-interest@qt.nokia.com/msg00509.html)],
et autres tentatives :

    * installation des binaires de QT Jambi 4.7.0, puis de ceux packagés par macports
    * installation de différentes versions de QT par fichier DMG (Apple Disk Image)
    * compilation de QT 4.8.5, puis de QT Jambi à partir du code source


Il fallait donc trouver une alternative, et avec la sortie imminente de Java 8,
je me suis penché sur [JavaFX](http://docs.oracle.com/javafx/). En moins d'une
journée j'avais réalisé un prototype que j'ai ensuite proposé à mon client.

### Gestion des priorités

Bien que le cahier des charges tînt en quelques lignes, il fallait gérer les
aléas du projet et prioriser la résolution des problèmes, tout comme les bonnes
idées qui surgissaient tout au long du projet.

Par exemple, en découvrant Akka, j'aurais voulu approfondir les aspects de
résilience des acteurs. Que se passerait-il si un acteur redémarrait en plein
calcul ? Mais en même temps, j'avais d'autres soucis : les tests avec plusieurs
solveurs concurrents provoquaient des exceptions dans l'interface utilisateur.

Evidemment, tous ces éléments sont intéressants, mais il faut en discuter avec
le client. L'Agile Board de JIRA permettait de choisir les alternatives dans le
backlog, j'ai cependant regretté qu'il y manque l'affichage des "story points"
(une mesure de l'effort estimé).

### Gestion de la qualité

Un code soigné permet des cycles de développement plus rapides tout en modérant
le risque de mauvaises "surprises" (bogues) dans le code. Le meilleur moyen de
garantir cette qualité est l'intégration continue et l'exécution systématique
des tests après tout changement.

![Intégration continue avec PMEase QuickBuild](images/quickbuild-commented.png)

Ceci présente un double avantage :

* il existe toujours une version livrable
* l'analyse du code détecte des régressions au plus tôt, ce qui diminue l'effort de correction

Pour tirer profit de ces outils, il faut se soumettre à une discipline. Ainsi,
avant de démarrer une nouvelle tâche, on créera systématiquement une branche
sous GIT.

Dans SonarQube, on gardera un oeil critique sur les bonnes pratiques ("Rules
Compliance"), la couverture des tests ("Test coverage") et globalement sur tout
changement brusque des autres indicateurs.

Par exemple, SonarQube m'a permis de détecter un "package tangle", donc une
inversion suspecte de responsabilités. En effet, un acteur Akka (potentiellement
distribué sur une autre machine) accédait à l'interface utilisateur locale.

Ces problèmes ne se détectent généralement pas pendant la compilation et peuvent
rester latents, même après la mise en production. L'analyse du code est donc un
outil intéressant à mettre en oeuvre, même pour de petits projets.

![Analyse Statique avec SonarQube](images/sonar.png)

