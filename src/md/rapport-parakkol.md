---
grade: |
	Département Informatique

	5\ieme{} année

	2013 - 2014

document:
	type: Rapport Parallélisme / Librairies
	title: Parakkol - Parallel Akka Color Solver
	short: Parallel Akka Color Solver
    abstract: |
        Parakkol is a java application that solves a color problem with a
        genetic algorithm. It makes use of the Akka framework to run several
        solvers in parallel.

    keywords: Project, Akka, parallel, solver, genetic, color, JavaFX, concurrent
    frenchabstract: |

        Parakkol est une application java servant à résoudre un problème
        d'optimisation de couleurs par un algorithme génétique. Elle utilise
        le framework Akka pour exécuter plusieurs solveurs en parallèle.

    frenchkeywords: Projet, Akka, parallèle, solveur, génétique, couleur, JavaFX,
                    concurrents

supervisors:
    category: Encadrant
	list:
		- name: Gilles VENTURINI
		  mail: gilles.venturini@univ-tours.fr
	details: "École polytechnique de l'université de Tours"

authors:
    category: Étudiant
	list:
		- name: Daniel TISCHER
		  mail: daniel.tischer@etu.univ-tours.fr
	details: DI5 2013 - 2014

preamble:
    include.tex
---

Introduction
============

Objectifs et contexte
---------------------

Ce rapport présente les travaux faits pour la validation des unités de valeur
"Parallélisme" et "Langage IV - Librairies", sur la base de concepts vus en
cours de Méthodes de résolution.

Sur une durée de 7 semaines, notre objectif est de paralléliser un algorithme
génétique et de développer une interface graphique pour son pilotage.

En plus de ces aspects fonctionnels, j'ai fait le choix pédagogique de prendre
en main et d'intégrer les librairies Akka et QT pour Java (QT Jambi).


Problème à résoudre
-------------------

Nous recherchons le meilleur rapport visuel entre un avant-plan et un arrière-plan
pour des personnes mal voyantes. L'évaluation se fait par une fonction
"fitness" décrite en détail en annexe et qu'il s'agit de minimiser. On peut
ensuite comparer les résultats de différents algorithmes, ce qui était
l'objectif précédent lors des travaux pratiques de Méthodes de résolution.

![Fonction fitness (instance 11-2)](images/eval-11-2-commented.png)

Ici, nous allons nous concentrer sur le temps d'exécution et faciliter le
paramétrage pour améliorer les performances globales.

### Paramétrage

Les algorithmes génétiques évoluent à partir d'une population initiale, sur
laquelle on applique de façon cyclique, pour chaque génération, des opérations de
sélection, de croisement et de mutation des individus.

Ces étapes comportent une part de hasard qui évite une convergence prématurée de
l'algorithme. La valeur de fitness de chaque exécution sera donc différente.
Pour compenser ces écarts, nous exécutons l'algorithme plusieurs fois sur une
même instance et calculons ensuite la moyenne des résultats.

Les paramètres sont les suivants:

* `population_size` : taille de la population
* `elites` : les M meilleurs parents sélectionnés pour le croisement
* `crossover_rate` : probabilité qu'un génome soit croisé entre deux individus
* `mutation_rate` : probabilité qu'un individu d'une population ait muté
* `mutation_spread` : probabilité qu'un génome particulier soit modifié
* `max_generations` : nombre maximal de générations
* `max_evaluations` : nombre maximal d'évaluations de la fonction fitness
* `run_count` : nombre d'exécutions sur une même instance

Pour notre objectif de parallélisation, seuls les deux derniers paramètres sont
significatifs :

* `max_evaluations` : influe sur la durée de traitement d'une instance
* `run_count` : multiplié par la durée de traitement = temps de calcul total
  d'une solution d'instance

### Entrées

En entrée, nous avons une série de 164 fichiers qui contiennent chacun des
données différentes du problème. Leur localisation peut être modifiée dans le
fichier de configuration principal `application.conf`.

~~~
parakkol {
    # Location of data files
    data_path = "src/main/resources/instances"
}
~~~

Avec les paramètres initiaux, nous avons donc 164 fichiers, pour chaque fichier
50 itérations et pour chaque itération, 10 000 évaluations de la fonction
fitness.

### Sorties

Dans la version initiale, on stockait les données statistiques dans des fichiers
pour pouvoir tracer des courbes et comparer ainsi les différents algorithmes.

Pour ne pas dénaturer l'application existante, nous maintenons la possibilité
d'exporter ces métriques, mais ce ne sera pas notre but. Nous verrons plus loin
les possibilités d'évolution de l'analyse statistique.

Aspects dynamiques
------------------

L'évaluation de la totalité des fichiers prend plusieurs minutes. Nous avons
déjà optimisé l'algorithme. De plus, la fonction fitness est très rapide. Une
parallélisation à ce niveau causerait des changements de contexte fréquents,
avec un impact négatif sur les performances \cite{hunt_java_2012,
ghosh_distributed_2007}.

Nous envisageons donc de paralléliser la résolution de problèmes entiers en les
distribuant sur plusieurs solveurs, ou unités de calcul (CPUs ou systèmes
répartis).

Cette répartition implique une gestion du démarrage, de l'arrêt des solveurs et
des changements de configuration du système.


Aspects non fonctionnels
------------------------
On attend de tout développement qu'il soit facilement maintenable et extensible.

L'ergonomie de l'interface graphique doit également permettre une prise en main
rapide et répondre aux commandes de l'utilisateur de façon réactive.

Enfin, le packaging permettra un déploiement facile sur MacOS et Microsoft
Windows.


