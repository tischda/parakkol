package polytech.parakkol.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * color-pair reverse index to lookup pairs affected by color change.
 *
 * @author 'Daniel TISCHER'
 *
 */
public final class ColorPairMap extends HashMap<Integer, Set<Integer>> {

    /**
     * Constructor.
     *
     * @param pairs the pairs
     */
    public ColorPairMap(int [][] pairs) {
        for (int i = 0; i < pairs.length; i++) {
            for (int j = 0; j < 2; j++) {
                Set<Integer> pairSet = get(pairs[i][j]);
                if (pairSet == null) {
                    pairSet = new HashSet<Integer>();
                    pairSet.add(i);
                    put(pairs[i][j], pairSet);
                } else {
                    pairSet.add(i);
                }
            }
        }
    }

    /**
     * Returns a set of pairs that have the color with index i in them.
     *
     * @param i the color index
     * @return the set of pairs that have this color
     */
    public Set<Integer> getPairsWithColor(int i) {
        return Collections.unmodifiableSet(get(i));
    }
}
