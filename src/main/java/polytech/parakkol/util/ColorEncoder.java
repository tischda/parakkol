package polytech.parakkol.util;

import org.apache.commons.math3.util.FastMath;

/**
 * Utility class for color space conversion and color encoding.
 *
 * @author 'Sébastien Aupetit'
 * @author 'Daniel TISCHER'
 *
 */
public final class ColorEncoder {

    //Check:OFF: MagicNumber

    public static final int MAX_COLORVALUE = 16777215;

    /** Cached colors of sRGB to linear RGB components */
    private static final double[] LINEAR_RGB;

    /**
     * Compute the hexadecimal representation of a sRGB color.
     *
     * @param r
     *            the red
     * @param g
     *            the green
     * @param b
     *            the blue
     * @return the string
     */
    public static String hex(final int r, final int g, final int b) {
        String v = Integer.toHexString((r * 256 + g) * 256 + b);
        while (v.length() < 6) {
            v = '0' + v;
        }
        return "#" + v;
    }

    /** do some precalculus to reduce computation time */
    static {
        LINEAR_RGB = new double[256];
        for (int i = 0; i <= 255; i++) {
            final double d = i / 255.;
            LINEAR_RGB[i] = d <= 0.03928 ? d / 12.92 : FastMath
                    .pow((d + 0.055) / (1 + 0.055), 2.4);
        }
    }

    /**
     * The D65 white reference color used in sRGB for web standards.
     *
     * @return D65 white in XYZ color space
     */
    public static double[] getD65White() {
        return new double[] { 0.9505, 1.000, 1.0890 };
    }

    /**
     * SRGB to lab.
     *
     * @param rgb
     *            the rgb
     * @return the double[]
     */
    public static double[] sRGBToLab(final int[] rgb) {
        return xyzToLab(sRGBToXYZ(rgb));
    }

    /**
     * SRGB to xyz.
     *
     * @param rgb
     *            the rgb
     * @return the double[]
     */
    public static double[] sRGBToXYZ(final int[] rgb) {
        final double linearR = LINEAR_RGB[rgb[0]];
        final double linearG = LINEAR_RGB[rgb[1]];
        final double linearB = LINEAR_RGB[rgb[2]];

        return new double[] {
                0.4124 * linearR + 0.3576 * linearG + 0.1805 * linearB,
                0.2126 * linearR + 0.7152 * linearG + 0.0722 * linearB,
                0.0193 * linearR + 0.1192 * linearG + 0.9505 * linearB };
    }

    /**
     * Convert a CIE XYZ color to a CIE LAB color.
     *
     * @param xyz
     *            the xyz
     * @return the color
     */
    public static double[] xyzToLab(final double[] xyz) {
        final double relativeX = xyz[0] / getD65White()[0];
        final double relativeY = xyz[1] / getD65White()[1];
        final double relativeZ = xyz[2] / getD65White()[2];

        final double fX = relativeX > 0.008856 ? FastMath
                .pow(relativeX, 1 / 3.) : 7.787 * relativeX + 16 / 116.;
        final double fY = relativeY > 0.008856 ? FastMath
                .pow(relativeY, 1 / 3.) : 7.787 * relativeY + 16 / 116.;
        final double fZ = relativeZ > 0.008856 ? FastMath
                .pow(relativeZ, 1 / 3.) : 7.787 * relativeZ + 16 / 116.;

        return new double[] { 116 * fY - 16, 500 * (fX - fY), 200 * (fY - fZ) };
    }

    /**
     * Converts binary to Gray Code. See http://en.wikipedia.org/wiki/Gray_code.
     *
     * @param num the integer to encode
     * @return the encoded value
     */
    public static int bin2gray(final int num) {
        return (num >> 1) ^ num;
    }

    /**
     * Converts Gray Code to binary.
     *
     * @param gray the gray code encoded int
     * @return the decoded value
     */
    public static int gray2bin(final int gray) {
        int result = gray;
        result ^= (result >> 16);
        result ^= (result >> 8);
        result ^= (result >> 4);
        result ^= (result >> 2);
        result ^= (result >> 1);
        return result;
    }

    /**
     * Converts and int back to RGB components.
     *
     * @param color the color as int
     * @return the array of RGB components
     */
    public static int[] intToRGB(final int color) {
        int[] rgb = new int[3];
        rgb[2] = color & 0xFF;
        rgb[1] = (color & 0xFF00) >> 8;
        rgb[0] = (color & 0xFF0000) >> 16;
        return rgb;
    }

    /**
     * Converts and RGB components array to int.
     *
     * @param rgb the RGB component array
     * @return the encoded int
     */
    public static int rgbToInt(final int[] rgb) {
        return (rgb[0] << 16) | (rgb[1] << 8) | rgb[2];
    }

    //Check:ON: MagicNumber

    private ColorEncoder() {
        // utility class
    }
}
