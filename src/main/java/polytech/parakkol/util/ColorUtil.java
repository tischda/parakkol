/**
 * Copyright 2012 Sébastien Aupetit <sebastien.aupetit@univ-tours.fr> This
 * software is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version. This software is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details. You should have received a copy of
 * the GNU Lesser General Public License along with this software. If not, see
 * <http://www.gnu.org/licenses/>. $Id$
 */
package polytech.parakkol.util;

import org.apache.commons.math3.util.FastMath;

/**
 * Utility class for W3C WAI measures. Functions are called by Evaluator to
 * calculate color palette fitness.
 *
 * @author Sébastien Aupetit
 */
public final class ColorUtil {

    //Check:OFF: MagicNumber

    /** The Constant CONTRASTRATIO_MINVALUE. */
    public static final double CONTRASTRATIO_MINVALUE = 1;

    /** The Constant CONTRASTRATIO_MAXVALUE. */
    public static final double CONTRASTRATIO_MAXVALUE = 21;

    /** The Constant BRIGHTNESSDIFFERENCE_MINVALUE. */
    public static final double BRIGHTNESSDIFFERENCE_MINVALUE = 0;

    /** The Constant BRIGHTNESSDIFFERENCE_MAXVALUE. */
    public static final double BRIGHTNESSDIFFERENCE_MAXVALUE = 255;

    /** The Constant TONALITYDIFFERENCE_MINVALUE. */
    public static final double TONALITYDIFFERENCE_MINVALUE = 0;

    /** The Constant TONALITYDIFFERENCE_MAXVALUE. */
    public static final double TONALITYDIFFERENCE_MAXVALUE = 255 * 3;

    /** The Constant WCAG2_LEVELAA_CONTRASTRATIO. */
    public static final double WCAG2_LEVELAA_CONTRASTRATIO = 4.5;

    /** The Constant WCAG2_LEVELAAA_CONTRASTRATIO. */
    public static final double WCAG2_LEVELAAA_CONTRASTRATIO = 7;

    /** The Constant WCAG1_TONALITYDIFFERENCE. */
    public static final double WCAG1_TONALITYDIFFERENCE = 500;

    /** The Constant WCAG1_BRIGHTNESSDIFFERENCE. */
    public static final double WCAG1_BRIGHTNESSDIFFERENCE = 125;

    /** Cached colors of sRGB to linear RGB components. */
    private static final double[] LINEAR_RGB;

    /** do some precalculus to reduce computation time */
    static {
        LINEAR_RGB = new double[256];
        for (int i = 0; i <= 255; i++) {
            final double d = i / 255.;
            ColorUtil.LINEAR_RGB[i] = d <= 0.03928 ? d / 12.92 : FastMath.pow(
                    (d + 0.055) / (1 + 0.055), 2.4);
        }
    }

    public static double distanceLAB(final double[] lab1, final double[] lab2) {
        final double dl = lab1[0] - lab2[0];
        final double da = lab1[1] - lab2[1];
        final double db = lab1[2] - lab2[2];
        return FastMath.sqrt(dl * dl + da * da + db * db);
    }

    public static double contrastDifferenceScaled(int[] newFg, int[] newBg) {
        return scaleContribution(contrastRatio(newFg, newBg),
                WCAG2_LEVELAAA_CONTRASTRATIO, CONTRASTRATIO_MINVALUE);
    }

    public static double tonalityDifferenceScaled(int[] newFg, int[] newBg) {
        return scaleContribution(tonalityDifference(newFg, newBg),
                WCAG1_TONALITYDIFFERENCE, TONALITYDIFFERENCE_MINVALUE);
    }

    public static double brightnessDifferenceScaled(int[] newFg, int[] newBg) {
        return scaleContribution(brightnessDifference(newFg, newBg),
                WCAG1_BRIGHTNESSDIFFERENCE, BRIGHTNESSDIFFERENCE_MINVALUE);

    }

    private static double scaleContribution(final double value,
            final double threshold, final double min) {
        if (threshold <= value) {
            // threshold attained
            return 0;
        } else {
            // threshold not attained, scale error on [0:1]
            return (threshold - value) / (threshold - min);
        }
    }

    /**
     * Brightness difference.
     *
     * @param srgb1
     *            the srgb1
     * @param srgb2
     *            the srgb2
     * @return the double
     */
    public static double brightnessDifference(final int[] srgb1,
            final int[] srgb2) {
        return FastMath.abs(0.299 * (srgb1[0] - srgb2[0]) + 0.587
                * (srgb1[1] - srgb2[1]) + 0.114 * (srgb1[2] - srgb2[2]));
    }

    /**
     * Contrast ratio.
     *
     * @param srgb1
     *            the srgb1
     * @param srgb2
     *            the srgb2
     * @return the double
     */
    public static double contrastRatio(final int[] srgb1, final int[] srgb2) {
        double l1 = ColorUtil.getsRGBLuminance(srgb1);
        double l2 = ColorUtil.getsRGBLuminance(srgb2);
        double l;
        if (l1 < l2) {
            l = l1;
            l1 = l2;
            l2 = l;
        }
        // l1 >= l2
        return (l1 + 0.05) / (l2 + 0.05);
    }

    /**
     * Gets the W3C luminance of a sRGB color as stated in WCAG.
     *
     * @param r
     *            the r
     * @param g
     *            the g
     * @param b
     *            the b
     * @return the luminance
     */
    public static double getsRGBLuminance(final int r, final int g, final int b) {
        return 0.2126 * ColorUtil.LINEAR_RGB[r] + 0.7152 * ColorUtil.LINEAR_RGB[g]
                + 0.0722 * ColorUtil.LINEAR_RGB[b];
    }

    /**
     * Gets the W3C luminance of a sRGB color as stated in WCAG.
     *
     * @param rgb
     *            the rgb
     * @return the luminance
     */
    public static double getsRGBLuminance(final int[] rgb) {
        return 0.2126 * ColorUtil.LINEAR_RGB[rgb[0]] + 0.7152
                * ColorUtil.LINEAR_RGB[rgb[1]] + 0.0722
                * ColorUtil.LINEAR_RGB[rgb[2]];
    }

    /**
     * Tonality difference.
     *
     * @param c1
     *            the c1
     * @param c2
     *            the c2
     * @return the double
     */
    public static double tonalityDifference(final int[] c1, final int[] c2) {
        return FastMath.abs(c1[0] - c2[0]) + FastMath.abs(c1[1] - c2[1])
                + FastMath.abs(c1[2] - c2[2]);
    }

    //Check:ON: MagicNumber

    private ColorUtil() {
        // utility class
    }
}
