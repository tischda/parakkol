package polytech.parakkol.ga;

import java.util.List;
import java.util.NavigableSet;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import polytech.parakkol.ParametersGA;
import polytech.parakkol.model.EvaluationListener;
import polytech.parakkol.model.Palette;
import polytech.parakkol.model.Problem;
import polytech.parakkol.stats.Metrics;
import polytech.parakkol.stats.Solution;
import polytech.parakkol.util.ColorEncoder;

/**
 * Genetic Algorithm solver for the Color Problem.
 *
 * @author 'Daniel TISCHER'
 *
 * @composed 1 - 1 Metrics
 * @composed 1 - 1 Operations
 *
 */
public final class Solver {

    /** Used to convert nanos to milliseconds */
    private static final double ONE_MILLION = 1e6;

    private final Logger log = LoggerFactory.getLogger(getClass());

    /** The solver parameters */
    private final ParametersGA params;

    /** The operations that the solvers executes */
    private final Operations ops;

    /**
     * Constructor.
     *
     * @param params the parameters
     */
    public Solver(final ParametersGA params) {
        this.params = params;
        this.ops = new Operations(params);
    }

    /**
     * Sets the randomizer in the operation (mostly for testing).
     *
     * @param newRandom new randomizer
     */
    public void setRandom(Random newRandom) {
        ops.setRandom(newRandom);
    }

    /**
     * Solve the problem.
     *
     * @param problem the problem to solve
     * @param metrics the metrics
     */
    public void solve(final Problem problem, final Metrics metrics) {

        EvaluationListener listener = createEvaluationListener(problem, metrics);
        problem.evaluator.addListener(listener);

        // start timer
        metrics.start();

        Population population = initPopulation(problem.palette);

        // add the problem's inital values
        population.add(problem.palette);

        while (metrics.getCurrentEvaluationCount() < params.getMaxEvaluations()
                && population.getGeneration() < params.getMaxGenerations()) {

            // Selection
            NavigableSet<Palette> parents = population.getBest(params.getElites());

            // Crossover
            List<Palette> offspring = ops.crossover(problem, parents);

            // Mutation
            ops.mutate(offspring);

            // Regeneration
            regenerate(population, offspring);
        }
        metrics.stop();
        problem.evaluator.removeListener(listener);

        log.debug("Generations: " + population.getGeneration()
                + ", evals: " + metrics.getCurrentEvaluationCount()
                + ", time: " + (metrics.getElapsedTime() / ONE_MILLION) + " ms");
    }

    /**
     * Registers the listener that will record the metrics when getFitness() is called.
     *
     * @param problem the problem
     * @param metrics the metrics
     */
    private EvaluationListener createEvaluationListener(final Problem problem, final Metrics metrics) {
        EvaluationListener listener = new EvaluationListener() {
            @Override
            public void fitnessEvaluated(Palette p) {
                if (metrics.getCurrentEvaluationCount() < params.getMaxEvaluations()) {
                    metrics.record(new Solution(p.getColors(), p.getFitness()));
                }
            }
        };
        return listener;
    }

    /**
     * Initializes population with random individuals. Fitness is automatically
     * computed for the added individuals when they are added to the population.
     */
    private Population initPopulation(Palette fromPalette) {
        Population population = new Population();
        for (int i = 0; i < params.getPopulationSize() - 1; i++) {
            population.add(newRandomPalette(fromPalette));
        }
        return population;
    }

    /**
     * Returns a random color palette.
     *
     * @param fromPalette the original problem palette (copied for sizing)
     * @return the new random palette
     */
    private Palette newRandomPalette(Palette fromPalette) {
        Palette palette = fromPalette.copy();
        for (int i = 0; i < palette.numberOfColors(); i++) {
            palette.setColor(i, ops.random.nextInt(ColorEncoder.MAX_COLORVALUE));
        }
        return palette;
    }

    /**
     * Regenerates a population with new individuals created during
     * crossover and mutation.
     *
     * @param population the original population
     * @param offspring the children
     */
    private void regenerate(Population population, List<Palette> offspring) {
        population.addAll(offspring);

        // resize population, removing worst candidates
        int size = population.size();
        while (size-- > params.getPopulationSize()) {
            population.remove(population.last());
        }

        // mark as next generation
        population.incGeneration();
    }
}
