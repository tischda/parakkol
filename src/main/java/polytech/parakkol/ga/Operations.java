package polytech.parakkol.ga;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NavigableSet;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import polytech.parakkol.ParametersGA;
import polytech.parakkol.model.Palette;
import polytech.parakkol.model.Problem;

/**
 * Genetic operations applied to Populations and Individuals (Palettes).
 *
 * @author 'Daniel TISCHER'
 *
 */
public final class Operations {

    /** ColorEncoder.MAX_COLORVALUE has 24 bits on */
    private static final int BITS_24 = 24;

    /** The solver parameters */
    private final ParametersGA params;

    /** Randomizer */
    Random random = ThreadLocalRandom.current();

    /**
     * Constructor.
     *
     * @param params the solver parameters
     */
    public Operations(final ParametersGA params) {
        this.params = params;
    }

    // -----------------------------------------------------------------------
    // CROSSOVER
    // -----------------------------------------------------------------------

    /**
     * Create M children. The couples are selected by matching the best parent
     * with the worst.
     *
     * @param problem the problem
     * @param parents the parents
     * @return the list of children
     */
    public List<Palette> crossover(Problem problem, NavigableSet<Palette> parents) {
        List<Palette> offspring = new ArrayList<Palette>();
        Iterator<Palette> it1 = parents.iterator();
        Iterator<Palette> it2 = parents.descendingIterator();

        for (int i=0; i < parents.size() / 2 ; i++) {
            Palette p1 = it1.next().copy();
            Palette p2 = it2.next().copy();

            randomCrossoverPairs(problem, p1, p2);

            offspring.add(p1);
            offspring.add(p2);
        }
        return offspring;
    }

    /**
     * Sets the randomizer for this class (mostly for testing).
     *
     * @param newRandom the new randomizer
     */
    void setRandom(Random newRandom) {
        this.random = newRandom;
    }

    /**
     * Here we make sure that the crossover actually changes the individual
     * and the same pair is not swapped twice.
     *
     * @param problem the problem
     * @param p1 first color palette
     * @param p2 second color palette
     */
    void randomCrossoverPairs(Problem problem, Palette p1, Palette p2) {
        List<Integer> indices = new ArrayList<Integer>();
        for (int i = 0; i < problem.numberOfPairs; i++) {
            indices.add(i);
        }
        Collections.shuffle(indices, random);
        int count = (int) Math.ceil(problem.numberOfPairs * params.getCrossoverRate());
        for (int index : indices.subList(0, count)) {
            Palette.swapPair(problem.pairs, index, p1, p2);
        }
    }

    /**
     * Alternative crossover function that swaps colors instead of color pairs.
     *
     * @param problem the problem
     * @param p1 first color palette
     * @param p2 second color palette
     */
    void randomCrossoverColors(Problem problem, Palette p1, Palette p2) {
        List<Integer> indices = new ArrayList<Integer>();
        for (int i = 0; i < problem.numberOfColors; i++) {
            indices.add(i);
        }
        Collections.shuffle(indices, random);
        int count = (int) Math.ceil(problem.numberOfColors * params.getCrossoverRate());
        for (int index : indices.subList(0, count)) {
            int swap = p1.getColor(index);
            p1.setColor(index, p2.getColor(index));
            p2.setColor(index, swap);
        }
    }


    // -----------------------------------------------------------------------
    // MUTATION
    // -----------------------------------------------------------------------

    /**
     * Mutates the population given as argument by flipping bits in its colors.
     * The individuals and colors are selected randomly.
     *
     * @param offspring the population to mutate
     */
    public void mutate(List<Palette> offspring) {
        for (Palette individual : offspring) {
            if (random.nextDouble() < params.getMutationRate()) {
                mutateColors(individual);
            }
        }
    }

    /**
     * Selects a random color from the individual and flips a random bit.
     *
     * @param individual the mutated inddividual.
     */
    void mutateColors(Palette individual) {
        for (int i = 0; i < individual.numberOfColors(); i++) {
            if (random.nextDouble() < params.getMutationSpread()) {
                int value = individual.getColor(i);
                int k = random.nextInt(BITS_24);

                // replace original value with mutated value
                individual.setColor(i, flipBit(value, k));
            }
        }
    }

    /**
     * Inverts bit at position k in the int argument;
     *
     * @param n integer in which a bit will be flipped
     * @param k the position of the bit
     * @return the new integer
     */
    int flipBit(int n, int k) {
        return n ^ (1 << k);
    }
}
