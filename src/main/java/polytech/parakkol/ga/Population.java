package polytech.parakkol.ga;

import java.util.Iterator;
import java.util.NavigableSet;
import java.util.TreeSet;

import polytech.parakkol.model.Palette;

/**
 * The population is a set of individuals (no duplicates). Implemented as a
 * TreeSet to make it easy to select the best individuals.
 *
 * @author 'Daniel TISCHER'
 *
 * @has "0..n" - "0..n" Palette
 */
public final class Population extends TreeSet<Palette> {

    /** The generation of the population */
    private int generation = 0;

    /**
     * Increases the generation count for this population.
     */
    public void incGeneration() {
        generation++;
    }

    /**
     * Gets the generation of this population.
     *
     * @return the generation
     */
    public int getGeneration() {
        return generation;
    };

    /**
     * Gets the N best individuals in the population.
     *
     * @param count
     *            the number of individuals to select
     *
     * @return the best 'count' individuals
     */
    public NavigableSet<Palette> getBest(final int count) {
        int counter = count;
        Palette key = null;
        Iterator<Palette> it = iterator();
        while (counter-- > 0 && it.hasNext()) {
            key = it.next();
        }
        return headSet(key, true);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Population of size ");
        sb.append(size());
        sb.append(" in generation ");
        sb.append(generation);
        sb.append(":\n");
        for (Palette individual : this) {
            sb.append(individual.toString());
            sb.append("\n");
        }
        return sb.toString();
    }
}
