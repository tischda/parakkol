package polytech.parakkol;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import polytech.parakkol.ConfigManager.Keys;

import com.typesafe.config.Config;

/**
 * Immutable parameters for the GA Solver Algorithm. This implementation
 * encapsulates an AKKA Config object accessed via ConfigManager.Keys.
 *
 * @author 'Daniel TISCHER'
 *
 */
public final class ParametersGA implements Parameters {

    /** Probability that a specific genome will be crossed between two individuals. */
    private final double CROSSOVER_RATE;

    /** Probability that a specific individual in the population will be mutated. */
    private final double MUTATION_RATE;

    /** Probability that a specific color in the mutated genome will be changed. */
    private final double MUTATION_SPREAD;

    /** Number of individuals in the population. */
    private final int POPULATION_SIZE;

    /** M best parents selected for crossover. */
    private final int ELITES;

    /** Maximal number of generations. */
    private final int MAX_GENERATIONS;

    /** The maximal number of evaluations required. */
    private final int MAX_EVALUATIONS;

    /** The number evaluation runs for each problem instance. */
    private final int RUN_COUNT;

    /**
     * Constructor. Values are initialized from AKKA Config object.
     *
     * @param config
     *            the AKKA configuration
     */
    public ParametersGA(Config config) {
        POPULATION_SIZE = config.getInt(Keys.POPULATION_SIZE.toString());
        ELITES = config.getInt(Keys.ELITES.toString());
        CROSSOVER_RATE = config.getDouble(Keys.CROSSOVER_RATE.toString());
        MUTATION_RATE = config.getDouble(Keys.MUTATION_RATE.toString());
        MUTATION_SPREAD = config.getDouble(Keys.MUTATION_SPREAD.toString());
        MAX_GENERATIONS = config.getInt(Keys.MAX_GENERATIONS.toString());
        MAX_EVALUATIONS = config.getInt(Keys.MAX_EVALUATIONS.toString());
        RUN_COUNT = config.getInt(Keys.RUN_COUNT.toString());
    }

    /**
     * Gets the population size.
     *
     * @return the population size
     */
    public int getPopulationSize() {
        return POPULATION_SIZE;
    }

    /**
     * Gets the elites.
     *
     * @return the elites
     */
    public int getElites() {
        return ELITES;
    }

    /**
     * Gets the crossover rate.
     *
     * @return the crossover rate
     */
    public double getCrossoverRate() {
        return CROSSOVER_RATE;
    }

    /**
     * Gets the mutation rate.
     *
     * @return the mutation rate
     */
    public double getMutationRate() {
        return MUTATION_RATE;
    }

    /**
     * Gets the mutation spread.
     *
     * @return the mutation spread
     */
    public double getMutationSpread() {
        return MUTATION_SPREAD;
    }

    /**
     * Gets the max generations.
     *
     * @return the max generations
     */
    public int getMaxGenerations() {
        return MAX_GENERATIONS;
    }

    /* (non-Javadoc)
     * @see polytech.parakkol.Parameters#getMaxEvaluations()
     */
    @Override
    public int getMaxEvaluations() {
        return MAX_EVALUATIONS;
    }

    /* (non-Javadoc)
     * @see polytech.parakkol.Parameters#getRunCount()
     */
    @Override
    public int getRunCount() {
        return RUN_COUNT;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
