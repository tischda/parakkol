package polytech.parakkol;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import polytech.parakkol.model.Problem;

/**
 * The problem loader. Loads problem instances from disk.
 *
 * @author 'Daniel TISCHER'
 */
public class Loader {

    /** Data file extension. */
    private static final String FILE_EXT = "*.dat";

    // keep Sonar happy (Magic Number)
    private static final int RGB_3 = 3;

    /** The directory containing data files. */
    private final Path path;

    /** List of file names to process. */
    private final List<String> toProcessFileNames;

    /**
     * Constructor. Instantiates a new loader and initializes the
     * list of files found in the dataPath.
     *
     * @param dataPath
     *            the data path
     */
    public Loader(String dataPath) {
        path = FileSystems.getDefault().getPath(dataPath);
        toProcessFileNames = readDirectory(dataPath);
    }

    /**
     * Loads a color problem from the specified instance file.
     *
     * @param fileName
     *            the file actorName
     * @return the problem
     * @throws IOException
     *             if an I/O exception has occurred
     */
    public Problem readFile(String fileName) throws IOException {
        try (Scanner scanner = new Scanner(path.resolve(fileName))) {

            // read colors
            int numberOfColors = scanner.nextInt();
            int[][] colors = new int[numberOfColors][RGB_3];
            for (int i = 0; i < numberOfColors; i++) {
                int r = scanner.nextInt();
                int g = scanner.nextInt();
                int b = scanner.nextInt();
                colors[i] = new int[] { r, g, b };
            }
            // read pairs
            int numberOfPairs = scanner.nextInt();
            int[][] pairs = new int[numberOfPairs][2];
            for (int i = 0; i < numberOfPairs; i++) {
                pairs[i][0] = scanner.nextInt();
                pairs[i][1] = scanner.nextInt();
            }
            return new Problem(fileName, colors, pairs);
        }
    }

    /**
     * Returns the number of files to process.
     *
     * @return the number of files to process
     */
    public int toProcessCount() {
        return toProcessFileNames.size();
    }

    /**
     * Removes a problem from the list and returns it.
     *
     * @return the problem
     * @throws IOException
     *             if an I/O exception has occurred
     */
    public Problem pickAFile() throws IOException {
        return readFile(toProcessFileNames.remove(0));
    }

    /**
     * Resturns list of file names from directory.
     *
     * @param pathName the path name
     * @return the list of files
     */
    private List<String> readDirectory(String pathName) {
        List<String> list = new ArrayList<String>();
        Path thePath = FileSystems.getDefault().getPath(pathName);
        try (DirectoryStream<Path> ds = Files.newDirectoryStream(thePath, FILE_EXT)) {
            for (Path datFile : ds) {
                list.add(datFile.toFile().getName());
            }
            if (list.size() == 0) {
                throw new IllegalStateException("Could not find any "
                        + FILE_EXT + " files in path '" + pathName + "'");
            }
        } catch (IOException e) {
            throw new IllegalStateException("Could not open path '" + pathName + "'", e);
        }
        return list;
    }
}
