package polytech.parakkol;

/**
 * Standard parameters interface used by Statistics.
 *
 * @author 'Daniel TISCHER'
 *
 */
public interface Parameters {

    /**
     * Gets the max evaluations.
     *
     * @return the max evaluations
     */
    int getMaxEvaluations();

    /**
     * Gets the run count.
     *
     * @return the run count
     */
    int getRunCount();
}