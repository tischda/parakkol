package polytech.parakkol.fx.detail;

import javafx.application.Platform;
import javafx.scene.control.ProgressBar;
import polytech.parakkol.actor.fx.ProgressUpdateable;

/**
 * Wrapper class to make ProgressBar updateable by master actor.
 *
 * @author 'Daniel TISCHER'
 *
 */
public final class DetailProgressBar implements ProgressUpdateable {

    private final ProgressBar progressBar;

    public DetailProgressBar(ProgressBar progressBar) {
        this.progressBar = progressBar;
    }

    @Override
    public void update(final double progress) {

        // JavaFX updates need to run on the application thread
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                progressBar.setProgress(progress);
            }
        });
    }
}
