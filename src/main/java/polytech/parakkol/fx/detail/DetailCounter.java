package polytech.parakkol.fx.detail;

import javafx.application.Platform;
import javafx.scene.control.TextField;
import polytech.parakkol.actor.fx.TextFieldUpdateable;

/**
 * Wrapper class to make TextField updateable by master actor.
 *
 * @author 'Daniel TISCHER'
 *
 */
public final class DetailCounter implements TextFieldUpdateable {

    private final TextField counterField;

    public DetailCounter(TextField field) {
        counterField = field;
    }

    @Override
    public void update(final int count) {

        // JavaFX updates need to run on the application thread
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                counterField.setText("" + count);
            }
        });
    }
}
