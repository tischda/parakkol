package polytech.parakkol.fx.detail;

import javafx.application.Platform;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import polytech.parakkol.actor.ActorState;
import polytech.parakkol.actor.Message.SolverMetrics;
import polytech.parakkol.actor.Message.SolverStatus;
import polytech.parakkol.actor.fx.StatusListUpdateable;

/**
 * This class makes list updates transparent by mapping SolverStatus to JavaFX
 * DetailModel properties. This updates the TableView cells automatically.
 *
 * http://stackoverflow.com/questions/13944013/implement-observablelist-extend-
 * observablelistwrapper
 *
 * @author 'Daniel TISCHER'
 *
 */

public final class DetailObservableList extends SimpleListProperty<DetailModel>
        implements StatusListUpdateable {

    private final Logger log = LoggerFactory.getLogger(getClass());

    /**
     * Constructor.
     */
    public DetailObservableList() {
        super(FXCollections.<DetailModel> observableArrayList());
    }

    @Override
    public void update(final Object message) {

        // UI updates must run on the JavaFX application thread
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if (message instanceof SolverStatus) {
                    updateList((SolverStatus) message);
                } else if (message instanceof SolverMetrics) {
                    updateList((SolverMetrics) message);
                }
            }
        });
    }

    /**
     * Updates the record if found, otherwise adds a new record.
     *
     * @param solverStatus
     *            the new status
     */
    private void updateList(final SolverStatus solverStatus) {
        log.debug("Updating solverStatus: " + solverStatus.name + "-"
                + solverStatus.problem);

        boolean found = false;
        for (DetailModel record : this) {
            String solver = record.getName();
            if (solver.equals(solverStatus.name)) {

                // if 'Idle', we're done (for this problem)
                String status = record.getStatus();

                // Not Idle, so either 'Starting' or 'Active'
                if (!status.equals(ActorState.IDLE.toString())) {
                    updateStatusFields(record, solverStatus);
                    found = true;
                }
            }
        }
        if (!found) {
            add(updateStatusFields(new DetailModel(), solverStatus));
        }
    }

    /**
     * Update the list with solver metrics.
     *
     * @param solverMetrics
     *            the solver metrics
     */
    private void updateList(SolverMetrics solverMetrics) {
        log.debug("Updating solverMetrics: " + solverMetrics.problem + "-"
                + solverMetrics.runCount);

        for (DetailModel record : this) {
            String problem = record.getProblem();

            // find the problem and update its metrics
            if (problem.equals(solverMetrics.problem)) {
                updateMetricsFields(record, solverMetrics);
            }
        }
    }

    /**
     * Sets status fields in the model.
     *
     * @param record the record to update
     * @param status the new status
     * @return the updated model record
     */
    private DetailModel updateStatusFields(DetailModel record, SolverStatus status) {
        record.setName(status.name);
        record.setStatus(status.status);
        record.setProblem(status.problem);
        return record;
    }

    /**
     * Updates metrics fields.
     *
     * @param record the record to update
     * @param metrics the new metrics
     */
    private void updateMetricsFields(final DetailModel record,
            final SolverMetrics metrics) {

        // add 1 because counter starts at zero
        record.setRun(metrics.runCount + 1);

        // calculatre average fitness
        double meanOfBestFitness = record.getFitness() * metrics.runCount;
        meanOfBestFitness += metrics.runMetrics.getBestSolution().getFitness();
        meanOfBestFitness /= metrics.runCount + 1;

        record.setFitness(meanOfBestFitness);
    }
}
