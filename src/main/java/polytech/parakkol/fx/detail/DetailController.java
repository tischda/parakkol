package polytech.parakkol.fx.detail;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import javax.inject.Inject;

import polytech.parakkol.actor.Message.ClearStatistics;
import polytech.parakkol.actor.Message.StartASolver;
import akka.actor.ActorRef;

/**
 * JavaFX controller for the Details AnchorPane (right pane).
 *
 * @author 'Daniel TISCHER'
 *
 */
public final class DetailController implements Initializable {

    private final DetailObservableList fxStatusList = new DetailObservableList();

    @FXML private TableView<DetailModel> solverTable;

    @FXML private TableColumn<DetailModel, String> nameColumn;
    @FXML private TableColumn<DetailModel, String> statusColumn;
    @FXML private TableColumn<DetailModel, String> problemColumn;
    @FXML private TableColumn<DetailModel, String> runColumn;
    @FXML private TableColumn<DetailModel, String> fitnessColumn;

    @FXML private ProgressBar progressBar;
    @FXML private TextField solverCount;
    @FXML private Button clearButton;
    @FXML private Button startButton;

    @Inject private ActorRef master;

    @Override
    public void initialize(URL arg0, ResourceBundle resources) {

        // localization
        // TODO localize remaining Detail fields
        if (resources != null) {
            clearButton.setText(resources.getString("clearStatistics"));
            startButton.setText(resources.getString("startSolver"));
        }

        // initialize controls
        solverTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        nameColumn.setCellValueFactory(new PropertyValueFactory<DetailModel, String>("name"));
        statusColumn.setCellValueFactory(new PropertyValueFactory<DetailModel, String>("status"));
        problemColumn.setCellValueFactory(new PropertyValueFactory<DetailModel, String>("problem"));
        runColumn.setCellValueFactory(new PropertyValueFactory<DetailModel, String>("run"));
        fitnessColumn.setCellValueFactory(new PropertyValueFactory<DetailModel, String>("fitness"));

        // link table control to list
        solverTable.setItems(fxStatusList);

        // tell master actor about updateable UI components
        configureMasterActor();
    }

    /**
     * Configures master actor.
     */

    // TODO master actor does not reconfigure after actor restart
    private void configureMasterActor() {
        DetailProgressBar fxProgressBar = new DetailProgressBar(progressBar);
        master.tell(fxProgressBar, ActorRef.noSender());

        DetailCounter fxTextField = new DetailCounter(solverCount);
        master.tell(fxTextField, ActorRef.noSender());

        master.tell(fxStatusList, ActorRef.noSender());
    }

    /**
     * Removes all items from the list and tells master to clean up.
     */
    @FXML
    void handleClear() {
        fxStatusList.clear();
        master.tell(new ClearStatistics(), ActorRef.noSender());
    }

    /**
     * Tells master to start a solver.
     */
    @FXML
    void handleStart() {
        master.tell(new StartASolver(), ActorRef.noSender());
    }
}
