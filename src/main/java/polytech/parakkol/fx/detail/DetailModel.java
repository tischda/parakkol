package polytech.parakkol.fx.detail;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * JavaFX ObservableList item backing object. Cell values are updated
 * automatically when value are set in an instance of this class.
 *
 * Attention: make sure updates are made on the JavaFX application thread!
 *
 * @author 'Daniel TISCHER'
 *
 */
public final class DetailModel {

    private final SimpleStringProperty name = new SimpleStringProperty();
    private final SimpleStringProperty status = new SimpleStringProperty();
    private final SimpleStringProperty problem = new SimpleStringProperty();
    private final SimpleIntegerProperty run = new SimpleIntegerProperty();
    private final SimpleDoubleProperty fitness = new SimpleDoubleProperty();

    // getters

    public String getName() {
        return name.get();
    }

    public String getStatus() {
        return status.get();
    }

    public String getProblem() {
        return problem.get();
    }

    public int getRun() {
        return run.get();
    }

    public double getFitness() {
        return fitness.get();
    }

    // setters

    public void setName(String newName) {
        name.set(newName);
    }

    public void setStatus(String newStatus) {
        status.set(newStatus);
    }

    public void setProblem(String newProblem) {
        problem.set(newProblem);
    }

    public void setRun(int newRun) {
        run.set(newRun);
    }

    public void setFitness(double newFitness) {
        fitness.set(newFitness);
    }

    // properties

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public SimpleStringProperty statusProperty() {
        return status;
    }

    public SimpleStringProperty problemProperty() {
        return problem;
    }

    public SimpleIntegerProperty runProperty() {
        return run;
    }

    public SimpleDoubleProperty fitnessProperty() {
        return fitness;
    }
}
