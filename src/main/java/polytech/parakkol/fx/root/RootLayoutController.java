package polytech.parakkol.fx.root;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JavaFX controller for the root layout (menu bar and area below).
 *
 * @author 'Daniel TISCHER'
 *
 */
public final class RootLayoutController implements Initializable {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @FXML private Menu fileMenu;
    @FXML private Menu helpMenu;

    @FXML private MenuItem closeMenu;
    @FXML private MenuItem aboutMenu;

    @Inject private Stage mainStage;

    @Override
    public void initialize(URL arg0, ResourceBundle resources) {

        // TODO localization
        if (resources != null) {
            fileMenu.setText(resources.getString("file"));
            helpMenu.setText(resources.getString("help"));
            closeMenu.setText(resources.getString("close"));
            aboutMenu.setText(resources.getString("about"));
        }

    }

    @FXML
    void handleClose() {
        mainStage.close();
    }

    @FXML
    void handleAbout() {
        // TODO: get actual version from build-info.properties
        log.info("Parakkol v2.3");
    }
}
