package polytech.parakkol.fx.master;

import javafx.application.Platform;
import javafx.scene.control.TextArea;
import polytech.parakkol.actor.fx.TextAreaUpdateable;

/**
 * Wrapper class to make TextArea updateable by master actor.
 *
 * @author 'Daniel TISCHER'
 *
 */
public class MasterTextArea implements TextAreaUpdateable {

    private final TextArea statusArea;

    public MasterTextArea(TextArea area) {
        statusArea = area;
    }

    @Override
    public void update(final String text) {

        // JavaFX updates need to run on the application thread
        Platform.runLater(new Runnable() {
              @Override
              public void run() {
                  statusArea.setText(text);
              }
        });
    }
}
