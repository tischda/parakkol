package polytech.parakkol.fx.master;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import polytech.parakkol.ConfigManager;
import polytech.parakkol.ConfigManager.Keys;

/**
 * JavaFX model for Parameters.
 *
 * @author 'Daniel TISCHER'
 *
 */
public class MasterModel {

    private final ConfigManager configManager;

    private final IntegerProperty populationSize;
    private final IntegerProperty elites;
    private final DoubleProperty crossoverRate;
    private final DoubleProperty mutationRate;
    private final DoubleProperty mutationSpread;
    private final IntegerProperty maxGenerations;
    private final IntegerProperty maxEvaluations;
    private final IntegerProperty runCount;

    /**
     * Constructor.
     */
    public MasterModel() {
        configManager = new ConfigManager();

        populationSize = new SimpleIntegerProperty();
        elites = new SimpleIntegerProperty();
        crossoverRate = new SimpleDoubleProperty();
        mutationRate = new SimpleDoubleProperty();
        mutationSpread = new SimpleDoubleProperty();
        maxGenerations = new SimpleIntegerProperty();
        maxEvaluations = new SimpleIntegerProperty();
        runCount = new SimpleIntegerProperty();
    }

    // Getters

    public int getPopulationSize() {
        return populationSize.getValue();
    }
    public int getElites() {
        return elites.getValue();
    }
    public double getCrossoverRate() {
        return crossoverRate.getValue();
    }
    public double getMutationRate() {
        return mutationRate.getValue();
    }
    public double getMutationSpread() {
        return mutationSpread.getValue();
    }
    public int getMaxGenerations() {
        return maxGenerations.getValue();
    }
    public int getMaxEvaluations() {
        return maxEvaluations.getValue();
    }
    public int getRunCount() {
        return runCount.getValue();
    }

    // Setters

    public void setPopulationSize(int populationSize) {
        this.populationSize.set(populationSize);
    }
    public void setElites(int elites) {
        this.elites.set(elites);
    }
    public void setCrossoverRate(double crossoverRate) {
        this.crossoverRate.set(crossoverRate);
    }
    public void setMutationRate(double mutationRate) {
        this.mutationRate.set(mutationRate);
    }
    public void setMutationSpread(double mutationSpread) {
        this.mutationSpread.set(mutationSpread);
    }
    public void setMaxGenerations(int maxGenerations) {
        this.maxGenerations.set(maxGenerations);
    }
    public void setMaxEvaluations(int maxEvaluations) {
        this.maxEvaluations.set(maxEvaluations);
    }
    public void setRunCount(int runCount) {
        this.runCount.set(runCount);
    }

    // Properties

    public IntegerProperty populationSizeProperty() {
        return populationSize;
    }
    public IntegerProperty elitesProperty() {
        return elites;
    }
    public DoubleProperty crossoverRateProperty() {
        return crossoverRate;
    }
    public DoubleProperty mutationRateProperty() {
        return mutationRate;
    }
    public DoubleProperty mutationSpreadProperty() {
        return mutationSpread;
    }
    public IntegerProperty maxGenerationsProperty() {
        return maxGenerations;
    }
    public IntegerProperty maxEvaluationsProperty() {
        return maxEvaluations;
    }
    public IntegerProperty runCountProperty() {
        return runCount;
    }

    public void load() throws IOException {
        configManager.load();
        setPopulationSize(configManager.getIntConfigValue(ConfigManager.Keys.POPULATION_SIZE));
        setElites(configManager.getIntConfigValue(ConfigManager.Keys.ELITES));
        setCrossoverRate(configManager.getDoubleConfigValue(ConfigManager.Keys.CROSSOVER_RATE));
        setMutationRate(configManager.getDoubleConfigValue(ConfigManager.Keys.MUTATION_RATE));
        setMutationSpread(configManager.getDoubleConfigValue(ConfigManager.Keys.MUTATION_SPREAD));
        setMaxGenerations(configManager.getIntConfigValue(ConfigManager.Keys.MAX_GENERATIONS));
        setMaxEvaluations(configManager.getIntConfigValue(ConfigManager.Keys.MAX_EVALUATIONS));
        setRunCount(configManager.getIntConfigValue(ConfigManager.Keys.RUN_COUNT));
    }

    public void save() throws IOException {
        Map<Keys, Object> configValues = new HashMap<Keys, Object>();
        configValues.put(ConfigManager.Keys.POPULATION_SIZE, getPopulationSize());
        configValues.put(ConfigManager.Keys.ELITES, getElites());
        configValues.put(ConfigManager.Keys.CROSSOVER_RATE, getCrossoverRate());
        configValues.put(ConfigManager.Keys.MUTATION_RATE, getMutationRate());
        configValues.put(ConfigManager.Keys.MUTATION_SPREAD, getMutationSpread());
        configValues.put(ConfigManager.Keys.MAX_GENERATIONS, getMaxGenerations());
        configValues.put(ConfigManager.Keys.MAX_EVALUATIONS, getMaxEvaluations());
        configValues.put(ConfigManager.Keys.RUN_COUNT, getRunCount());

        configManager.setConfigValues(configValues);
        configManager.save();
    }
}
