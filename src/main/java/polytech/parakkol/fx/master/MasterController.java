package polytech.parakkol.fx.master;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;

import javax.inject.Inject;

import polytech.parakkol.ConfigManager;
import polytech.parakkol.fx.detail.DetailView;
import akka.actor.ActorRef;

/**
 * JavaFX controller for the Master AnchorPane (left pane).
 *
 * @author 'Daniel TISCHER'
 *
 */
public final class MasterController implements Initializable {

    @FXML private SplitPane splitPane;

    @FXML private TextField populationSize;
    @FXML private TextField elites;
    @FXML private TextField crossoverRate;
    @FXML private TextField mutationRate;
    @FXML private TextField mutationSpread;
    @FXML private TextField maxGenerations;
    @FXML private TextField maxEvaluations;
    @FXML private TextField runCount;

    @FXML private TextArea statusArea;

    @FXML private Button saveParametersButton;

    @Inject private ActorRef master;

    /** backing object for form data */
    private final MasterModel model = new MasterModel();

    @Override
    public void initialize(URL arg0, ResourceBundle resources) {

        // TODO localization, also for status messages
        if (resources != null) {
            saveParametersButton.setText(resources.getString("saveParameters"));
        }

        DetailView detailView = new DetailView();
        splitPane.getItems().add(detailView.getView());

        bindViewToModel();

        try {
            model.load();
        } catch (IOException e) {
            statusArea.setText(e.getMessage().toString());
        }

        // tell master actor about updateable UI components
        configureMasterActor();

        // set UI behaviour on parameters edit
        addTextFieldListeners();

    }

    /**
     * Configures master actor.
     */

    // TODO master actor does not reconfigure after actor restart
    private void configureMasterActor() {
        MasterTextArea fxTextArea= new MasterTextArea(statusArea);
        master.tell(fxTextArea, ActorRef.noSender());
    }

    /**
     * Saves the parameters and tells master actor about new configuration.
     */
    @FXML
    void handleSaveParameters() {
        try {
            model.save();

            // show converter glitches in the UI ('sfqsfdq' -> 0.0)
            refreshTextFieldsFromActualValues();

            statusArea.setText("Parameters saved.");
            master.tell((new ConfigManager()).load(), ActorRef.noSender());
        }
        catch(IOException e) {
            statusArea.setText(e.getMessage().toString());
        }
    }

    /**
     * JavaFX tries to be smart and converts strings to 0.0
     * Let's notify a change and retrieve the actual values.
     *
     * @throws IOException if parameters cannot be loaded
     */
    private void refreshTextFieldsFromActualValues() throws IOException {
        model.setPopulationSize(-1);
        model.setElites(-1);
        model.setCrossoverRate(-1.0);
        model.setMutationRate(-1.0);
        model.setMutationSpread(-1.0);
        model.setMaxGenerations(-1);
        model.setMaxEvaluations(-1);
        model.setRunCount(-1);
        model.load();
    }

    /**
     * Binds the JavaFX TextFields to the underlying model.
     */
    private void bindViewToModel() {
        // custom converter
        NumberStringConverter converter = new NumberStringConverter(new DecimalFormat("######.##"));

        // bindings
        populationSize.textProperty().bindBidirectional(model.populationSizeProperty(), converter);
        elites.textProperty().bindBidirectional(model.elitesProperty(), converter);
        crossoverRate.textProperty().bindBidirectional(model.crossoverRateProperty(), converter);
        mutationRate.textProperty().bindBidirectional(model.mutationRateProperty(), converter);
        mutationSpread.textProperty().bindBidirectional(model.mutationSpreadProperty(), converter);
        maxGenerations.textProperty().bindBidirectional(model.maxGenerationsProperty(), converter);
        maxEvaluations.textProperty().bindBidirectional(model.maxEvaluationsProperty(), converter);
        runCount.textProperty().bindBidirectional(model.runCountProperty(), converter);
    }

    /**
     * Sets 'modified' marker when TextFields are edited.
     */
    private void addTextFieldListeners() {
        ChangeListener<String> changeListener = new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable,
                    String oldValue, String newValue) {
                statusArea.setText("*");
            }
        };
        populationSize.textProperty().addListener(changeListener);
        elites.textProperty().addListener(changeListener);
        crossoverRate.textProperty().addListener(changeListener);
        mutationRate.textProperty().addListener(changeListener);
        mutationSpread.textProperty().addListener(changeListener);
        maxGenerations.textProperty().addListener(changeListener);
        maxEvaluations.textProperty().addListener(changeListener);
        runCount.textProperty().addListener(changeListener);
    }

    /**
     * Returns contents of maxEvaluations TextField (for testing).
     *
     * @return the value of the maxEvaluations TextField
     */
    int getMaxEvaluations() {
        return Integer.parseInt(maxEvaluations.getText());
    }
}
