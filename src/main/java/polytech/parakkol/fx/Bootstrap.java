package polytech.parakkol.fx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import polytech.parakkol.ConfigManager;
import polytech.parakkol.actor.MasterActor;
import polytech.parakkol.fx.afterburner.DependencyInjector;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.kernel.Bootable;

/**
 * Main class to start Parakkol. Inspired from:
 *
 *  http://doc.akka.io/docs/akka/2.2.3/java/microkernel.html.
 *  http://stackoverflow.com/questions/17343287/whats-the-point-of-the-akka-microkernel-using-java-and-maven
 *
 * @author 'Daniel TISCHER'
 *
 */
public final class Bootstrap implements Bootable {

    private static final Logger log = LoggerFactory.getLogger(Bootable.class);

    private final ActorSystem system = ActorSystem.create("parakkol");
    private final ActorRef masterActor;

    /**
     * Constructor. Creates the master actor and the UI.
     */
    public Bootstrap() {
        ConfigManager configManager = new ConfigManager();
        Props props = Props.create(MasterActor.class, configManager.load());
        masterActor = system.actorOf(props, MasterActor.NAME);
    }

    /**
     * Main methods. Starts the Parakkol actors and JavaFX main application.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();

        log.info("Starting AKKA from Bootstrap");
        bootstrap.startup();

        // injecting class of type ActorRef, beware: there can be only one!
        DependencyInjector.initializeCacheWith(ActorRef.class, bootstrap.masterActor);

        // this will block until the UI application is closed
        MainApp.main(args);

        log.info("Application closed, shutting down AKKA");
        bootstrap.shutdown();
    }

    @Override
    public void startup() {
        // nothing special to do here
    }

    @Override
    public void shutdown() {
        system.shutdown();
    }
}
