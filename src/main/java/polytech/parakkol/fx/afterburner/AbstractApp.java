package polytech.parakkol.fx.afterburner;

import javafx.application.Application;

/**
 * This abstract class guarantees taht its children are singletons. It also
 * ensures that the afterburner dependency injection is shut down properly.
 *
 * @author 'Daniel TISCHER'
 */
public abstract class AbstractApp extends Application {

    // http://stackoverflow.com/questions/13943654/how-to-create-a-singleton-mainclass-in-javafx
    private static AbstractApp instance;

    public AbstractApp() {
        super();
        synchronized (AbstractApp.class) {
            if (instance != null) {
                throw new UnsupportedOperationException(getClass()
                        + " is singleton but constructor called more than once");
            }
            instance = this;
        }
    }

    @Override
    public void stop() {
        DependencyInjector.shutdownAll();
    }
}
