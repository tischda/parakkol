package polytech.parakkol.fx.afterburner;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Collection;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

/**
 * Inspired from Adam Bien's InjectionProvider. Refactored for own requirements:
 *
 *   - initialize with external Properties
 *   - single, threadsafe cache for all classes
 *   - removed code not used in this application
 *
 * @author adam-bien.com
 * @author 'Daniel TISCHER'
 */
public final class DependencyInjector {

    private static final ConcurrentHashMap<Class<?>, Object> CACHED_INSTANCES =
            new ConcurrentHashMap<Class<?>, Object>();

    private static final Properties PROPERTIES = new Properties();

    /**
     * Adds a configured instance to the instance cache so it can be injected.
     * Obviously, this method must be called before the target is instantiated.
     *
     * @param clazz the class
     * @param instance the instance
     */
    public static void initializeCacheWith(Class<?> clazz, Object instance) {
        cacheInstance(clazz, instance);
    }

    /**
     * Put all external Properties into PROPERTIES so they can be injected.
     * Obviously, this method should be called before injection happens.
     *
     * @param externalProperties the external properties
     */
    public static void initializePropertiesWith(Properties externalProperties) {
        PROPERTIES.putAll(externalProperties);
    }

    /**
     * Destroys all instances managed by this injector by calling their methods
     * annotated with PreDestroy. You should call this method in the stop()
     * method of your application.
     */
    public static void shutdownAll() {
        Collection<Object> instances = CACHED_INSTANCES.values();
        for (Object object : instances) {
            invokeMethodsAnnotated(object.getClass(), object, PreDestroy.class);
        }
        CACHED_INSTANCES.clear();
    }

    /**
     * Instantiates the class (controller for the view), injects the annotated
     * members, invokes annoted methods and caches the instance.
     *
     * @param clazz
     *            the class to instantiate
     * @return the injected class
     */
    public static Object getInjected(Class<?> clazz) {
        try {
            return instantiateInitializeAndCache(clazz);
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new IllegalStateException("Cannot instantiate: " + clazz, ex);
        }
    }

    /**
     * Instantiates a new object to be injected and caches it. The object will
     * be  instantiated only once (we assume all objects to be singletons).
     *
     * @param clazz
     *            the class to instantiate
     * @return the instantiated object
     * @throws InstantiationException
     *             if there is an exception
     * @throws IllegalAccessException
     *             if there is an exception
     */
    static Object instantiateInitializeAndCache(Class<?> clazz)
            throws InstantiationException, IllegalAccessException {

        Object instance = CACHED_INSTANCES.get(clazz);
        if (instance == null) {
            instance = clazz.newInstance();
            injectMembers(instance.getClass(), instance);
            invokeMethodsAnnotated(instance.getClass(), instance, PostConstruct.class);
            cacheInstance(clazz, instance);
        }
        return instance;
    }

    /**
     * Injects class members.
     *
     * @param clazz
     *            the class to inject
     * @param instance
     *            the target instance to inject
     * @throws InstantiationException
     *             if there is an exception
     * @throws IllegalAccessException
     *             if there is an exception
     */
    static void injectMembers(Class<? extends Object> clazz,
            final Object instance) throws InstantiationException,
            IllegalAccessException {

        Field[] fields = clazz.getDeclaredFields();
        for (final Field field : fields) {
            if (field.isAnnotationPresent(Inject.class)) {
                Class<?> type = field.getType();

                // inject a String Property
                if (type.isAssignableFrom(String.class)) {
                    injectIntoField(field, instance,
                            getPropertyValue(field.getName()));

                // inject an instance of type
                } else {
                    final Object value = instantiateInitializeAndCache(type);
                    injectIntoField(field, instance, value);
                }
            }
        }
        // move up class hierarchy recursively
        Class<? extends Object> superclass = clazz.getSuperclass();
        if (superclass != null) {
            injectMembers(superclass, instance);
        }
    }

    /**
     * Returns the property value, overriding Properties with system Properties.
     *
     * @param key
     *            the key
     * @return the property value
     */
    private static String getPropertyValue(String key) {
        String systemProperty = System.getProperty(key);
        return (String) ((systemProperty != null) ? systemProperty : PROPERTIES.get(key));
    }

    /**
     * Injects value into field of the instance.
     *
     * @param field
     *            the field to inject
     * @param instance
     *            the target instance
     * @param value
     *            the value
     */
    static void injectIntoField(final Field field, final Object instance,
            final Object value) {

        AccessController.doPrivileged(new PrivilegedAction<Object>() {
            @Override
            public Object run() {
                boolean wasAccessible = field.isAccessible();
                try {
                    field.setAccessible(true);
                    field.set(instance, value);
                    return null;
                } catch (IllegalAccessException ex) {
                    throw new IllegalStateException("Cannot set field: " + field, ex);
                } finally {
                    field.setAccessible(wasAccessible);
                }
            }
        });
    }

    /**
     * Invokes the methods annotated with annotationClass on the target object.
     *
     * @param instance
     *            the target instance to invoke the methods on
     */
    static void invokeMethodsAnnotated(Class<?> clazz, final Object instance,
            final Class<? extends Annotation> annotationClass) {

        Method[] declaredMethods = clazz.getDeclaredMethods();
        for (final Method method : declaredMethods) {
            if (method.isAnnotationPresent(annotationClass)) {
                invokeMethod(instance, method);
            }
        }
        // move up class hierarchy recursively
        Class<?> superclass = clazz.getSuperclass();
        if (superclass != null) {
            invokeMethodsAnnotated(superclass, instance, annotationClass);
        }
    }

    /**
     * Invokes the method on the target object.
     *
     * @param instance
     *            the target instance to inoke the method on
     * @param method
     *            the method to invoke
     */
    private static void invokeMethod(final Object instance, final Method method) {
        AccessController.doPrivileged(new PrivilegedAction<Object>() {
            @Override
            public Object run() {
                boolean wasAccessible = method.isAccessible();
                try {
                    method.setAccessible(true);
                    return method.invoke(instance, new Object[] {});
                } catch (IllegalAccessException | InvocationTargetException ex) {
                    throw new IllegalStateException("Cannot invoke:" + method, ex);
                } finally {
                    method.setAccessible(wasAccessible);
                }
            }
        });
    }

    /**
     * Caches the instance in the ConcurrentHashMap. This is the only place we
     * add to it (so it should be threadsafe).
     *
     * @param clazz the singleton class
     * @param instance the instance to cache
     */
    private static void cacheInstance(Class<?> clazz, Object instance) {
        CACHED_INSTANCES.putIfAbsent(clazz, instance);
    }

    private DependencyInjector() {
        // utility class
    }
}
