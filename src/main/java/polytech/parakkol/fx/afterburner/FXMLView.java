package polytech.parakkol.fx.afterburner;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.util.Callback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class has been refactored from afterburner project. A few things that
 * would need clarification in the original:
 *
 *   - Runnable inside of Callable, is this intentional?
 *   - Executor is never shut down (quitting Application takes 6 seconds)
 *   - Lazy loading is not on demand, this is not lazy but 'async' loading
 *   - Lowercase convention: when .fxml is put into jar, case does matter!
 *
 * I prefer to have .fxml resources named EXACTLY the same as the class names.
 *
 * I completely removed multithreaded resource loading (not necessary).
 *
 * @author adam-bien.com
 * @author 'Daniel TISCHER'
 */
public abstract class FXMLView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final FXMLLoader fxmlLoader;
    private final String fxmlName;
    private final URL fxmlResource;

    private final Locale locale = new Locale("en", "EN");

    /**
     * Constructor.
     */
    public FXMLView() {
        fxmlName = getName() + ".fxml";
        fxmlResource = getClass().getResource(fxmlName);

        fxmlLoader = instantiateLoader();
        configureLocale();
        configureControllerInjection();
        loadObjectHierarchy();
    }

    /**
     * Gets the view by convention.
     *
     * @return the view
     */
    public Parent getView() {
        Parent parent = fxmlLoader.getRoot();
        addCSSifAvailable(parent);
        return parent;
    }

    /**
     * Gets the controller specified in the FXML view.
     *
     * @return the controller
     */
    public Object getController() {
        return fxmlLoader.getController();
    }

    private FXMLLoader instantiateLoader() {
        if (fxmlResource == null) {
            throw new IllegalStateException("Cannot find fxmlResource: "
                    + fxmlName + " for class: " + getClass().getSimpleName());
        }
        return new FXMLLoader(fxmlResource);
    }

    private void configureLocale() {
        String bundleName = stripNameEnding(getClass().getName());
        try {
            fxmlLoader.setResources(ResourceBundle.getBundle(bundleName, locale));
        }
        catch(MissingResourceException e) {
            log.warn(e.toString());
        }
    }

    private void configureControllerInjection() {
        fxmlLoader.setControllerFactory(new Callback<Class<?>, Object>() {
            @Override
            public Object call(Class<?> clazz) {
                return DependencyInjector.getInjected(clazz);
            }
        });
    }

    private void loadObjectHierarchy() {
        try {
            fxmlLoader.load();
        } catch (IOException ex) {
            throw new IllegalStateException("Cannot load " + fxmlName, ex);
        }
    }

    private void addCSSifAvailable(Parent parent) {
        URL url = getClass().getResource(getName() + ".css");
        if (url != null) {
            parent.getStylesheets().add(url.toExternalForm());
        }
    }

    private String getName() {
        return stripNameEnding(getClass().getSimpleName());
    }

    private String stripNameEnding(final String className) {
        String srippedClassName = className;
        if (className.toLowerCase().endsWith("view")) {
            int viewIndex = className.toLowerCase().lastIndexOf("view");
            srippedClassName = className.substring(0, viewIndex);
        }
        return srippedClassName;
    }
}
