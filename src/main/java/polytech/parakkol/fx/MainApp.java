package polytech.parakkol.fx;

import java.io.IOException;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import polytech.parakkol.fx.afterburner.AbstractApp;
import polytech.parakkol.fx.afterburner.DependencyInjector;
import polytech.parakkol.fx.master.MasterView;
import polytech.parakkol.fx.root.RootLayoutView;

/**
 * Main class for the JavaFX application.
 *
 * @author 'Daniel TISCHER'
 *
 */
public final class MainApp extends AbstractApp {

    /* (non-Javadoc)
     * @see javafx.application.Application#start(javafx.stage.Stage)
     */
    @Override
    public void start(Stage stage) throws IOException {

        // inject stage so window can be closed from DetailController
        DependencyInjector.initializeCacheWith(Stage.class, stage);

        stage.setTitle("Parakkol");
        Parent rootLayout = new RootLayoutView().getView();
        Scene scene = new Scene(rootLayout);
        stage.setScene(scene);
        stage.show();

        // now add MasterView to RootLayoutView
        Parent masterView = new MasterView().getView();
        ((BorderPane)rootLayout).setCenter(masterView);

        // MasterView will load the DetailView into its right pane.
    }

    /**
     * Launch the JavaFX application.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
