package polytech.parakkol;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigRenderOptions;
import com.typesafe.config.ConfigValueFactory;

/**
 * Manages the AKKA configuration and solver parameters.
 *
 * @author 'Daniel TISCHER'
 *
 */
public final class ConfigManager {
    public static final String PARAMETERS_CONFIG_KEY = "parameters";
    public static final String CUSTOM_PARAMETERS_FILENAME = "parameters.json";

    /** The config. */
    private Config config;

    /** Configuration file keys. */
    public static enum Keys {
        POPULATION_SIZE("population_size"),
        ELITES("elites"),
        CROSSOVER_RATE("crossover_rate"),
        MUTATION_RATE("mutation_rate"),
        MUTATION_SPREAD("mutation_spread"),
        MAX_GENERATIONS("max_generations"),
        MAX_EVALUATIONS("max_evaluations"),
        RUN_COUNT("run_count");

        private final String text;

        private Keys(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    /**
     * Sets a new configuration.
     *
     * @param configValues the config values
     */
    public void setConfigValues(Map<Keys, Object> configValues) {
        Config newConfig= ConfigFactory.empty();
        for(Entry<Keys, Object> entry : configValues.entrySet()) {
            newConfig = newConfig.withValue(
                    entry.getKey().toString(),
                    ConfigValueFactory.fromAnyRef(entry.getValue()));
        }
        this.config = newConfig;
    }

    /**
     * Returns the configuration integer value for the provided key.
     *
     * @param key the key
     * @return the value
     */
    public int getIntConfigValue(Keys key) {
        return config.getInt(key.toString());
    }

    /**
     * Returns the configuration double value for the provided key.
     *
     * @param key the key
     * @return the value
     */
    public double getDoubleConfigValue(Keys key) {
        return config.getDouble(key.toString());
    }

    /**
     * Loads and returns values from parameters file. Falls back to AKKA
     * configuration if not found.
     *
     * Inspired from https://groups.google.com/forum/#!topic/akka-user/kHnjrvfwHC0
     *
     * @return the config
     */
    public Config load() {
        config = ConfigFactory.parseFile(new File(CUSTOM_PARAMETERS_FILENAME))
                .withFallback(ConfigFactory.load().getConfig(PARAMETERS_CONFIG_KEY));

        // config is immutable, so this is ok.
        return config;
    }

    /**
     * Saves the solver parameters file to disk.
     *
     * @throws IOException if an IO exception occurs
     */
    public void save() throws IOException {
        Path path = FileSystems.getDefault().getPath(".", CUSTOM_PARAMETERS_FILENAME);
        String formattedJson = beautifyJSON(toJSON());
        Files.write(path, formattedJson.getBytes("UTF-8"));
    }

    /**
     * Gets the config.
     *
     * @return the config
     */
    public Config getConfig() {
        return config;
    }

    /**
     * Sets the config.
     *
     * @param config the config
     */
    public void setConfig(Config config) {
        this.config = config;
    }

    /**
     * Returns a JSON representation of the configuration.
     *
     * @return the parameters as JSON
     */
    String toJSON() {
        // https://github.com/typesafehub/config/issues/1
        return config.root().render(ConfigRenderOptions.concise());
    }

    // http://stackoverflow.com/questions/5457524/json-beautifier-library-for-java
    private String beautifyJSON(String originalJson) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        JsonNode tree = objectMapper.readTree(originalJson);
        return objectMapper.writeValueAsString(tree);
    }
}
