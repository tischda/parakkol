package polytech.parakkol.stats;

import java.util.Arrays;

import polytech.parakkol.util.ColorEncoder;

/**
 * Immutable lightweight version of Palette. Colors are encoded the same as in
 * Palette, but fitness cache and setters have been removed.
 *
 * @author 'Daniel TISCHER'
 *
 */
public final class Solution {

    private final int[] colors;
    private final double fitness;

    /**
     * Constructor.
     *
     * @param colors
     *            the colors
     * @param fitness
     *            the fitness
     */
    public Solution(final int[] colors, final double fitness) {
        this.colors = Arrays.copyOf(colors, colors.length);
        this.fitness = fitness;
    }

    /**
     * Returns the fitness of the solution.
     *
     * @return the fitness of the solution
     */
    public double getFitness() {
        return fitness;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(fitness);
        builder.append(" [ ");
        for (int color : colors) {
            int[] rgb = ColorEncoder.intToRGB(ColorEncoder.gray2bin(color));
            builder.append(ColorEncoder.hex(rgb[0], rgb[1], rgb[2]));
            builder.append(" ");
        }
        builder.append("]");
        return builder.toString();
    }
}
