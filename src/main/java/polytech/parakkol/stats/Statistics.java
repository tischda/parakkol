package polytech.parakkol.stats;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

import polytech.parakkol.Parameters;

/**
 * Gathers statistics for multiples runs of the same problem, for a fixed set
 * of parameters.
 *
 * @author 'Daniel TISCHER'
 */
public class Statistics {

    // TODO: handle case when MAX_GENERATIONS reached but not MAX_EVALUATIONS

    /** Counts the number of runs */
    final AtomicInteger runCounter = new AtomicInteger(-1);

    /** The best fitness over the execution for each run. */
    final double[][] fitness;

    /** Min and Max per evaluation for all runs */
    final double[] fitnessMin, fitnessMax;

    /** The elapsed times over the execution for each run. */
    long[][] elapsed;

    /** The parameters */
    final Parameters params;

    /** The mean of best fitnesses for all runs */
    private double meanOfBestFitness;

    /** The overall best solution */
    private Solution best;

    /**
     * Constructor.
     *
     * @param params the parameters
     */
    public Statistics(Parameters params) {
        this.params = params;

        fitness = new double[params.getRunCount()][params.getMaxEvaluations()];
        elapsed = new long[params.getRunCount()][params.getMaxEvaluations()];

        fitnessMin = new double[params.getMaxEvaluations()];
        fitnessMax = new double[params.getMaxEvaluations()];

        Arrays.fill(fitnessMin, Double.MAX_VALUE);
    }

    /**
     * Reset statistics for all runs.
     */
    public void reset() {
        runCounter.set(-1);
        best = null;
        meanOfBestFitness = 0;
        Arrays.fill(fitnessMax, Double.MIN_VALUE);
        Arrays.fill(fitnessMin, Double.MAX_VALUE);
    }

    /**
     * Record metrics for a single run.
     *
     * @param metrics
     *            the metrics
     */
    public void record(final Metrics metrics) {
        int run = runCounter.incrementAndGet();

        // TODO make Metrics truly immutable.
        // new Metrics are created for each run in solveProblem() in class SolverActor
        fitness[run] = metrics.fitness;
        elapsed[run] = metrics.elapsed;

        flattenCurve(run);

        updateMinFitness();
        updateMaxFitness();

        updateMeanOfBestFitness(metrics.getBestSolution().getFitness());
        setSolutionIfBest(metrics.getBestSolution());
    }

    /**
     * Returns the mean of each run's best fitness.
     *
     * @return the mean of the best fitness
     */
    public double getMeanOfBestFitness() {
        return meanOfBestFitness;
    }

    /**
     * Returns the best of the best fitness of each run.
     *
     * @return the best fitness
     */
    public double getOverallBestFitness() {
        return best.getFitness();
    }

    /**
     * Flatten curve (as in original project). The focus here is on how the
     * algorithm _improves_ the results, not on how the fitness values are
     * distributed. For instance, you won't see the curve going up when
     * the algorithm explores new regions.
     *
     * @param run the current run
     */
    private void flattenCurve(int run) {
        double min = Double.MAX_VALUE;
        for (int eval = 0; eval < params.getMaxEvaluations(); eval++) {
            if (fitness[run][eval] < min) {
                min = fitness[run][eval];
            }
            else {
                fitness[run][eval] = min;
            }
        }
    }

    /**
     * Updates min fitness per evaluation with the current run.
     */
    private void updateMinFitness() {
        int lastRun = runCounter.get();
        for (int i = 0; i < params.getMaxEvaluations(); i++) {
            if (fitness[lastRun][i] < fitnessMin[i]) {
                fitnessMin[i] = fitness[lastRun][i];
            }
        }
    }

    /**
     * Updates max fitness per evaluation with the current run.
     */
    private void updateMaxFitness() {
        int lastRun = runCounter.get();
        for (int i = 0; i < params.getMaxEvaluations(); i++) {
            if (fitness[lastRun][i] > fitnessMax[i]) {
                fitnessMax[i] = fitness[lastRun][i];
            }
        }
    }

    /**
     * Updates mean of best fitness.
     *
     * @param bestFitness the best fitness in the current run
     */
    private void updateMeanOfBestFitness(double bestFitness) {
        meanOfBestFitness *= runCounter.get();
        meanOfBestFitness += bestFitness;
        meanOfBestFitness /= runCounter.get() + 1;
    }

    /**
     * Records best solution.
     *
     * @param solution the current solution
     */
    private void setSolutionIfBest(final Solution solution) {
        if (best == null || solution.getFitness() < best.getFitness()) {
            best = solution;
        }
    }
}
