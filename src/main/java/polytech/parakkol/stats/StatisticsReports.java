package polytech.parakkol.stats;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import polytech.parakkol.Parameters;

/**
 * Extension of Statistics that adds saving statistical reports to files.
 *
 * @author 'Daniel TISCHER'
 *
 */
public final class StatisticsReports extends Statistics {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private static final double ONE_MILLION = 1e6;
    private static final String COL_SEPARATOR = " ";

    /**
     * Constructor.
     *
     * @param params the parameters
     */
    public StatisticsReports(Parameters params) {
        super(params);
    }

    /**
     * Calculates mean fitness by evaluation.
     *
     * @param eval the evaluation
     * @return the mean fitness value for this evaluation
     */
    double meanByEvaluation(final int eval) {
        SummaryStatistics stats = new SummaryStatistics();
        for (int run = 0; run <= runCounter.get(); run++) {
            stats.addValue(fitness[run][eval]);
        }
        return stats.getMean();
    }

    /**
     * Calculates the standard deviation by evaluation.
     *
     * @param eval the evaluation
     * @return the standard deviation for this evaluation
     */
    double stddevByEvaluation(final int eval) {
        SummaryStatistics stats = new SummaryStatistics();
        for (int run = 0; run <= runCounter.get(); run++) {
            stats.addValue(fitness[run][eval]);
        }
        return stats.getStandardDeviation();
    }

    /**
     * Saves statistics to files.
     *
     * @param fileName the file name
     * @param description the description
     * @throws FileNotFoundException if there is an exception
     * @throws UnsupportedEncodingException if there is an exception
     */
    public void saveToFiles(String fileName, String description)
            throws FileNotFoundException, UnsupportedEncodingException {
        saveFitnessByEvaluations(fileName + "-eval.dat", description);
        saveFitnessByTime(fileName + "-elapsed.dat", description);
    }

    /**
     * Saves the fitness by evaluation.
     *
     * @param fileName the file name
     * @param description the description
     * @throws FileNotFoundException if there is an exception
     * @throws UnsupportedEncodingException if there is an exception
     */
    void saveFitnessByEvaluations(String fileName, String description)
            throws FileNotFoundException, UnsupportedEncodingException {

        File file = createFile(fileName);
        try (PrintStream out = new PrintStream(file, "UTF-8")) {
            saveFileHeader(out, description, "eval");
            for (int eval = 0; eval < params.getMaxEvaluations(); eval++) {
                printRecord(out, eval, eval + 1);
            }
        }
    }

    // There are important differences in the first runs. Therefore, we will
    // print records for the run performing closest to the the mean time.
    //
    // 452 INFO p.p.g.Solver : Generations: 91, evals: 10000, time: 311.343556 ms
    // 563 INFO p.p.g.Solver : Generations: 92, evals: 10000, time: 107.700036 ms
    // 653 INFO p.p.g.Solver : Generations: 92, evals: 10000, time: 89.64477 ms

    /**
     * Saves the fitness by time.
     *
     * @param fileName the file name
     * @param description the description
     * @throws FileNotFoundException if there is an exception
     * @throws UnsupportedEncodingException if there is an exception
     */
    void saveFitnessByTime(String fileName, String description)
            throws FileNotFoundException, UnsupportedEncodingException {

        File file = createFile(fileName);
        try (PrintStream out = new PrintStream(file, "UTF-8")) {
            saveFileHeader(out, description, "elapsed");
            int run = selectRunClosestToMeanTime();
            for (int eval = 0; eval < params.getMaxEvaluations(); eval++) {
                printRecord(out, eval, elapsed[run][eval] / ONE_MILLION);
            }
        }
    }

    int selectRunClosestToMeanTime() {
        SummaryStatistics stats = new SummaryStatistics();
        for (int run = 0; run <= runCounter.get(); run++) {
            stats.addValue(elapsed[run][params.getMaxEvaluations() - 1]);
        }
        double mean = stats.getMean();
        double min = Double.MAX_VALUE;
        int minIndex = params.getRunCount();
        for (int run = 0; run <= runCounter.get(); run++) {
            double time = elapsed[run][params.getMaxEvaluations() - 1];
            double diff = Math.abs(mean - time);
            if (diff < min) {
                min = diff;
                minIndex = run;
            }
        }
        log.debug("Mean exec time of 1 run: {} ms", mean / ONE_MILLION);
        log.debug("Run idx closest to mean: #{} with time: {} ms", minIndex,
                elapsed[minIndex][params.getMaxEvaluations() - 1] / ONE_MILLION);

        return minIndex;
    }

    /**
     * Creates the file and subdirectories if necessary.
     *
     * @param fileName the file name
     * @return the file
     */
    File createFile(String fileName) {
        log.info("Saving statistics to {} ...", fileName);

        File file = new File(fileName);
        File parent = file.getParentFile();
        if (parent != null) {
            boolean created = parent.mkdirs();
            log.debug("Subdirectories created: {}", created);
        }
        return file;
    }

    /**
     * Prints a signle statistical record to the stream.
     *
     * @param out the PrintStream
     * @param eval the evaluation
     * @param firstCol the first column
     */
    void printRecord(PrintStream out, int eval, Object firstCol) {
        final double mean = meanByEvaluation(eval);
        final double stddev = stddevByEvaluation(eval);
        StringBuilder sb = new StringBuilder();
        sb.append(firstCol);
        sb.append(COL_SEPARATOR);
        sb.append(mean);
        sb.append(COL_SEPARATOR);
        sb.append(stddev);
        sb.append(COL_SEPARATOR);
        sb.append(mean - stddev);
        sb.append(COL_SEPARATOR);
        sb.append(mean + stddev);
        sb.append(COL_SEPARATOR);
        sb.append(fitnessMin[eval]);
        sb.append(COL_SEPARATOR);
        sb.append(fitnessMax[eval]);
        sb.append(COL_SEPARATOR);
        out.println(sb.toString());
    }

    /**
     * Saves the file header.
     *
     * @param out the PrintStream
     * @param description the description
     * @param type the type of data ("eval" or "elapsed")
     */
    void saveFileHeader(PrintStream out, String description, String type) {

        // The context
        out.println("# Params: [");
        try (Scanner scanner = new Scanner(params.toString())) {
            scanner.nextLine();
            while (scanner.hasNextLine()) {
                out.println("#         " + scanner.nextLine());
            }
        }

        // The problem
        out.println("# Problem description:");
        try (Scanner scanner = new Scanner(description)) {
            while (scanner.hasNextLine()) {
                out.println("# " + scanner.nextLine());
            }
        }
        out.println("#");

        // The data column headings
        out.print("#" + type + " mean stddev mean-stddev mean+stddev min max");
        out.println();
    }
}
