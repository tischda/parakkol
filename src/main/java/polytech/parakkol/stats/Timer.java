package polytech.parakkol.stats;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Handles time metrics. Works like a stopwatch.
 *
 * Overridden by Metrics to add time to solver metrics.
 * Overridden by TestingTimer to test timing issues.
 *
 * @author 'Daniel TISCHER'
 *
 */
public class Timer {

    /** true if timer has been started */
    private final AtomicBoolean started = new AtomicBoolean();

    /** true if timer has been started and not yet stopped */
    private final AtomicBoolean running = new AtomicBoolean();

    /** nanos when timer was started */
    private long startTime;

    /** nanos when timer was stopped */
    private long endTime;

    /**
     * Starts the timer.
     */
    public void start() {
        started.set(true);
        running.set(true);
        startTime = System.nanoTime();
    }

    /**
     * Stops the timer.
     */
    public void stop() {
        running.set(false);
        endTime = System.nanoTime();
    }

    /**
     * Returns true if the timer has been started, i.e. start() has been called.
     * Still returns true after stop();
     *
     * @return true if timer has been started, false otherwise
     */
    public boolean isTimerStarted() {
        return started.get();
    }

    /**
     * Returns true if the timer is running, i.e. start() has been called but
     * not stop().
     *
     * @return true if timer is running, false otherwise
     */
    public boolean isTimerRunning() {
        return running.get();
    }

    /**
     * Resets the timer.
     */
    public void reset() {
        started.set(false);
        running.set(false);
        startTime = 0;
        endTime = 0;
    }

    /**
     * Returns nanos since start.
     *
     * @return nano seconds since start.
     */
    public long getElapsedTime() {
        if (!started.get()) {
            throw new IllegalStateException(
                    "You need to start the timer before measuring");
        }
        if (running.get()) {
            endTime = System.nanoTime();
        }
        return endTime - startTime;
    }
}
