package polytech.parakkol.stats;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Records fitness, elapsed time and best solution for a single solver run.
 * Once the run is completed, do not reuse this object. An improvement would
 * be to make this object immutable after processing. Avoid deep copies of
 * this object since it contains tons of statistical data.
 *
 * @author 'Daniel TISCHER'
 *
 * @composed "1..1" - "0..n" Solution
 */
public class Metrics extends Timer {

    private final AtomicInteger evalCounter = new AtomicInteger(-1);

    final double[] fitness;
    final long[] elapsed;
    private Solution best;

    /**
     * Constructor.
     *
     * @param size number of evaluations
     */
    public Metrics(int size) {
        fitness = new double[size];
        elapsed = new long[size];
    }

    /**
     * Records a solution.
     *
     * @param solution the solution
     */
    public void record(final Solution solution) {
        int eval = evalCounter.incrementAndGet();

        fitness[eval] = solution.getFitness();
        elapsed[eval] = getElapsedTime();

        setSolutionIfBest(solution);
    }

    /**
     * Returns the overall best solution.
     *
     * @return the best solution or null if none
     */
    public Solution getBestSolution() {
        return best;
    }

    /**
     * Returns evalutions count.
     *
     * @return the current evaluation count
     */
    public int getCurrentEvaluationCount() {
        // add 1 because counter starts at zero
        return evalCounter.get() + 1;
    }

    /**
     * Compares the argument with recorded best solution and replaces it when
     * it is better.
     *
     * @param solution the solution
     */
    private void setSolutionIfBest(final Solution solution) {
        if (best == null || solution.getFitness() < best.getFitness()) {
            best = solution;
        }
    }
}
