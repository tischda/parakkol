package polytech.parakkol.model;

import static polytech.parakkol.util.ColorUtil.brightnessDifferenceScaled;
import static polytech.parakkol.util.ColorUtil.contrastDifferenceScaled;
import static polytech.parakkol.util.ColorUtil.distanceLAB;
import static polytech.parakkol.util.ColorUtil.tonalityDifferenceScaled;

import java.util.HashSet;
import java.util.Set;

import polytech.parakkol.util.ColorEncoder;

/**
 * Evaluates a solution against a problem. As long as the problem data does not
 * change, the same evaluator instance can be reused for multiple palettes.
 *
 * @author 'Daniel TISCHER'
 *
 */
public final class Evaluator {

    /**
     * The weight applied to brightness, contrast and tonality differences in
     * the fitness evaluation formula.
     */
    private static final double ALPHA = 0.8;

    /** Number of color components: red + green + blue */
    private static final int RGB_3 = 3;

    /** Number of metrics: brightnessDifference + tonalityDifference + contrastRatio */
    private static final int NB_METRICS = 3;

    /** The maximum distance between two colors in CIE L*a*b*. */
    private static final double MAX_DISTANCE_LAB = 100 * Math.sqrt(3);

    /** The problem associated with this Evaluator */
    private final Problem problem;

    /** Pre-calculated lab color cache */
    private final double[][] labColors;

    /** Listeners that need to know about evaluation events */
    private final Set<EvaluationListener> listeners = new HashSet<EvaluationListener>();

    /**
     * Constructor. Note that problem.palette needs to be initialized so that
     * lab colors can be pre-calculated.
     *
     * @param problem            the problem
     * @param colors the colors
     */
    public Evaluator(Problem problem, int[][] colors) {
        this.problem = problem;
        this.labColors = new double[problem.numberOfColors][RGB_3];
        precalculateLabColors(colors);
    }

    /**
     * Pre-compiles labColors from the Problem palette.
     *
     * @param colors the colors
     */
    private void precalculateLabColors(int[][] colors) {
        for (int i = 0; i < labColors.length; i++) {
            labColors[i] = ColorEncoder.sRGBToLab(colors[i]);
        }
    }

    /**
     * Adds a listener to the Evaluator.
     *
     * @param listener
     *            the listener
     */
    public void addListener(EvaluationListener listener) {
        listeners.add(listener);
    }

    /**
     * Removes a listener from the Evaluator.
     *
     * @param listener
     *            the listener
     */
    public void removeListener(EvaluationListener listener) {
        listeners.remove(listener);
    }

    /**
     * Evaluates a color palette. This accesses and modifies the fitness cache
     * in the object passed as parameter.
     *
     * @param palette            the palette to evaluate
     * @return the fitness value of the palette
     */
    public double evaluate(Palette palette) {
        if (!palette.isEvaluated() && problem.numberOfPairs != 0) {
            palette.cache.fitness = sumFitnessOfPairs(palette) / problem.numberOfPairs;
            notifyListeners(palette);
        }
        return palette.cache.fitness;
    }

    /**
     * Calculates the sum of the fitness for all pairs in the palette.
     *
     * @param palette the color palette
     * @return the sum of fitness
     */
    private double sumFitnessOfPairs(Palette palette) {
        double sum = 0;
        for (int i = 0; i < problem.numberOfPairs; i++) {
            if (palette.cache.fitnessOfPair[i].isNaN()) {
                palette.cache.fitnessOfPair[i] = evaluatePair(palette, problem.pairs[i]);
            }
            sum += palette.cache.fitnessOfPair[i];
        }
        return sum;
    }

    /**
     * Evaluates pair fitness of a single pair.
     *
     * @param palette the color palette
     * @param pair the pair to evaluate
     * @return the fitness of this pair
     */
    private Double evaluatePair(Palette palette, int[] pair) {
        int[] newFg = palette.getRGBColor(pair[0]);
        int[] newBg = palette.getRGBColor(pair[1]);

        // brightness, tonality and contrast
        double brightnessDifference = brightnessDifferenceScaled(newFg, newBg);
        double tonalityDifference = tonalityDifferenceScaled(newFg, newBg);
        double contrastRatio = contrastDifferenceScaled(newFg, newBg);

        double colorDistance = 0;
        colorDistance += distanceLAB(labColors[pair[0]], ColorEncoder.sRGBToLab(newFg));
        colorDistance += distanceLAB(labColors[pair[1]], ColorEncoder.sRGBToLab(newBg));
        colorDistance /= (MAX_DISTANCE_LAB * 2);

        double result = ALPHA
                * ((brightnessDifference + tonalityDifference + contrastRatio) / NB_METRICS)
                + (1 - ALPHA) * colorDistance;

        return result;
    }

    /**
     * Notifies listeners about completed evaluations.
     *
     * @param palette the color palette that was evaluated
     */
    private void notifyListeners(Palette palette) {
        for (EvaluationListener l : listeners) {
            l.fitnessEvaluated(palette);
        }
    }
}
