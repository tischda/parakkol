package polytech.parakkol.model;

import java.util.Arrays;
import java.util.Set;

import polytech.parakkol.util.ColorPairMap;

/**
 * Caches pair fitness to speed up recalculation of palette fitness since only
 * modified pairs need to be recalculated and summed up. There is a bit more to
 * it since modifying a single color may impact other pairs that references the
 * same color, so a single color change can affect multiple other pairs.
 *
 * @author 'Daniel TISCHER'
 *
 * @has 1 - 1 Evaluator
 * @has 1 - 1 ColorPairMap
 */
public final class FitnessCache {

    /** The number of pairs */
    private final int numberOfPairs;

    /** Maps colors to pairs (to track color changes impacting pairs) */
    private final ColorPairMap reverseIndex;

    /** The evaluator that calculates the fitness when getFitness is called. */
    private final Evaluator evaluator;

    /** Current fitness of the palette. Updated by Evaluator. */
    Double fitness = Double.NaN;

    /** Fitness cache for evaluated color pairs. Updated by Evaluator. */
    Double[] fitnessOfPair;

    /**
     * Constructor.
     *
     * @param numberOfPairs
     *            number of pairs
     * @param reverseIndex
     *            reverse color-pair index
     * @param evaluator
     *            the evaluator
     */
    public FitnessCache(int numberOfPairs, ColorPairMap reverseIndex, Evaluator evaluator) {
        this.numberOfPairs = numberOfPairs;
        this.reverseIndex = reverseIndex;
        this.evaluator = evaluator;
        initializeFitness();
    }

    /**
     * Creates a deep copy of the FitnessCache.
     *
     * @return the copy
     */
    public FitnessCache copy() {
        FitnessCache result = new FitnessCache(numberOfPairs, reverseIndex, evaluator);
        result.fitness = fitness;
        result.fitnessOfPair = Arrays.copyOf(fitnessOfPair, fitnessOfPair.length);
        return result;
    }

    /**
     * Gets the fitness.
     *
     * @param palette the palette
     * @return the fitness
     */
    public double getFitness(Palette palette) {
        return evaluator.evaluate(palette);
    }

    /**
     * Returns true if the cache is invalid, which means that either fitness has
     * never been computed or a color has changed and some pair fitness needs to
     * be recomputed.
     *
     * @return true if fitness needs to be recomputed, false otherwise.
     */
    public boolean isInvalid() {
        return fitness.isNaN();
    }

    /**
     * Initializes the cache.
     */
    private void initializeFitness() {
        fitnessOfPair = new Double[numberOfPairs];
        for (int i = 0; i < numberOfPairs; i++) {
            fitnessOfPair[i] = Double.NaN;
        }
        resetFitness();
    }

    /**
     * Invalidates the fitness of the pairs affected by a color change. This
     * impacts the global fitness, so it is invalidated as well.
     *
     * This method does nothing when evaluator has not been set, eg. when a
     * palette is being initialized.
     *
     * @param colorIndex the color index
     */
    public void invalidateFitness(int colorIndex) {
        Set<Integer> affectedPairs = reverseIndex.getPairsWithColor(colorIndex);
        resetPairFitness(affectedPairs);
        resetFitness();
    }

    /**
     * Resets the fitness for the set of pairs provided as argument.
     *
     * @param pairs the pairs that need to be reset
     */
    private void resetPairFitness(Set<Integer> pairs) {
        if (pairs != null) {
            for (int pair : pairs) {
                fitnessOfPair[pair] = Double.NaN;
            }
        }
    }

    /**
     * Resets the cached color palette fitness.
     */
    private void resetFitness() {
        fitness = Double.NaN;
    }
}
