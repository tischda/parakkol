package polytech.parakkol.model;

/**
 * Notifies when an evaluation is peformed on an Evaluator.
 *
 * @author 'Daniel TISCHER'
 *
 */
public interface EvaluationListener {
    void fitnessEvaluated(Palette colorPalette);
}
