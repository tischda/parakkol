package polytech.parakkol.model;

import java.util.Arrays;

import polytech.parakkol.util.ColorEncoder;

/**
 * A representation of the color problem data, for initial colors
 * (problem.palette) and potential solutions.
 *
 * Each value is one color (RGB) encoded in an int as Gray Code.
 *
 * @has 1 - 1 FitnessCache
 */
public final class Palette implements Comparable<Palette> {

    /** The colors in the palette */
    private int[] colors;

    /** The fitness cache */
    FitnessCache cache;

    /**
     * Constructor.
     *
     * @param colors
     *            the colors
     * @param cache
     *            the fitness cache
     */
    public Palette(int[][] colors, FitnessCache cache) {
        this.cache = cache;
        this.colors = new int[colors.length];
        initializeColors(colors);
    }

    /**
     * Creates a deep copy of a palette.
     *
     * @return the copy
     */
    public Palette copy() {
        Palette result = new Palette(new int[][] {}, cache);
        result.colors = Arrays.copyOf(colors, colors.length);
        result.cache = cache.copy();
        return result;
    }

    /**
     * Returns a copy of the colors table.
     *
     * @return the copy
     */
    public int[] getColors() {
        return Arrays.copyOf(colors, colors.length);
    }

    /**
     * Gets the fitness.
     *
     * @return the fitness
     */
    public double getFitness() {
        return cache.getFitness(this);
    }

    /**
     * Swaps a color pair between palette p1 and p2.
     *
     * @param pairs
     *            the pairs
     * @param i
     *            the index of the pair to swap
     * @param p1
     *            the first palette
     * @param p2
     *            the second palette
     */
    public static void swapPair(int[][] pairs, int i, Palette p1, Palette p2) {
        Double swapFitness1 = p1.cache.fitnessOfPair[i];
        Double swapFitness2 = p2.cache.fitnessOfPair[i];

        // swap colors
        for (int j = 0; j < 2; j++) {
            int colorIndex = pairs[i][j];
            int swap = p1.getColor(colorIndex);

            // note that setColor invalidates pair fitness
            p1.setColor(colorIndex, p2.getColor(colorIndex));
            p2.setColor(colorIndex, swap);
        }

        // swap original fitness of exchanged pair
        p1.cache.fitnessOfPair[i] = swapFitness2;
        p2.cache.fitnessOfPair[i] = swapFitness1;
    }

    /**
     * Gets the index-th value as RGB array.
     *
     * @param index
     *            the index
     * @return the value
     */
    public int[] getRGBColor(final int index) {
        return ColorEncoder.intToRGB(ColorEncoder.gray2bin(colors[index]));
    }

    /**
     * Sets the index-th value from RGB array.
     *
     * @param index
     *            the index
     * @param rgb
     *            the value
     */
    public void setRGBColor(final int index, final int[] rgb) {
        setColor(index, ColorEncoder.bin2gray(ColorEncoder.rgbToInt(rgb)));
    }

    /**
     * Gets the index-th color value.
     *
     * @param index
     *            the index
     * @return the value
     */
    public int getColor(final int index) {
        return colors[index];
    }

    /**
     * Sets the index-th color value.
     *
     * @param index
     *            the index
     * @param value
     *            the value
     */
    public void setColor(final int index, final int value) {
        colors[index] = value;

        if (cache != null) {
            cache.invalidateFitness(index);
        }
    }

    /**
     * Returns the number of colors in the palette.
     *
     * @return the number of colors in the palette
     */
    public int numberOfColors() {
        return colors.length;
    }

    /**
     * Returns true if the palette has been evaluated.
     *
     * @return true if evaluated, false otherwise.
     */
    public boolean isEvaluated() {
        return !cache.isInvalid();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(Palette palette) {
        return Double.compare(getFitness(), palette.getFitness());
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Palette)) {
            return false;
        }
        int[] c = ((Palette) o).getColors();
        return Arrays.equals(colors, c);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(colors);
    }

    /**
     * Returns solution as string including fitness and array of hex colors.
     *
     * @return the string
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(isEvaluated() ? getFitness() : "?");
        builder.append(" [ ");
        for (int color : colors) {
            int[] rgb = ColorEncoder.intToRGB(ColorEncoder.gray2bin(color));
            builder.append(ColorEncoder.hex(rgb[0], rgb[1], rgb[2]));
            builder.append(" ");
        }
        builder.append("]");
        return builder.toString();
    }

    /**
     * Called by constructor to initialize internal color table from parameter.
     *
     * @param colors the colors
     */
    private void initializeColors(int[][] colors) {
        for (int i = 0; i < colors.length; i++) {
            setRGBColor(i, colors[i]);
        }
    }
}
