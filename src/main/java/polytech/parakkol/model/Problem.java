package polytech.parakkol.model;

import java.util.Arrays;

import polytech.parakkol.util.ColorPairMap;

/**
 * Immutable problem instance that can be referenced by evaluators.
 * We use public final fields instead of getters for simplicity.
 *
 * @author 'Daniel TISCHER'
 *
 * @has 1 - 1 Palette
 * @has 1 - 1 Evaluator
 * @has 1 - 1 ColorPairMap
 */
public final class Problem {

    /** Problem instance name (for logging) */
    public final String name;

    /** Color pairs of the problem */
    public final int[][] pairs;

    /** The problem reference palette */
    public final Palette palette;

    /** Number of colors in the problem */
    public final int numberOfColors;

    /** Number of pairs in the problem */
    public final int numberOfPairs;

    /** Evaluator for the FitnessCache. */
    public final Evaluator evaluator;

    /** color-pair reverse index of pairs affected by color change. */
    final ColorPairMap reverseIndex;

    /**
     * Constructor.
     *
     * @param name    the name of the instance
     * @param colors  the colors of the reference palette
     * @param pairs   the pairs of the reference palette
     */
    public Problem(String name, final int[][] colors, final int[][] pairs) {

        int[][] copiedPairs = Arrays.copyOf(pairs, pairs.length);
        int[][] copiedColors = Arrays.copyOf(colors, colors.length);

        this.name = name;
        this.pairs = copiedPairs;

        numberOfColors = copiedColors.length;
        numberOfPairs = copiedPairs.length;

        reverseIndex = new ColorPairMap(copiedPairs);
        evaluator = new Evaluator(this, copiedColors);
        palette = new Palette(copiedColors,
                new FitnessCache(numberOfPairs, reverseIndex, evaluator));
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();

        sb.append("\tColor problem instance: ").append(name);
        sb.append(" (number of colors: ").append(numberOfColors);
        sb.append(", number of pairs: ").append(numberOfPairs);
        sb.append(")\n");
        sb.append("\t\tcolors:\n");
        sb.append("\t\t\t" + palette);
        sb.append("\n\t\t(fg,bg) pairs:\n");
        for (int i = 0; i < pairs.length; ++i) {
            sb.append("\t\t\t(").append(pairs[i][0]).append(',');
            sb.append(pairs[i][1]).append(")\n");
        }
        return sb.toString();
    }
}
