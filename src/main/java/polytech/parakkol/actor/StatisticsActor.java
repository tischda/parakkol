package polytech.parakkol.actor;

import java.util.concurrent.ConcurrentHashMap;

import polytech.parakkol.ConfigManager.Keys;
import polytech.parakkol.ParametersGA;
import polytech.parakkol.actor.Message.ClearStatistics;
import polytech.parakkol.actor.Message.FilesProcessed;
import polytech.parakkol.actor.Message.SolverMetrics;
import polytech.parakkol.stats.Statistics;
import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import com.typesafe.config.Config;

public final class StatisticsActor extends UntypedActor {

    private final LoggingAdapter log = Logging.getLogger(getContext().system(), this);
    public static final String NAME = "statisticsActor";

    /** The Statistics map (Key: problem name, Value: problem statistics) */
    final ConcurrentHashMap<String, Statistics> statistics;

    private Config config;
    private Config newConfig;

    private int maxRunCount;

    /**
     * Constructor.
     *
     * @param config the config
     */
    public StatisticsActor(Config config) {
        this.config = config;
        maxRunCount = config.getInt(Keys.RUN_COUNT.toString());
        statistics = new ConcurrentHashMap<String, Statistics>();
    }

    @Override
    public void onReceive(Object message) {
        if (message instanceof Config) {
            handleConfigurationChange(message);
        }
        else if (message instanceof SolverMetrics) {
            processMetrics(message);
        } else if (message instanceof ClearStatistics) {
            clearStatistics();
        } else {
            unhandled(message);
        }
    }

    private void handleConfigurationChange(Object message) {
        newConfig = (Config) message;
    }

    private void processMetrics(Object message) {
        SolverMetrics metrics = (SolverMetrics) message;
        log.debug("received new metrics for problem: {}, run: {}", metrics.problem, metrics.runCount);

        recordSolverMetrics(metrics);
        tellProgressStatistics(metrics);
    }

    /**
     * Tells master know about run metrics and overall progress for UI updates.
     *
     * @param metrics the original metrics received from solver actor
     */
    private void tellProgressStatistics(SolverMetrics metrics) {
        ActorRef parent = getContext().parent();
        parent.forward(metrics, getContext());

        if (isLastRun(metrics)) {
            int count = statistics.size();
            parent.tell(new FilesProcessed(count), getSelf());
        }
    }

    /**
     * Clears the statistics and activates new configuration if necessary.
     */
    void clearStatistics() {
        if (newConfig != null) {
            config = newConfig;
            newConfig = null;
            log.info("New configuration activated.");
        }
        maxRunCount = config.getInt(Keys.RUN_COUNT.toString());

        // TODO garbage collection?
        statistics.clear();
    }

    /**
     * Records the solver metrics.
     *
     * @param solverMetrics
     */
    void recordSolverMetrics(SolverMetrics solverMetrics) {
        Statistics newStats = new Statistics(new ParametersGA(config));

        // the previous value associated with the specified key, or null if there was no mapping for the key
        Statistics previousValue = statistics.putIfAbsent(solverMetrics.problem, newStats);

        if (previousValue == null) {
            newStats.record(solverMetrics.runMetrics);
        } else {
            previousValue.record(solverMetrics.runMetrics);
        }
    }

    /**
     * Returns true if this is the last run for this problem.
     *
     * @param metrics the run metrics
     * @return true if last run, false otherwise
     */
    private boolean isLastRun(SolverMetrics metrics) {
        return metrics.runCount == maxRunCount - 1;
    }
}
