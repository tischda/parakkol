package polytech.parakkol.actor;

/**
 * cf. page 129, AKKA Java Documentation
 *
 * @author 'Daniel TISCHER'
 *
 */
public enum ActorState {

    STARTING("Starting"),
    IDLE("Idle"),
    ACTIVE("Active");

    private final String text;

    private ActorState(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
