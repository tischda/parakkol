package polytech.parakkol.actor;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import polytech.parakkol.actor.Message.ClearStatistics;
import polytech.parakkol.actor.Message.FilesProcessed;
import polytech.parakkol.actor.Message.FilesToProcess;
import polytech.parakkol.actor.Message.RequestAFile;
import polytech.parakkol.actor.Message.SolverMetrics;
import polytech.parakkol.actor.Message.SolverStatus;
import polytech.parakkol.actor.Message.StartASolver;
import polytech.parakkol.actor.fx.ProgressUpdateable;
import polytech.parakkol.actor.fx.StatusListUpdateable;
import polytech.parakkol.actor.fx.TextAreaUpdateable;
import polytech.parakkol.actor.fx.TextFieldUpdateable;
import polytech.parakkol.actor.fx.Updateable;
import polytech.parakkol.stats.Timer;
import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.Kill;
import akka.actor.Props;
import akka.actor.Terminated;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public final class MasterActor extends UntypedActor {
    private final LoggingAdapter log = Logging.getLogger(getContext().system(), this);
    public static final String NAME = "masterActor";

    // used to convert nanos to seconds
    private static final double ONE_BILLION = 1e9;

    // child actors created by master
    private ActorRef loaderActor;
    private ActorRef statisticsActor;

    // JavaFX updateable controls
    private ProgressUpdateable fxProgress;
    private TextFieldUpdateable fxTextField;
    private TextAreaUpdateable fxTextArea;
    private StatusListUpdateable fxStatusList;
    private int toProcessCount;

    // solver parameters
    private Config config;
    private Config newConfig;

    private final Timer timer = new Timer();
    private final AtomicBoolean statisticsNeedClearing = new AtomicBoolean();

    // solvers
    private final AtomicInteger solverId = new AtomicInteger();
    private final List<String> nameList = ConfigFactory.load().getConfig("solver").getStringList("names");
    private final AtomicInteger activeSolverCount = new AtomicInteger();

    /**
     * Constructor.
     *
     * @param config
     *            the AKKA configuration.
     */
    public MasterActor(Config config) {
        this.config = config;
    }

    // TODO missing initialization of JavaFX UI controls. Unfortunately, this
    // can only be done by the MainApp, so when the actors restarts the fields
    // are nulled (and we have a problem).

    // TODO UI is not aware of actor restarts, nor does it about exceptions
    // occuring here, so when should it resend the configuration data?

    /**
     * Initializes the master actor before start.
     */
    @Override
    public void preStart() {
        // create loader actor
        loaderActor = getContext().actorOf(Props.create(LoaderActor.class),
                LoaderActor.NAME);

        // request configuration value from loader actor
        loaderActor.tell(new FilesToProcess(-1), getSelf());

        // create statistics actor
        Props props = Props.create(StatisticsActor.class, config);
        statisticsActor = getContext().actorOf(props, StatisticsActor.NAME);
    }

    @Override
    public void onReceive(Object message) {
        log.debug("Received a message: '{}'", message);

        // configuration messages
        if (message instanceof Updateable) {
            registerJavaFXControls(message);
        } else if (message instanceof Config) {
            handleConfigurationChange(message);
        } else if (message instanceof FilesToProcess) {
            toProcessCount = ((FilesToProcess) message).getCount();
        }

        // solver termination
        else if (message instanceof Terminated) {
            handleSolverTermination();
        }

        // status updates and user actions
        else if (!statisticsNeedClearing.get()) {
            processActions(message);

            // TODO starting new solvers too quickly, timer will start and old
            // messages still in the queue will update non-existing solvers...
            if (timer.isTimerStarted()) {
                processUpdates(message);
            }
        } else {
            unhandled(message);
        }
    }

    /**
     * Registers JavaFX controls for status updates.
     *
     * @param message
     *            the message is the actual control
     */
    private void registerJavaFXControls(Object message) {
        if (message instanceof ProgressUpdateable) {
            fxProgress = (ProgressUpdateable) message;
        } else if (message instanceof TextFieldUpdateable) {
            fxTextField = (TextFieldUpdateable) message;
        } else if (message instanceof TextAreaUpdateable) {
            fxTextArea = (TextAreaUpdateable) message;
        } else if (message instanceof StatusListUpdateable) {
            fxStatusList = (StatusListUpdateable) message;
        }
    }

    /**
     * Notifies statistics actor about configuration change and saves the new
     * configuration in a temporary field. It will be activated when statistics
     * are cleared (i.e. configuration changes won't affect running solvers).
     *
     * @param message
     *            the AKKA config object
     */
    private void handleConfigurationChange(Object message) {
        newConfig = (Config) message;
        statisticsActor.forward(message, getContext());
    }

    /**
     * Processes user actions.
     *
     * @param message
     *            the message
     */
    private void processActions(Object message) {
        if (message instanceof ClearStatistics) {
            clearStatistics(message);
        } else if (message instanceof StartASolver) {
            if (!timer.isTimerStarted()) {
                timer.start();
            }
            startASolver();
        }
    }

    /**
     * Processes status updates.
     *
     * @param message
     *            the message
     */
    private void processUpdates(Object message) {
        if (message instanceof FilesProcessed) {
            int processedCount = ((FilesProcessed) message).getCount();
            updateProgressBar(processedCount);
            log.info("processed: {}/{}", processedCount, toProcessCount);
        } else if (message instanceof SolverStatus
                || message instanceof SolverMetrics) {
            updateStatusList(message);
            updateTextArea("Time: " + timer.getElapsedTime() / ONE_BILLION
                    + " seconds");
        }
    }

    /**
     * Processes actions related to solver termination, for instance
     * decrementing the solver count, stopping timers, etc.
     */
    private void handleSolverTermination() {
        if (activeSolverCount.decrementAndGet() == 0) {
            log.info("last solver terminated");

            // all work finished, stop timer so we know how long it took
            timer.stop();

            // clearing may have been delayed until all solver stopped
            if (statisticsNeedClearing.get()) {
                clearStatistics(new ClearStatistics());
            }
        }
        updateTextField(activeSolverCount.get());
    }

    /**
     * Resets statitics and state, offering a clean context for next execution.
     *
     * This is a bit tricky since a lot of things are running in the background,
     * so even if we stop all actors, messages from the previous execution may
     * still be (or enter) the mailbox.
     *
     * @param message the ClearStatistics message
     */
    private void clearStatistics(Object message) {
        if (activeSolverCount.get() > 0) {
            log.info("clearStatistics cond. 1");
            if (!statisticsNeedClearing.get()) {
                statisticsNeedClearing.set(true);
                stopAllSolvers();
            }
        } else {
            log.info("clearStatistics cond. 2");

            // solvers are dead, now notify clients and servers
            statisticsActor.forward(message, getContext());
            loaderActor.forward(message, getContext());

            // time to activate new configuration if any
            if (newConfig != null) {
                config = newConfig;
                newConfig = null;
                log.info("New configuration activated.");
            }

            // let's start form scratch
            timer.reset();
            updateProgressBar(0);
            updateTextArea("Statistics cleared.");

            // from now on, we allow creation of new solvers and process status
            // messages again. This does not guarantee that some old messages
            // from the previous execution will not hit us...
            statisticsNeedClearing.set(false);
        }
    }

    /**
     * Stops all solvers.
     */
    // http://stackoverflow.com/questions/13847963/akka-kill-vs-stop-vs-poison-pill
    private void stopAllSolvers() {

        // display all children (incl. loaderActor and statisticsActor)
        log.debug(MasterActor.NAME + " has following children:");
        for (ActorRef child : getContext().getChildren()) {
            log.debug(child.toString());
        }

        // kill solvers only
        String query = "/user/" + MasterActor.NAME + "/" + SolverActor.PREFIX + "*";
        ActorSelection selection = getContext().actorSelection(query);
        selection.tell(Kill.getInstance(), getSelf());
    }

    /**
     * Starts a solver and tell it to request a file.
     */
    private void startASolver() {
        ActorRef solverActor = createSolver();
        activeSolverCount.incrementAndGet();

        // watch actor lifecyle (i.e. receive 'Terminated' messages from solvers)
        getContext().watch(solverActor);

        // update UI
        updateTextField(activeSolverCount.get());

        // ok, now we can start processing files...
        solverActor.tell(new RequestAFile(), getSelf());
    }

    /**
     * Creates a solver.
     */
    ActorRef createSolver() {
        int id = solverId.incrementAndGet();
        String displayName = nameList.get(id % nameList.size());
        Props props = Props.create(SolverActor.class, displayName,
                loaderActor, statisticsActor, config);

        // Naming is critical to stop the solvers (we filter on PREFIX)
        return getContext().actorOf(props, SolverActor.PREFIX + id);
    }

    /**
     * Updates the progress bar control.
     *
     * @param processedCount the number of files processed
     */
    private void updateProgressBar(int processedCount) {
        if (fxProgress == null) {
            log.error("fxProgress not initialized");
        } else {
            if (toProcessCount > 0) {
                fxProgress.update((double) processedCount / toProcessCount);
            }
        }
    }

    /**
     * Updates the number of active solvers in the UI.
     *
     * @param value the number of solvers currently active
     */
    private void updateTextField(int value) {
        if (fxTextField == null) {
            log.error("fxTextField not initialized");
        } else {
            fxTextField.update(value);
        }
    }

    /**
     * Updates the status area in the UI.
     *
     * @param text status text to be displayed (e.g. processing time)
     */
    private void updateTextArea(String text) {
        if (fxTextArea == null) {
            log.error("fxTextArea not initialized");
        } else {
            fxTextArea.update(text);
        }
    }

    /**
     * Updates the list of actors and problems in the UI.
     *
     * @param message SolverStatus or SolverMetrics
     */
    private void updateStatusList(Object message) {
        if (fxStatusList == null) {
            log.error("fxStatusList not initialized");
        } else {
            fxStatusList.update(message);
        }
    }

    /**
     * Starts the internal timer (for testing).
     */
    void startTimer() {
        timer.start();
    }
}
