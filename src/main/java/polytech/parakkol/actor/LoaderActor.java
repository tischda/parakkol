package polytech.parakkol.actor;

import java.io.IOException;

import polytech.parakkol.Loader;
import polytech.parakkol.actor.Message.ClearStatistics;
import polytech.parakkol.actor.Message.FilesToProcess;
import polytech.parakkol.actor.Message.RequestAFile;
import akka.actor.PoisonPill;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

/**
 * The problem loader actor.
 *
 * @author 'Daniel TISCHER'
 */
public final class LoaderActor extends UntypedActor {

    private final LoggingAdapter log = Logging.getLogger(getContext().system(), this);
    public static final String NAME = "loaderActor";

    private Loader loader;

    @Override
    public void preStart() {
        initializeLoader();
    }

    @Override
    public void onReceive(final Object message) {
        if (message instanceof FilesToProcess) {
            sendFilesToProcessCount();
        } else if (message instanceof ClearStatistics) {
            initializeLoader();
        } else if (message instanceof RequestAFile) {
            sendFileOrPoisonIfFinished(message);
        } else {
            unhandled(message);
        }
    }

    /**
     * Sets a new loader for this actor. This will reset the list of files to be
     * processed as they will be read again from the dataPath.
     */
    private void initializeLoader() {
        Config config = ConfigFactory.load();
        String dataPath = config.getConfig("parakkol").getString("data_path");
        loader = new Loader(dataPath);
    }

    /**
     * Tells the master about the number of files to process.
     *
     * @param message
     *            the number of files to process
     */
    private void sendFilesToProcessCount() {
        int count = loader.toProcessCount();
        getSender().tell(new FilesToProcess(count), getSelf());
    }

    /**
     * Sends a problem instance to the solver.
     *
     * When all work is finished, there is no point in keeping the solvers
     * waiting, so send them a poison pill.
     *
     * @param message the RequestAFile message from the solver
     */
    private void sendFileOrPoisonIfFinished(final Object message) {
        if (loader.toProcessCount() > 0) {
            try {
                getSender().tell(loader.pickAFile(), getSelf());
            } catch (IOException e) {
                log.error(e, "Cannot pick a file");
                unhandled(message);
            }
        } else {
            getSender().tell(PoisonPill.getInstance(), getSelf());
        }
    }

    /**
     * Gets the loader (for testing).
     *
     * @return the loader
     */
    Loader getLoader() {
        return loader;
    }
}
