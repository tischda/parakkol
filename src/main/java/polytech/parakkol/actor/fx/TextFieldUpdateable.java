package polytech.parakkol.actor.fx;

/**
 * Interface to decouple Akka main actor from JavaFX TextField.
 *
 * @author 'Daniel TISCHER'
 *
 */
public interface TextFieldUpdateable extends Updateable {
    void update(int count);
}
