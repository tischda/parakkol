package polytech.parakkol.actor.fx;

/**
 * Marker interface used in message type switch in MasterActor.
 *
 * @author 'Daniel TISCHER'
 *
 */
public interface Updateable { }
