package polytech.parakkol.actor.fx;

/**
 * Interface to decouple Akka main actor from JavaFX ProgressBar.
 *
 * @author 'Daniel TISCHER'
 *
 */
public interface ProgressUpdateable extends Updateable {
    void update(double progress);
}
