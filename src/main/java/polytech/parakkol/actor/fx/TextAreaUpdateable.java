package polytech.parakkol.actor.fx;

/**
 * Interface to decouple Akka main actor from JavaFX TextArea.
 *
 * @author 'Daniel TISCHER'
 *
 */
public interface TextAreaUpdateable extends Updateable {
    void update(String text);
}
