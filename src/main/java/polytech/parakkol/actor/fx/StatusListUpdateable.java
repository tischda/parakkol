package polytech.parakkol.actor.fx;

/**
 * Interface to decouple Akka main actor from JavaFX ObservableList.
 *
 * @author 'Daniel TISCHER'
 *
 */
public interface StatusListUpdateable extends Updateable {
    void update(Object message);
}
