package polytech.parakkol.actor;

import polytech.parakkol.stats.Metrics;

//TODO: UmlGraph sequence diagram

/**
 * Message types sent between actors.
 *
 * @author 'Daniel TISCHER'
 *
 */
public class Message {
    /**
     * 1. user (UI) asks master to clear statistics
     * 2. master forwards message to statistics
     */
    public static final class ClearStatistics extends Message { }

    /** user (UI) asks master to start a solver */
    public static final class StartASolver extends Message { }

    /**
     * 1. master asks solver to request a file
     * 2. solver requests a file from loader
     */
    public static final class RequestAFile extends Message { }

    /** solver sends status to master */
    public static final class SolverStatus {
        public final String name;
        public final String status;
        public final String problem;

        public SolverStatus(String name, String status, String problem) {
            this.name = name;
            this.status = status;
            this.problem = problem;
        }
    }

    /**
     * 1. solver sends metrics to statistics
     * 2. statistics forwards metrics to master
     */
    public static final class SolverMetrics {
        public final String problem;
        public final int runCount;
        public final Metrics runMetrics;

        public SolverMetrics(String problem, int runCounter, Metrics runMetrics) {
            this.problem = problem;
            this.runCount = runCounter;
            this.runMetrics = runMetrics;
        }
    }

    /** statistics send number of processed files to master */
    public static final class FilesProcessed extends FilesCount {
        public FilesProcessed(int count) {
            super(count);
        }
    }

    /**
     * 1. master asks loader about number of files to be processed
     * 2. loader replies with number of files to be processed
     */
    public static final class FilesToProcess extends FilesCount {
        public FilesToProcess(int count) {
            super(count);
        }
    }

    /**
     * Returns a string representation of the message.
     */
    @Override
    public String toString() {
        return this.getClass().getCanonicalName();
    }

    /**
     * Base class for FilesToProcess and FilesProcessed.
     */
    private static class FilesCount extends Message {
        private final int count;
        public FilesCount(int count) {
            this.count = count;
        }
        public int getCount() {
            return count;
        }
    }
}
