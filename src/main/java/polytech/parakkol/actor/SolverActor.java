package polytech.parakkol.actor;

import polytech.parakkol.Parameters;
import polytech.parakkol.ParametersGA;
import polytech.parakkol.actor.Message.RequestAFile;
import polytech.parakkol.actor.Message.SolverMetrics;
import polytech.parakkol.actor.Message.SolverStatus;
import polytech.parakkol.ga.Solver;
import polytech.parakkol.model.Problem;
import polytech.parakkol.stats.Metrics;
import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import com.typesafe.config.Config;

/**
 * The solver actor requests problems from the loader. A solver can solve
 * several problems. For each run, the metrics are sent to statistics.
 *
 * When all work is done, the loader replies with a poison pill and the
 * solver actor dies.
 *
 * @author 'Daniel TISCHER'
 */
public final class SolverActor extends UntypedActor {

    private final LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    /** The SolverActor name prefix used for ActorSelection. */
    public static final String PREFIX = "solverActor";

    /** Used to convert nanos to milliseconds. */
    private static final double ONE_MILLION = 1e6;

    /**  The solver backing object. */
    final Solver solver;

    /** The loader ActorRef. */
    private final ActorRef loaderActor;

    /** The statistics ActorRef. */
    private final ActorRef statisticsActor;

    /** The solver name (seen by the user). */
    private final String displayName;

    /** The solver parameters. */
    private final Parameters params;

    /** The actor state. */
    ActorState state = ActorState.IDLE;

    /**
     * Instantiates a new solver actor.
     *
     * @param name the name
     * @param loader the loader
     * @param statistics the statistics
     * @param config the config
     */
    public SolverActor(String name, final ActorRef loader, final ActorRef statistics, Config config) {
        displayName = name;
        loaderActor = loader;
        statisticsActor = statistics;
        params = new ParametersGA(config);
        solver = new Solver((ParametersGA) params);
    }

    /* (non-Javadoc)
     * @see akka.actor.UntypedActor#preStart()
     */
    @Override
    public void preStart() {
        setState(ActorState.STARTING, "n/a");
    }

    /* (non-Javadoc)
     * @see akka.actor.UntypedActor#onReceive(java.lang.Object)
     */
    @Override
    public void onReceive(final Object message) {
        if (message instanceof RequestAFile) {
            requestAFile();
        }
        else if (message instanceof Problem) {
            solveProblem((Problem) message);
            requestAFile();
        } else {
            unhandled(message);
        }
    }

    /**
     * Executes state transition and notifies supervisor.
     *
     * @param newState the new state
     * @param problemName the problem displayName
     */
    private void setState(ActorState newState, String problemName) {
        state = newState;
        tellStateChange(newState, problemName);
    }

    /**
     * Requests a new problem file from the loader.
     */
    private void requestAFile() {
        log.debug("[" + displayName + "] requesting a file");
        loaderActor.tell(new RequestAFile(), getSelf());
    }

    /**
     * Solves a problem RUN_COUNT times.
     *
     * @param problem the problem to solve
     */
    void solveProblem(Problem problem) {
        log.debug("[" + displayName + "] received problem '{}' from loader", problem.name);

        setState(ActorState.ACTIVE, problem.name);
        for (int run = 0; run < params.getRunCount(); run++) {
            Metrics metrics = new Metrics(params.getMaxEvaluations());
            executeSingleRun(problem, metrics);
            tellSolverMetrics(problem, run, metrics);
        }
        setState(ActorState.IDLE, problem.name);
    }

    /**
     * Executes a single run.
     *
     * @param problem the problem
     * @param metrics the metrics
     */
    private void executeSingleRun(Problem problem, Metrics metrics) {
        solver.solve(problem, metrics);

        log.debug("[" + displayName + "] solved problem '{}'", problem.name);
        log.debug("Best: {}", metrics.getBestSolution());
        log.debug("Time: {} ms\n", metrics.getElapsedTime() / ONE_MILLION);
    }

    /**
     * Notifies statistics actor with a metrics update.
     *
     * @param problem the problem
     * @param run the run
     * @param metrics the runMetrics
     */
    private void tellSolverMetrics(Problem problem, int run, Metrics metrics) {
        SolverMetrics solverMetrics = new SolverMetrics(problem.name, run, metrics);
        statisticsActor.tell(solverMetrics, this.getSelf());
    }

    /**
     * Notified master actor with state change.
     *
     * @param newState the new state
     * @param problemName the problem currently being solved
     */
    private void tellStateChange(ActorState newState, String problemName) {
        SolverStatus solverStatus = new SolverStatus(displayName, newState.toString(), problemName);
        getContext().parent().tell(solverStatus, this.getSelf());
    }
}
