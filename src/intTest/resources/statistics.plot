
#set terminal postscript eps enhanced rounded color colortext lw 2 dl 4 20
#set terminal postscript eps enhanced rounded lw 2 dl 4 20
set terminal png nocrop medium
#set key below
#set key off

set key title ""
set key inside right top vertical Right noreverse enhanced autotitles nobox
set key noinvert samplen 4 spacing 1 width 0 height 0

set style data lines
set style function lines
set encoding utf8
set ylabel "fitness"

set terminal pngcairo size 800,600 enhanced font 'Verdana,10'

set xlabel "#evaluation"
set output "build/color".solver."-".instance."-eval.png"
plot \
     "build/color".solver."-".instance."-eval.dat" using 1:4:5 with filledcurve  title solver." mean+/-stddev" lc rgb "#66CDAA", \
     "build/color".solver."-".instance."-eval.dat" using 1:2 with lines title solver." mean" lt 1 lw 2 linecolor rgb "#0000CD"

set xlabel "time (ms)"
set output "build/color".solver."-".instance."-elapsed.png"
plot \
     "build/color".solver."-".instance."-elapsed.dat" using 1:4:5 with filledcurve  title solver." mean+/-stddev" lc rgb "#66CDAA", \
     "build/color".solver."-".instance."-elapsed.dat" using 1:2 with lines title solver." mean" lt 1 lw 2 linecolor rgb "#0000CD"


