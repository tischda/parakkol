package polytech.parakkol;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import polytech.parakkol.actor.LoaderActor;
import polytech.parakkol.actor.Message.RequestAFile;
import polytech.parakkol.model.Problem;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.JavaTestKit;

/**
 * cf. Akka Java Documentation p.134
 */
public class LoaderActorIntTest {

    static ActorSystem system;

    @BeforeClass
    public static void setup() {
        system = ActorSystem.create();
    }

    @AfterClass
    public static void teardown() {
        JavaTestKit.shutdownActorSystem(system);
        system = null;
    }

    @Test
    public void testIt() {
        new JavaTestKit(system) {{
            final Props props = Props.create(LoaderActor.class);
            final ActorRef subject = system.actorOf(props);

            subject.tell(new RequestAFile(), getRef());

            // await the correct response
            expectMsgClass(duration("1 second"), Problem.class);
            Assert.assertEquals(subject, getLastSender());

            // the run() method needs to finish within 2 seconds
            new Within(duration("2 seconds")) {

                @Override
                protected void run() {
                    subject.tell("hello", getRef());

                    // Will wait for the rest of the 2 seconds
                    expectNoMsg();
                }
            };
        }};
    }
}
