package polytech.parakkol.fx.master;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.Test;

import polytech.parakkol.ConfigManager;

public final class MasterModelIntTest {

    @Test
    public void testPersistence() throws IOException {
        Path path = FileSystems.getDefault().getPath(".", ConfigManager.CUSTOM_PARAMETERS_FILENAME);
        Files.deleteIfExists(path);
        try {
            MasterModel model = new MasterModel();
            model.load();

            // 5 should be the default for testing
            assertThat(model.getRunCount(), equalTo(5));

            model.setRunCount(7);
            model.save();

            MasterModel newModel = new MasterModel();
            newModel.load();
            assertThat(model.getRunCount(), equalTo(7));
        }
        finally {
            Files.deleteIfExists(path);
        }
    }
}
