package polytech.parakkol.stats;

import java.io.File;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import polytech.parakkol.ConfigManager;
import polytech.parakkol.ParametersGA;
import polytech.parakkol.Loader;
import polytech.parakkol.ga.Solver;
import polytech.parakkol.model.Problem;

import com.typesafe.config.ConfigFactory;

public class StatisticsPlotIntTest {

    private static final String GNUPLOT_EXEC = System.getProperty("os.name")
            .startsWith("Windows") ? "C:\\usr\\bin\\gnuplot\\bin\\wgnuplot.exe" : "gnuplot";

    private static final String TEST_INSTANCE = "11-2";
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Before
    public void setUp() throws IOException {
        ConfigManager configManager = new ConfigManager();
        ParametersGA params = new ParametersGA(configManager.load());

        // load problem
        String dataPath = ConfigFactory.load().getConfig("parakkol")
                .getString("data_path");
        Loader loader = new Loader(dataPath);
        Problem problem = loader.readFile(TEST_INSTANCE + ".dat");

        // configure solver
        final Solver solver = new Solver(params);
        Random random = new Random();
        random.setSeed(1L);
        solver.setRandom(random);

        StatisticsReports stats = new StatisticsReports(params);

        // run solver
        long startTime = System.nanoTime();
        for (int i = 0; i < params.getRunCount(); i++) {
            Metrics metrics = new Metrics(params.getMaxEvaluations());
            solver.solve(problem, metrics);
            stats.record(metrics);
        }
        double duration = (System.nanoTime() - startTime)
                / (1.e6 * params.getRunCount());
        log.info("Solver mean time: {} ms", duration);

        // write stats
        stats.saveToFiles("build" + File.separator + "colorGA-" + TEST_INSTANCE, problem.toString());
    }

    @Test
    public void testPlot() throws IOException, InterruptedException {

        String[] cmd = new String[] { GNUPLOT_EXEC, "-e",
                "solver='GA';instance='" + TEST_INSTANCE + "'",
                "build" + File.separator + "resources"
                        + File.separator + "intTest"
                        + File.separator + "statistics.plot" };

        ProcessBuilder pb = new ProcessBuilder(cmd);
        pb.redirectOutput(Redirect.INHERIT);
        pb.redirectError(Redirect.INHERIT);
        Process exec = pb.start();
        exec.waitFor();
    }
}
