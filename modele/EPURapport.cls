% ---------------------------------------------------------------------------- %
% EPURapport.cls
% Fichier de style LaTeX pour les rapports et cahiers de specifications des PFE.
%
% CHANGELOG:
% * version 2.2 du 25/04/2014 - Daniel Tischer <daniel.tischer@etu.univ-tours.fr>
% - merge CDS + http://redmine.polytech.univ-tours.fr/svn/modele-epurapport (r6)
% - police de caractere plus lisible sur ecran et ajout de microtype et eurosym
% - generation de PDF en version 1.7
%
% * version r6 du 09/04/2014 - Cyrille Faucheux <cyrille.faucheux@etu.univ-tours.fr>
% - correction de la pagination en mode twoside
%
% * version 2.1 du 07/10/2010 - Nicolas Ragot <nicolas.ragot@univ-tours.fr>
% - correction des en-tetes du CDS
%
% * version 2 du 21/10/2009 - Nicolas Ragot <nicolas.ragot@univ-tours.fr>
% - intégration cahier de specifications (CDS)
%
% * version 1 du 24/04/2009 - Cyrille Faucheux <cyrille.faucheux@etu.univ-tours.fr>
%                             et Lissy A.
% ---------------------------------------------------------------------------- %

\NeedsTeXFormat{LaTeX2e}
\pdfoptionpdfminorversion 7

%%%%
% Identification
\ProvidesClass{EPURapport}[2014/04/25 - v2.2 EPURapport document class]

%%%%
% Packages that must be loaded BEFORE superclass (a4paper)
\RequirePackage{ifthen}

%%%%
% Superclass options
\newboolean{BTwoPages}
\newboolean{BFourPages}
\newboolean{BUTF8}
\newboolean{BFinal}
\newboolean{BCDS}

\DeclareOption{UTF8}{
    \setboolean{BUTF8}{true}
}

\DeclareOption{twoside}{
	\PassOptionsToClass{twoside}{report}
	\PassOptionsToClass{openright}{report}
	\setboolean{BTwoPages}{true}
}
\DeclareOption{fourside}{
	\PassOptionsToClass{twoside}{report}
	\PassOptionsToClass{openright}{report}
	\setboolean{BTwoPages}{true}
	\setboolean{BFourPages}{true}
}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}

%%%%
% Custom class options
\DeclareOption{final}{
    \PassOptionsToClass{\CurrentOption}{final}
    \setboolean{BFinal}{true}
}

\DeclareOption{CDS}{
    \setboolean{BCDS}{true}
}

\ProcessOptions

\ifthenelse{\boolean{BUTF8}}{\RequirePackage[utf8]{inputenc}}{}

%%%%
% Superclass
\LoadClass[11pt,a4paper]{report}

%%%%
% Packages that must be loaded AFTER superclass
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}
%\RequirePackage[charter]{mathdesign}
\RequirePackage[francais]{babel}
\RequirePackage[francais]{translator}   % Required for glossaries
\RequirePackage{url}
\RequirePackage[svgnames]{xcolor}
\RequirePackage{comment}
\RequirePackage{fancyhdr}               % Bling-bling headers and footers
\RequirePackage{lastpage}
\RequirePackage{emptypage}              % No header and footer on empty pages
\RequirePackage{fp}                     % Arithmetic functions
\RequirePackage{graphicx}
\RequirePackage{tabularx}               % Required for CDS
\RequirePackage[unicode=true]{hyperref} % Clicka-proof links
\RequirePackage{microtype}
\RequirePackage{eurosym}

% http://tex.stackexchange.com/questions/79930/textbf-and-bf-within-sans-serif
\renewcommand{\bfdefault}{bx}

%%%%
% Dimensions
\setlength{\evensidemargin}{2cm}
\setlength{\oddsidemargin}{2cm}
\setlength{\textheight}{\paperheight}
\addtolength{\textheight}{-5.7cm}       % 24cm
\setlength{\textwidth}{\paperwidth}
\addtolength{\textwidth}{-4cm}          % 17cm
\setlength{\topmargin}{0.5cm}
\setlength{\headheight}{2cm}
\setlength{\headsep}{0.5cm}
\setlength{\voffset}{-2.54cm}
\setlength{\hoffset}{-2.54cm}
\setlength{\footskip}{1.5cm}

%%%%
% Other document options defined by the user (include.tex file)
\newcommand\notableofcontents{%
    \renewcommand{\tableofcontents}{}
}

\newcommand\nolistoffigures{%
    \renewcommand{\listoffigures}{}
}

\newcommand\nolistoftables{%
    \renewcommand{\listoftables}{}
}

\newcommand{\addextratables}[1]{%
    \renewcommand\extratables{#1}
}

\newcommand\nosupervisors{%
    \renewcommand{\supervisorsDisplay}{}
}

%%%%
% Url, Email and general formatting
\urlstyle{same}                         % don't use monospace font for urls

\newcommand{\email}[1]{
    \href{mailto:#1}{#1}
}

\setcounter{tocdepth}{1}                % Only 2 levels in the TOC
\linespread{1.1}\selectfont             % Increase line spacing for readability

\newenvironment{romanfont}{%
    \fontfamily{lmr}%
    \selectfont
}

%%%%
% Settings applied by Pandoc (must be the same to compare test documents)
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
\setlength{\emergencystretch}{3em}      % prevent overfull lines
\setcounter{secnumdepth}{5}

% ---------------------------------------------------------------------------- %
% Document meta-data
% ---------------------------------------------------------------------------- %

%%%%
% Document information
\newcommand{\thedocument}[3]{%
    \def\thedocumenttype{#1}
    \def\thedocumentlongtitle{#2}
    \def\thedocumentshorttitle{#3}

    % Title and subject in pdf properties
    \hypersetup{
        pdftitle={\thedocumentlongtitle{}}
    }
}

\newcommand{\grade}[1]{%
    \def\theLongGrade{#1}
}

\newcommand{\entitylogo}[1]{%
    \def\theentitylogo{#1}
}

%%%%
% Abstracts and Keywords
\newcommand{\abstracts}[4]{%
    \def\thefrenchabstract{#1}
    \def\thefrenchkeywords{#2}
    \def\theenglishabstract{#3}
    \def\theenglishkeywords{#4}

    % French Keywords in document properties
    \hypersetup{
        pdfsubject={\thefrenchabstract{}},
        pdfkeywords={\thefrenchkeywords{}}
    }
}

% ---------------------------------------------------------------------------- %
% CDS meta-data
% ---------------------------------------------------------------------------- %

%%%%
% Define Authors, Supervisors, Validators, Modifications

% Short version of name and mail
\newcommand{\shortname}[1]{#1}
\newcommand{\shortmail}[1]{<#1>}
\newcommand{\shortcategory}[2]{#2}
\newcommand{\shortdetails}[1]{}

% Long version of name and mail
\newcommand{\longname}[1]{#1\\}
\newcommand{\longmail}[1]{\email{#1}\\}
\newcommand{\longcategory}[2]{
    \textbf{#1}

    #2~\\
}

% Short and long versions for CDS
\newcommand{\bftext}[1]{\textbf{#1~:}}
\newcommand{\shortcomment}[1]{#1~;}
\newcommand{\shortdate}[1]{#1~;}
\newcommand{\longdetails}[1]{#1\\}

\newcommand{\emetteur}[2]{%
    \def\nameEmet{#1}
    \def\coordonnees{#2}
}

% Define all the lists
\newcommand{\validators}[1]{%
    \def\listvalidators{#1}
}

\newcommand{\modifications}[1]{%
    \def\listmodifications{#1}
}

\newcommand{\supervisors}[1]{%
    \def\listsupervisors{#1}
}

\newcommand{\authors}[1]{%
    \def\listauthors{#1}

    % Functors that will be used when evaluating the content of \listauthors
    % They can be redefined (see \authorsDisplay)
    \def\name{\shortname}
    \def\mail{\shortmail}
    \def\category{\shortcategory}
    \def\details{\shortdetails}

    % Adds the authors to the pdf file metadata
    \hypersetup{
        pdfauthor={\listauthors} % The functors previously defined are evaluated
    }
}

% ---------------------------------------------------------------------------- %
% Display macros
% ---------------------------------------------------------------------------- %

%%%%
% Display Authors, Supervisors, Validators, Modifications
\newcommand{\authorsDisplay}{
    \def\name{\longname}
    \def\mail{\longmail}
    \def\category{\longcategory}
    \def\details{\longdetails}
    \listauthors{} % The new functors previously defined are evaluated.
}

\newcommand{\supervisorsDisplay}{
    \def\name{\longname}
    \def\mail{\longmail}
    \def\category{\longcategory}
    \def\details{\longdetails}
    \listsupervisors{}
}

\newcommand{\validatorsDisplay}{
    \def\name{\bftext}
    \def\dateValide{\shortdate}
    \def\valide{\shortcomment}
    \def\comment{\longdetails}
    \listvalidators{}
}

\newcommand{\modificationsDisplay}{
    \def\version{\bftext}
    \def\dateModif{\shortdate}
    \def\description{\longdetails}
    \listmodifications{}
}

\newcommand{\authorsAndSupervisorsDisplay}{%
    \noindent\begin{minipage}[t]{8cm}
        \begin{flushleft}
            \ifthenelse{ \isundefined{\swapAS} }{\supervisorsDisplay{}}{\authorsDisplay{}}
        \end{flushleft}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{8cm}
        \begin{flushright}
            \ifthenelse{ \isundefined{\swapAS} }{\authorsDisplay{}}{\supervisorsDisplay{}}
        \end{flushright}
    \end{minipage}
}

\newcommand{\swapAuthorsAndSupervisors}{
    \def\swapAS{}
}

% ---------------------------------------------------------------------------- %
% CDS Header (on first page)
% ---------------------------------------------------------------------------- %

\newcommand{\tableCDS}{
    \begin{tabularx}{17cm}{|c|c|c|c|X|}
        \hline
        \multicolumn{5}{|c|}{} \\
        \multicolumn{5}{|c|}{\large{\textbf{\thedocumenttype}}}\\
        \multicolumn{5}{|c|}{} \\
        \hline
        \multicolumn{2}{|l|}{\textbf{Projet~:}} & \multicolumn{3}{l|}{\textbf{\thedocumentshorttitle{}}}\\
        \hline
        \multicolumn{2}{|l|}{\textbf{Emetteur~:}} & \multicolumn{2}{l|}{\nameEmet} & Coordonn\'ees~: \coordonnees \\
        \hline
        \multicolumn{2}{|l|}{\textbf{Date d'\'emission~:}} & \multicolumn{3}{l|}{\today}\\
        \hline
        \multicolumn{5}{|c|}{} \\
        \multicolumn{5}{|c|}{\large{\textbf{Validation}}}\\
        \multicolumn{5}{|c|}{} \\
        \hline
        \multicolumn{2}{|l|}{Nom} & Date & Valide (O/N) & Commentaires \\
        \hline
    \end{tabularx}

    \bigskip
        \begin{minipage}[t]{16cm}
            \begin{flushleft}
                {\validatorsDisplay{}}
            \end{flushleft}
        \end{minipage}
    \bigskip

    \begin{tabularx}{17cm}{|l|l|X|}
        \hline
        \multicolumn{3}{|c|}{} \\
        \multicolumn{3}{|c|}{\large{\textbf{Historique des modifications}}}\\
        \multicolumn{3}{|c|}{} \\
        \hline
        Version & Date & Description de la modification \\
        \hline
    \end{tabularx}

    \bigskip
        \begin{minipage}[t]{16cm}
            \begin{flushleft}
                {\modificationsDisplay{}}
            \end{flushleft}
        \end{minipage}
}

\def\schoolLogo{logos/Logo_Polytech_Tours_DI}

% ---------------------------------------------------------------------------- %
% Document first page
% ---------------------------------------------------------------------------- %

\newcommand{\thefirstpage}{%
\thispagestyle{empty}
    \begin{center}
        \begin{romanfont}

        \includegraphics[width=8cm]{\schoolLogo}

        \bigskip{
            {\'Ecole Polytechnique de l'Universit\'e de Tours}\\
            64, Avenue Jean Portalis\\
            37200 TOURS, FRANCE\\
            T\'el. +33 (0)2 47 36 14 14\\
            %Fax  +33 (0)2 47 36 14 22\\
            \href{http://www.polytech.univ-tours.fr}{\texttt{www.polytech.univ-tours.fr}}
        }

        \bigskip

        % si on est dans le cahier de specif
        \ifthenelse{\boolean{BCDS}}{%
            \begin{center}{\LARGE \textbf{\theLongGrade}}\end{center}
            \bigskip
            \tableCDS
        }{% sinon (rapport)
            {\LARGE \textbf{\theLongGrade}
            \vfill
            \textbf{\thedocumenttype}}

            \ifthenelse{\isundefined{\theentitylogo}} % si l'on a un logo defini
            {}{\bigskip \includegraphics[height=2cm]{\theentitylogo}}

            \vfill
            {\Huge \textbf{\thedocumentlongtitle}}
            \vfill

            \end{romanfont}
            \authorsAndSupervisorsDisplay
            \begin{romanfont}

            \ifthenelse{\boolean{BFinal}}{}{~\newline {\small Version du \today}}
        }
        \end{romanfont}
    \end{center}

    % Affichage des pages en chiffres romains pour les pages informatives
    \renewcommand{\thepage}{\Roman{page}}

    % Affichage des tables et listes habituelles
    \cleardoublepage
    \tableofcontents
    \cleardoublepage
    \listoffigures
    \cleardoublepage
    \listoftables
    \cleardoublepage
    % Et des non habituelles
    \ifthenelse{\isundefined{\extratables}}
    {}{\extratables\cleardoublepage}

    % Affichage des numeros de pages en chiffres arabes pour le corps du document
    \renewcommand{\thepage}{\arabic{page}}

    % Liens internes de nouveau en rouge
    \hypersetup{linkcolor=red}
}

% ---------------------------------------------------------------------------- %
% Document last page
% ---------------------------------------------------------------------------- %

\newcommand{\thelastpage}{%
    % Si on est en mode twosides, on va chercher a avoir un
    % nombre de pages multiple de 4
    \clearpage
    \ifthenelse{\boolean{BTwoPages}}
    {%
        \ifthenelse{\boolean{BFourPages}}{ \FPset{\DivB}{4} }{ \FPset{\DivB}{2} }

        \FPset{\DivA}{\thepage}

        %% Decomposition du nombre de pages en A = BQ+R
        %% Compiler avec --shell-escape pour avoir le debug
        %% C'est le R qui nous interesse
        %%% Example : DivA = 26 pages
        %\immediate\write18{printf "Nombre de pages : \DivA"}

        \FPdiv{\tmpDivQ}{\DivA}{\DivB} %% /4 ou /2
        %\immediate\write18{printf "Division : \tmpDivQ"}
        %%% tmpDivQ = 6.75

        \FPtrunc{\DivQ}{\tmpDivQ}{0} %% Nb de paquets complets de 4 pages
        %\immediate\write18{printf "Nombre de paquets de 4 pages : \DivQ"}
        %%% DivQ = 6

        \FPmul{\DivBQ}{\DivQ}{\DivB}
        %\immediate\write18{printf "Nombre de pages en paquet : \DivBQ"}
        %%% DivBQ = 24

        \FPsub{\DivR}{\DivA}{\DivBQ}
        \FPtrunc{\DivR}{\DivR}{0}
        %\immediate\write18{printf "Nombre de pages hors paquet : \DivR"}
        %%% DivR = 27-24 = 3

        \FPifzero{\DivR}
        {
            \FPset{\DivB}{0}
            %\immediate\write18{printf "On ne rajoutera pas de pages : \DivR, \DivB"}
        }\else {
            %\immediate\write18{printf "On rajoutera des pages"}
        }

        \FPsub{\Delta}{\DivB}{\DivR}
        \FPtrunc{\Delta}{\Delta}{0}
        %\immediate\write18{printf "Nombre de pages a ajouter : \Delta"}
        %%% Delta = 4-3 = 1

        \loop
            \FPsub{\Delta}{\Delta}{1}
            \FPifpos{\Delta}{
                %\immediate\write18{printf "Ajout d'une page \Delta"}
                \pagestyle{empty}%
                \hbox{}\newpage
                \if@twocolumn
                    \hbox{}\newpage
                \fi
            }
        \repeat
    }{}

    % En mode "twoside", la derniere page sera impaire, il faut donc
    % intercepter l'ajout d'une page supplementaire.
    \renewcommand\cleardoublepage{}
    \begin{romanfont}
        \chapter*{\thedocumentlongtitle}
    \end{romanfont}
    \thispagestyle{empty}
    \vspace{-1cm}
    \begin{center}
        {\large\theLongGrade}\\~\\
        {\large\thedocumenttype}\\~\\
    \end{center}

    \paragraph*{R\'esum\'e :} \thefrenchabstract
    \paragraph*{Mots clefs :} \thefrenchkeywords
    \paragraph*{Abstract:} \theenglishabstract
    \paragraph*{Keywords:} \theenglishkeywords
    \vspace{1cm}
    \hrule
    \vfill

    \authorsAndSupervisorsDisplay
}

% ---------------------------------------------------------------------------- %
% Appendix
% ---------------------------------------------------------------------------- %

\newcommand{\annexes}{
    \appendix
    \renewcommand\chaptername{\appendixname}
    \part*{Annexes}
    \addcontentsline{toc}{part}{Annexes}
}

% ---------------------------------------------------------------------------- %
% Headers, footers, chapters
% ---------------------------------------------------------------------------- %

\pagestyle{fancy}

%%%%
% Affichage du titre du chapitre
%
%  \chaptername = "Chapitre"
%  \thechapter = numero du chapitre
%  #1 = nom du chapitre
%  --> leftmark
\renewcommand{\chaptermark}[1]{%
    \ifthenelse{\boolean{BCDS}}%
    {
        \markboth{#1}{}
    }
    % sinon (rapport)
    {
        \markboth{\chaptername\ \thechapter. \ #1}{}
    }
}

%%%%
% Affichage du titre de la section
%
%  #1 = nom de la section
%  --> rightmark
\renewcommand{\sectionmark}[1]{\markright{#1}}

% Largeur de l'en-tete
% On enleve 4cm afin de pouvoir placer lo logo
\newlength{\headersize}
\setlength{\headersize}{\textwidth}
\addtolength{\headersize}{-4cm}

% Logo utilise dans l'en-tete
\newsavebox{\EPUlogo}
\sbox{\EPUlogo}{\includegraphics[width=2.6cm]{\schoolLogo}}

\ifthenelse{\boolean{BTwoPages}}{% Twosides mode

    \fancyhead[LE]{% En-tete, champ gauche, page paire (gauche)
        \begin{minipage}[c]{3cm}\usebox{\EPUlogo}\end{minipage}%
        \begin{minipage}{\headersize}%
        {%
            \bfseries\leftmark%
        }%
        \end{minipage}%
    }

    \fancyhead[RO]{% En-tete, champ droit, page impaire (droite)
        \begin{minipage}{\headersize}%
        {%
            \begin{flushright}\bfseries\rightmark\end{flushright}%
        }%
        \end{minipage}%
        \begin{minipage}[c]{3cm}\hfill\usebox{\EPUlogo}\end{minipage}%
    }

    % En tete, champ droit page paire (gauche),
    % et champ gauche page impaire (droite)
    \fancyhead[RE,LO]{}

    % Pied de page
    \fancyfoot[LE,RO]{\bfseries\thepage}
    \fancyfoot[RE,LO]{\bfseries\thedocumentshorttitle}
    %% Pour les pages avec titre
    \fancypagestyle{plain}{%
        \fancyfoot[LE,RO]{\bfseries\thepage}
        \fancyfoot[RE,LO]{}
    }
}{% Oneside mode
    % En-tete, champ gauche
    \fancyhead[L]{%
        \begin{minipage}{3cm}\usebox{\EPUlogo}\end{minipage}%
        \begin{minipage}{\headersize}%
        {%
            \bfseries\leftmark\\%
            \rightmark%
        }%
        \end{minipage}%
    }

    % En-tete, champ gauche
    \fancyhead[R]{}

    % Pied de page
    \fancyfoot[R]{\bfseries\thepage}
    \fancyfoot[L]{\bfseries\thedocumentshorttitle}
    %% Page avec titre
    \fancypagestyle{plain}{%
        \fancyfoot[R]{\bfseries\thepage}
        \fancyfoot[L]{}
    }
}

%%%%
% Reglages valables dans tous les cas
%
% Rien au centre du pied de page
\fancyfoot[C]{}

% Epaisseur des lignes de separation
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0.2pt}

% Pages avec en-tete (debut de chapitre, table des matieres...)
\fancypagestyle{plain}{%
    \fancyhead{}% Rien dans l'en-tete
}

% Definit une ligne horizontale de 2px d'epaisseur
% sur 3/4 de la page. Utilisee pour souligner le
% titre d'un chapitre.
\newcommand{\semirule}{\hbox to \hsize{\hfill
    \vrule height 2pt width.75\hsize
    \hfill}}

%%%%
% Chapitres et pagination
%
% Redifinition de l'en-tete d'un chapitre genere par la commande \chapter{}.
\def\@makechapterhead#1{%
    \begingroup
    \fontsize{\@xivpt}{18}\bfseries\centering
        \ifnum\c@secnumdepth>\m@ne
            \leavevmode \hskip-\leftskip
            \rlap{\vbox to\z@{\vss
                \vskip 3pc}
            }\hskip\leftskip\fi
        \Huge\thechapter .\  #1\par\semirule\vspace{1cm} \endgroup
    \skip@34\p@ \advance\skip@-\normalbaselineskip
    \vskip\skip@ }

% Redifinition de l'en-tete d'un chapitre genere par la commande \chapter*{}.
\def\@makeschapterhead#1{%
    \begingroup
    \fontsize{\@xivpt}{18}\bfseries\centering
    \Huge#1\par\semirule\vspace{1cm} \endgroup
    \skip@34\p@ \advance\skip@-\normalbaselineskip
    \vskip\skip@ }

% Gestion de la premiere et derniere page.
\AtBeginDocument{\thefirstpage}

% Pas de derniere page dans le CDS
\ifthenelse{\boolean{BCDS}}{}{\AtEndDocument{\thelastpage}}
